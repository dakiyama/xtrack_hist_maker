#path="/gpfs/fs7001/dakiyama/DTStudy/DTAnalysis/second_wave/private_nt_maker/xtrack_ntuple_maker/run"
#path="/gpfs/fs7001/dakiyama/ntuple_forsecondwave/user.dakiyama.mc16_13TeV.700325.Sh_2211_Zmumu_maxHTpTV2_CVetoBVeto.deriv.ntuple.20220725_ver0_MyAnalysisAlg"
#path="/gpfs/fs7001/dakiyama/ntuple_forsecondwave/user.dakiyama.422013.single_sigmaplus_logE0p1to1000.ntuple.20220814_ver0_MyAnalysisAlg"
path="/gpfs/fs8001/dakiyama/ntuple_forsecondwave/user.dakiyama.data15_13TeV.AllYear.physics_Main.PhysCont.DAOD_SUSY6.grp15_v01_p4379.ntuple.20220422_0_MyAnalysisAlg"
#path="/gpfs/fs7001/dakiyama/ntuple_forsecondwave/user.dakiyama.user.dakiyama.361107.Py8_Zmumu.simul.DAOD_SUSY6.e3601_s3126_r11620_20220328_EXT0.ntuple.20220516_ver0_MyAnalysisAlg"
#path="/gpfs/fs7001/dakiyama/ntuple_forsecondwave/user.dakiyama.422013.single_sigmaplus_logE5to2000.ntuple.20220502_ver0_MyAnalysisAlg"
while read line
do
    echo ${line}
    logfile="LOG/${line}.log"
    outfile="${line/\/}"
    echo ${path}"/"${line}
    #bsub -q 1d -o ${logfile} root '/gpfs/fs7001/dakiyama/DTStudy/DTAnalysis/second_wave/xtrk-hist-maker/Root/run.C("'${path}'/'${line}'/data-'${line}'/*","'${outfile}'")'
    root '../xtrack_hist_maker/share/run.C("'${path}'/'${line}'","'${outfile}'",false)'
#done < list_sigma.txt
#done < /gpfs/fs7001/dakiyama/ntuple_forsecondwave/user.dakiyama.user.dakiyama.361107.Py8_Zmumu.simul.DAOD_SUSY6.e3601_s3126_r11620_20220328_EXT0.ntuple.20220516_ver0_MyAnalysisAlg/test.txt
done < ${path}/test.txt
#done < /gpfs/fs7001/dakiyama/ntuple_forsecondwave/user.dakiyama.422013.single_sigmaplus_logE5to2000.ntuple.20220502_ver0_MyAnalysisAlg/test.txt
#done < /gpfs/fs7001/dakiyama/DTStudy/DTAnalysis/check_new_ntuple/MyAnalysis/data/data_test.txt
