# xtrack_hist_maker
This is a histogram maker for the disappearing track run2 2nd wave analysis.

## setup
set the correct sample's path which suits for your environment in `share/run.C`.

## run
`share/run_test.sh` is a simple exmaple for running this. prepare a file list as a text file which doesn't include.

## plotting
set the correct sample's path which suits for your environment in `plotting/run.cxx`.
this is ongoing part.