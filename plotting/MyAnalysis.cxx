#include "MyAnalysis.h"

void MyAnalysis::main_alg(){
  gROOT->SetBatch();
  fake_manager();
  open_data_file();
  get_hist();
  print_hist();
  // wtaunu_manager();
  // ptcone_checker();
  // ptcone_abcd();
  et_pt_manager();
  //et_met_manager();
  //met_bias_check();
  make_output_file();
  weight_maker();
  fit_ettopoclus20();
  close_file();
}


MyAnalysis::MyAnalysis()
{
  
}
