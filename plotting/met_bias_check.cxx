#include "MyAnalysis.h"

void MyAnalysis::met_bias_check() {
  get_met_bias_hist();
  print_met_bias_hist();
}

void MyAnalysis::print_met_bias_hist() {
  std::vector<TH1*>* hists = new std::vector<TH1*>;
  std::vector<std::string>* hist_names = new std::vector<std::string>;
  std::vector<std::string>* hist_types = new std::vector<std::string>;

  c_std->SetRightMargin(0.15);
  m_h_ettopoclus20_met_data_allmet_signal_region_fourlayer->Draw("colz");
  c_std->Print("m_h_ettopoclus20_met_data_allmet_signal_region_fourlayer.png");
  m_h_ettopoclus20_subtractmet_data_allmet_signal_region_fourlayer->Draw("colz");
  c_std->Print("m_h_ettopoclus20_subtractmet_data_allmet_signal_region_fourlayer.png");
  c_std->SetRightMargin(0.05);

  hist_config(m_h_ettopoclus20_data_verylowmet_signal_region_fourlayer, 1, 20, 1, 1, 1, 0);
  hist_config(m_h_ettopoclus20_data_lowmet_signal_region_fourlayer, 2, 20, 2, 1, 1, 0);
  hist_config(m_h_ettopoclus20_data_middlemet_signal_region_fourlayer, 4, 20, 4, 1, 1, 0);
  hist_config(m_h_ettopoclus20_data_highmet_signal_region_fourlayer, 210, 20, 210, 1, 1, 0);
  normalize_hist(m_h_ettopoclus20_data_verylowmet_signal_region_fourlayer);
  normalize_hist(m_h_ettopoclus20_data_lowmet_signal_region_fourlayer);
  normalize_hist(m_h_ettopoclus20_data_middlemet_signal_region_fourlayer);
  normalize_hist(m_h_ettopoclus20_data_highmet_signal_region_fourlayer);
  hists->push_back(m_h_ettopoclus20_data_verylowmet_signal_region_fourlayer);
  hists->push_back(m_h_ettopoclus20_data_lowmet_signal_region_fourlayer);
  hists->push_back(m_h_ettopoclus20_data_middlemet_signal_region_fourlayer);
  hists->push_back(m_h_ettopoclus20_data_highmet_signal_region_fourlayer);
  hist_names->push_back("#it{E}_{T}^{miss} < 100 GeV");
  hist_names->push_back("100 < #it{E}_{T}^{miss} < 150 GeV");
  hist_names->push_back("150 < #it{E}_{T}^{miss} < 200 GeV");
  hist_names->push_back("#it{E}_{T}^{miss} > 200 GeV");
  make_ratio_hist(hists, hist_names, false, false);
  decorate_hist({"#int L dt = 136 fb^{-1} #sqrt{s} = 13 TeV", "4-layer"});
  c_sqr->Print("m_h_ettopoclus20_data_verylowmet_signal_region_fourlayer.png");
  hists->clear();
  hist_names->clear();

  hist_config(m_h_ettopoclus20_data_verylowsubtractmet_signal_region_fourlayer, 1, 20, 1, 1, 1, 0);
  hist_config(m_h_ettopoclus20_data_lowsubtractmet_signal_region_fourlayer, 2, 20, 2, 1, 1, 0);
  hist_config(m_h_ettopoclus20_data_middlesubtractmet_signal_region_fourlayer, 4, 20, 4, 1, 1, 0);
  hist_config(m_h_ettopoclus20_data_highsubtractmet_signal_region_fourlayer, 210, 20, 210, 1, 1, 0);
  normalize_hist(m_h_ettopoclus20_data_verylowsubtractmet_signal_region_fourlayer);
  normalize_hist(m_h_ettopoclus20_data_lowsubtractmet_signal_region_fourlayer);
  normalize_hist(m_h_ettopoclus20_data_middlesubtractmet_signal_region_fourlayer);
  normalize_hist(m_h_ettopoclus20_data_highsubtractmet_signal_region_fourlayer);
  hists->push_back(m_h_ettopoclus20_data_verylowsubtractmet_signal_region_fourlayer);
  hists->push_back(m_h_ettopoclus20_data_lowsubtractmet_signal_region_fourlayer);
  hists->push_back(m_h_ettopoclus20_data_middlesubtractmet_signal_region_fourlayer);
  hists->push_back(m_h_ettopoclus20_data_highsubtractmet_signal_region_fourlayer);
  hist_names->push_back("#it{E}_{T}^{miss} < 100 GeV");
  hist_names->push_back("100 < #it{E}_{T}^{miss} < 150 GeV");
  hist_names->push_back("150 < #it{E}_{T}^{miss} < 200 GeV");
  hist_names->push_back("#it{E}_{T}^{miss} > 200 GeV");
  make_ratio_hist(hists, hist_names, false, false);
  decorate_hist({"#int L dt = 136 fb^{-1} #sqrt{s} = 13 TeV", "4-layer"});
  c_sqr->Print("m_h_ettopoclus20_data_verylowsubtractmet_signal_region_fourlayer.png");
  hists->clear();
  hist_names->clear();
}

void MyAnalysis::get_met_bias_hist() {
  m_h_ettopoclus20_met_data_allmet_signal_region_fourlayer = (TH2D*)m_file_data->Get("tracklet_ettopoclus20_met_allmet_signal_region_fourlayer");
  m_h_ettopoclus20_subtractmet_data_allmet_signal_region_fourlayer = (TH2D*)m_file_data->Get("tracklet_ettopoclus20_subtractmet_allmet_signal_region_fourlayer");
  m_h_ettopoclus20_data_verylowmet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_verylowmet_signal_region_fourlayer");
  m_h_ettopoclus20_data_lowmet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_lowmet_signal_region_fourlayer");
  m_h_ettopoclus20_data_middlemet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_middlemet_signal_region_fourlayer");
  m_h_ettopoclus20_data_highmet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_highmet_signal_region_fourlayer");
  m_h_ettopoclus20_data_verylowsubtractmet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_verylowsubtractmet_allmet_signal_region_fourlayer");
  m_h_ettopoclus20_data_lowsubtractmet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_lowsubtractmet_allmet_signal_region_fourlayer");
  m_h_ettopoclus20_data_middlesubtractmet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_middlesubtractmet_allmet_signal_region_fourlayer");
  m_h_ettopoclus20_data_highsubtractmet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_highsubtractmet_allmet_signal_region_fourlayer");
}
