#include "MyAnalysis.h"

void MyAnalysis::et_met_manager() {
  get_et_met_hist();
  print_et_met_hist();
}

void MyAnalysis::print_et_met_hist() {
  std::vector<TH1*>* hists = new std::vector<TH1*>;
  std::vector<std::string>* hist_names = new std::vector<std::string>;
  std::vector<std::string>* hist_types = new std::vector<std::string>;
  hist_config(m_h_ettopoclus20_highmet_signal_region_fourlayer, 1, 20, 1, 1, 1, 0);
  hist_config(m_h_ettopoclus20_middlemet_signal_region_fourlayer, 2, 21, 2, 1, 1, 0);
  hist_config(m_h_ettopoclus20_lowmet_signal_region_fourlayer, 4, 22, 4, 1, 1, 0);
  hist_config(m_h_ettopoclus20_verylowmet_signal_region_fourlayer, 210, 23, 210, 1, 1, 0);


  c_sqr->cd();

  normalize_hist(m_h_ettopoclus20_highmet_signal_region_fourlayer);  
  normalize_hist(m_h_ettopoclus20_middlemet_signal_region_fourlayer);  
  normalize_hist(m_h_ettopoclus20_lowmet_signal_region_fourlayer);  
  normalize_hist(m_h_ettopoclus20_verylowmet_signal_region_fourlayer);  
  hists->push_back(m_h_ettopoclus20_highmet_signal_region_fourlayer);
  hists->push_back(m_h_ettopoclus20_middlemet_signal_region_fourlayer);
  hists->push_back(m_h_ettopoclus20_lowmet_signal_region_fourlayer);
  hists->push_back(m_h_ettopoclus20_verylowmet_signal_region_fourlayer);
  hist_names->push_back("#it{E}_{T}^{miss} = (200, #infty)");
  hist_names->push_back("#it{E}_{T}^{miss} = (150, 200)");
  hist_names->push_back("#it{E}_{T}^{miss} = (100, 150)");
  hist_names->push_back("#it{E}_{T}^{miss} = (0, 100)");
  hist_types->push_back("data");
  hist_types->push_back("data");
  hist_types->push_back("data");
  hist_types->push_back("data");
  make_normal_hist(hists, hist_names, hist_types, false, false);
  decorate_hist({"#int L dt = 136 fb^{-1} #sqrt{s} = 13 TeV", "4-layer"});
  c_std->Print("m_h_ettopoclus20_lowtohighmet_signal_region_fourlayer.png");
  hists->clear();
  hist_names->clear();
  hist_types->clear();
}

void MyAnalysis::get_et_met_hist() {
  m_h_ettopoclus20_highmet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_highmet_signal_region_fourlayer");
  m_h_ettopoclus20_middlemet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_middlemet_signal_region_fourlayer");
  m_h_ettopoclus20_lowmet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_lowmet_signal_region_fourlayer");
  m_h_ettopoclus20_verylowmet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_verylowmet_signal_region_fourlayer");
}
