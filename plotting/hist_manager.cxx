#include "MyAnalysis.h"

void MyAnalysis::print_hist() {
  make_canvas();

  std::vector<TH1*>* hists = new std::vector<TH1*>;
  std::vector<std::string>* hist_names = new std::vector<std::string>;
  hist_config(m_h_ettopoclus20_zll_signal_region_fourlayer, 1, 20, 1, 1, 1, 0);
  hist_config(m_h_ettopoclus20_rvptcone_zll_signal_region_fourlayer, 2, 20, 2, 1, 0, 0);
  // hist_config(m_h_ettopoclus20_zll_highmet_signal_region_fourlayer, 1, 20, 1, 1, 1, 0);
  // hist_config(m_h_ettopoclus20_rvptcone_zll_highmet_signal_region_fourlayer, 2, 20, 2, 1, 0, 0);
  // hist_config(m_h_ettopoclus20_zll_lowmet_signal_region_fourlayer, 1, 20, 1, 1, 1, 0);
  // hist_config(m_h_ettopoclus20_rvptcone_zll_lowmet_signal_region_fourlayer, 2, 20, 2, 1, 0, 0);
  hist_config(m_h_ettopoclus20_lowmet_signal_region_fourlayer, 1, 20, 1, 1, 1, 0);
  hist_config(m_h_ettopoclus20_rvptcone_lowmet_signal_region_fourlayer, 2, 20, 2, 1, 0, 0);
  hist_config(m_h_ettopoclus20_highmet_signal_region_fourlayer, 1, 20, 1, 1, 1, 0);
  hist_config(m_h_ettopoclus20_rvptcone_highmet_signal_region_fourlayer, 2, 20, 2, 1, 0, 0);
  hist_config(m_h_ettopoclus20_zll_signal_region_threelayer, 1, 20, 1, 1, 1, 0);
  hist_config(m_h_ettopoclus20_rvptcone_zll_signal_region_threelayer, 2, 20, 2, 1, 0, 0);
  // hist_config(m_h_ettopoclus20_zll_highmet_signal_region_threelayer, 1, 20, 1, 1, 1, 0);
  // hist_config(m_h_ettopoclus20_rvptcone_zll_highmet_signal_region_threelayer, 2, 20, 2, 1, 0, 0);
  // hist_config(m_h_ettopoclus20_zll_lowmet_signal_region_threelayer, 1, 20, 1, 1, 1, 0);
  // hist_config(m_h_ettopoclus20_rvptcone_zll_lowmet_signal_region_threelayer, 2, 20, 2, 1, 0, 0);
  hist_config(m_h_ettopoclus20_lowmet_signal_region_threelayer, 1, 20, 1, 1, 1, 0);
  hist_config(m_h_ettopoclus20_rvptcone_lowmet_signal_region_threelayer, 2, 20, 2, 1, 0, 0);
  hist_config(m_h_ettopoclus20_highmet_signal_region_threelayer, 1, 20, 1, 1, 1, 0);
  hist_config(m_h_ettopoclus20_rvptcone_highmet_signal_region_threelayer, 2, 20, 2, 1, 0, 0);

  hist_config(m_h_eta_ex_caloveto_zll_signal_region_fourlayer, 1, 20, 1, 1, 1, 0);
  hist_config(m_h_eta_rvptcone_zll_signal_region_fourlayer, 2, 20, 2, 1, 0, 0);
  hist_config(m_h_eta_ex_caloveto_lowmet_signal_region_fourlayer, 1, 20, 1, 1, 1, 0);
  hist_config(m_h_eta_rvptcone_lowmet_signal_region_fourlayer, 2, 20, 2, 1, 0, 0);
  hist_config(m_h_eta_ex_caloveto_highmet_signal_region_fourlayer, 1, 20, 1, 1, 1, 0);
  hist_config(m_h_eta_rvptcone_highmet_signal_region_fourlayer, 2, 20, 2, 1, 0, 0);
  hist_config(m_h_pt_ex_caloveto_zll_signal_region_fourlayer, 1, 20, 1, 1, 1, 0);
  hist_config(m_h_pt_rvptcone_zll_signal_region_fourlayer, 2, 20, 2, 1, 0, 0);
  hist_config(m_h_pt_ex_caloveto_lowmet_signal_region_fourlayer, 1, 20, 1, 1, 1, 0);
  hist_config(m_h_pt_rvptcone_lowmet_signal_region_fourlayer, 2, 20, 2, 1, 0, 0);
  hist_config(m_h_pt_ex_caloveto_highmet_signal_region_fourlayer, 1, 20, 1, 1, 1, 0);
  hist_config(m_h_pt_rvptcone_highmet_signal_region_fourlayer, 2, 20, 2, 1, 0, 0);


  hists->push_back((TH1*)m_h_ettopoclus20_zll_signal_region_fourlayer);
  hists->push_back((TH1*)m_h_ettopoclus20_rvptcone_zll_signal_region_fourlayer);
  hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} < 0.04");
  hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} > 0.1");
  make_ratio_hist(hists, hist_names, false, false);
  p_upper->cd();
  decorate_hist({"#int L dt = 136 fb^{-1} #sqrt{s} = 13 TeV", "Z #rightarrow ll 4-layer CR"});
  c_sqr->Print("h_ettopoclus20_zll_signal_region_fourlayer.png");
  hists->clear();
  hist_names->clear();

  // hists->push_back((TH1*)m_h_ettopoclus20_zll_highmet_signal_region_fourlayer);
  // hists->push_back((TH1*)m_h_ettopoclus20_rvptcone_zll_highmet_signal_region_fourlayer);
  // hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} < 0.04");
  // hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} > 0.1");
  // make_ratio_hist(hists, hist_names, false, false);
  // p_upper->cd();
  // decorate_hist({"#int L dt = 136 fb^{-1} #sqrt{s} = 13 TeV", "Z #rightarrow ll 4-layer CR"});
  // c_sqr->Print("h_ettopoclus20_zll_highmet_signal_region_fourlayer.png");
  // hists->clear();
  // hist_names->clear();

  // hists->push_back((TH1*)m_h_ettopoclus20_zll_lowmet_signal_region_fourlayer);
  // hists->push_back((TH1*)m_h_ettopoclus20_rvptcone_zll_lowmet_signal_region_fourlayer);
  // hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} < 0.04");
  // hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} > 0.1");
  // make_ratio_hist(hists, hist_names, false, false);
  // p_upper->cd();
  // decorate_hist({"#int L dt = 136 fb^{-1} #sqrt{s} = 13 TeV", "Z #rightarrow ll 4-layer CR"});
  // c_sqr->Print("h_ettopoclus20_zll_lowmet_signal_region_fourlayer.png");
  // hists->clear();
  // hist_names->clear();

  hists->push_back((TH1*)m_h_ettopoclus20_lowmet_signal_region_fourlayer);
  hists->push_back((TH1*)m_h_ettopoclus20_rvptcone_lowmet_signal_region_fourlayer);
  hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} < 0.04");
  hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} > 0.1");
  make_ratio_hist(hists, hist_names, false, false);
  p_upper->cd();
  decorate_hist({"#int L dt = 136 fb^{-1} #sqrt{s} = 13 TeV", "low #it{E}_{T}^{miss} 4-layer CR"});
  c_sqr->Print("h_ettopoclus20_lowmet_signal_region_fourlayer.png");
  hists->clear();
  hist_names->clear();

  hists->push_back((TH1*)m_h_ettopoclus20_highmet_signal_region_fourlayer);
  hists->push_back((TH1*)m_h_ettopoclus20_rvptcone_highmet_signal_region_fourlayer);
  hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} < 0.04");
  hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} > 0.1");
  make_ratio_hist(hists, hist_names, false, false);
  p_upper->cd();
  decorate_hist({"#int L dt = 136 fb^{-1} #sqrt{s} = 13 TeV", "high #it{E}_{T}^{miss} 4-layer SR"});
  c_sqr->Print("h_ettopoclus20_highmet_signal_region_fourlayer.png");
  hists->clear();
  hist_names->clear();

  hists->push_back((TH1*)m_h_ettopoclus20_zll_signal_region_threelayer);
  hists->push_back((TH1*)m_h_ettopoclus20_rvptcone_zll_signal_region_threelayer);
  hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} < 0.04");
  hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} > 0.1");
  make_ratio_hist(hists, hist_names, false, false);
  p_upper->cd();
  decorate_hist({"#int L dt = 136 fb^{-1} #sqrt{s} = 13 TeV", "Z #rightarrow ll 4-layer CR"});
  c_sqr->Print("h_ettopoclus20_zll_signal_region_threelayer.png");
  hists->clear();
  hist_names->clear();

  // hists->push_back((TH1*)m_h_ettopoclus20_zll_highmet_signal_region_threelayer);
  // hists->push_back((TH1*)m_h_ettopoclus20_rvptcone_zll_highmet_signal_region_threelayer);
  // hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} < 0.04");
  // hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} > 0.1");
  // make_ratio_hist(hists, hist_names, false, false);
  // p_upper->cd();
  // decorate_hist({"#int L dt = 136 fb^{-1} #sqrt{s} = 13 TeV", "Z #rightarrow ll 4-layer CR"});
  // c_sqr->Print("h_ettopoclus20_zll_highmet_signal_region_threelayer.png");
  // hists->clear();
  // hist_names->clear();

  // hists->push_back((TH1*)m_h_ettopoclus20_zll_lowmet_signal_region_threelayer);
  // hists->push_back((TH1*)m_h_ettopoclus20_rvptcone_zll_lowmet_signal_region_threelayer);
  // hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} < 0.04");
  // hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} > 0.1");
  // make_ratio_hist(hists, hist_names, false, false);
  // p_upper->cd();
  // decorate_hist({"#int L dt = 136 fb^{-1} #sqrt{s} = 13 TeV", "Z #rightarrow ll 4-layer CR"});
  // c_sqr->Print("h_ettopoclus20_zll_lowmet_signal_region_threelayer.png");
  // hists->clear();
  // hist_names->clear();

  hists->push_back((TH1*)m_h_ettopoclus20_lowmet_signal_region_threelayer);
  hists->push_back((TH1*)m_h_ettopoclus20_rvptcone_lowmet_signal_region_threelayer);
  hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} < 0.04");
  hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} > 0.1");
  make_ratio_hist(hists, hist_names, false, false);
  p_upper->cd();
  decorate_hist({"#int L dt = 136 fb^{-1} #sqrt{s} = 13 TeV", "low #it{E}_{T}^{miss} 4-layer CR"});
  c_sqr->Print("h_ettopoclus20_lowmet_signal_region_threelayer.png");
  hists->clear();
  hist_names->clear();

  hists->push_back((TH1*)m_h_ettopoclus20_highmet_signal_region_threelayer);
  hists->push_back((TH1*)m_h_ettopoclus20_rvptcone_highmet_signal_region_threelayer);
  hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} < 0.04");
  hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} > 0.1");
  make_ratio_hist(hists, hist_names, false, false);
  p_upper->cd();
  decorate_hist({"#int L dt = 136 fb^{-1} #sqrt{s} = 13 TeV", "high #it{E}_{T}^{miss} 4-layer SR"});
  c_sqr->Print("h_ettopoclus20_highmet_signal_region_threelayer.png");
  hists->clear();
  hist_names->clear();

  hists->push_back((TH1*)m_h_eta_ex_caloveto_zll_signal_region_fourlayer);
  hists->push_back((TH1*)m_h_eta_rvptcone_zll_signal_region_fourlayer);
  hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} < 0.04");
  hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} > 0.1");
  make_ratio_hist(hists, hist_names, false, false);
  p_upper->cd();
  decorate_hist({"#int L dt = 136 fb^{-1} #sqrt{s} = 13 TeV", "Z #rightarrow ll 4-layer CR"});
  c_sqr->Print("h_eta_zll_signal_region_fourlayer.png");
  hists->clear();
  hist_names->clear();

  hists->push_back((TH1*)m_h_eta_ex_caloveto_lowmet_signal_region_fourlayer);
  hists->push_back((TH1*)m_h_eta_rvptcone_lowmet_signal_region_fourlayer);
  hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} < 0.04");
  hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} > 0.1");
  make_ratio_hist(hists, hist_names, false, false);
  p_upper->cd();
  decorate_hist({"#int L dt = 136 fb^{-1} #sqrt{s} = 13 TeV", "low #it{E}_{T}^{miss} 4-layer CR"});
  c_sqr->Print("h_eta_lowmet_signal_region_fourlayer.png");
  hists->clear();
  hist_names->clear();

  hists->push_back((TH1*)m_h_eta_ex_caloveto_highmet_signal_region_fourlayer);
  hists->push_back((TH1*)m_h_eta_rvptcone_highmet_signal_region_fourlayer);
  hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} < 0.04");
  hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} > 0.1");
  make_ratio_hist(hists, hist_names, false, false);
  p_upper->cd();
  decorate_hist({"#int L dt = 136 fb^{-1} #sqrt{s} = 13 TeV", "high #it{E}_{T}^{miss} 4-layer SR"});
  c_sqr->Print("h_eta_highmet_signal_region_fourlayer.png");
  hists->clear();
  hist_names->clear();

  hists->push_back((TH1*)m_h_pt_ex_caloveto_zll_signal_region_fourlayer);
  hists->push_back((TH1*)m_h_pt_rvptcone_zll_signal_region_fourlayer);
  hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} < 0.04");
  hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} > 0.1");
  make_ratio_hist(hists, hist_names, true, true);
  p_upper->cd();
  decorate_hist({"#int L dt = 136 fb^{-1} #sqrt{s} = 13 TeV", "Z #rightarrow ll 4-layer CR"});
  c_sqr->Print("h_pt_zll_signal_region_fourlayer.png");
  hists->clear();
  hist_names->clear();

  hists->push_back((TH1*)m_h_pt_ex_caloveto_lowmet_signal_region_fourlayer);
  hists->push_back((TH1*)m_h_pt_rvptcone_lowmet_signal_region_fourlayer);
  hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} < 0.04");
  hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} > 0.1");
  make_ratio_hist(hists, hist_names, true, true);
  p_upper->cd();
  decorate_hist({"#int L dt = 136 fb^{-1} #sqrt{s} = 13 TeV", "low #it{E}_{T}^{miss} 4-layer CR"});
  c_sqr->Print("h_pt_lowmet_signal_region_fourlayer.png");
  hists->clear();
  hist_names->clear();

  hists->push_back((TH1*)m_h_pt_ex_caloveto_highmet_signal_region_fourlayer);
  hists->push_back((TH1*)m_h_pt_rvptcone_highmet_signal_region_fourlayer);
  hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} < 0.04");
  hist_names->push_back("#it{p}_{T}^{cone40}/#it{p}_{T} > 0.1");
  make_ratio_hist(hists, hist_names, true, true);
  p_upper->cd();
  decorate_hist({"#int L dt = 136 fb^{-1} #sqrt{s} = 13 TeV", "high #it{E}_{T}^{miss} 4-layer SR"});
  c_sqr->Print("h_pt_highmet_signal_region_fourlayer.png");
  hists->clear();
  hist_names->clear();
}

void MyAnalysis::get_hist() {
  m_h_ettopoclus20_zll_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_zll_signal_region_fourlayer");
  m_h_ettopoclus20_rvptcone_zll_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_rvptcone_zll_signal_region_fourlayer");
  m_h_ettopoclus20_zll_highmet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_zll_highmet_signal_region_fourlayer");
  m_h_ettopoclus20_rvptcone_zll_highmet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_rvptcone_zll_highmet_signal_region_fourlayer");
  m_h_ettopoclus20_zll_lowmet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_zll_lowmet_signal_region_fourlayer");
  m_h_ettopoclus20_rvptcone_zll_lowmet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_rvptcone_zll_lowmet_signal_region_fourlayer");
  m_h_ettopoclus20_lowmet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_lowmet_signal_region_fourlayer");
  m_h_ettopoclus20_rvptcone_lowmet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_rvptcone_lowmet_signal_region_fourlayer");
  m_h_ettopoclus20_highmet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_highmet_signal_region_fourlayer");
  m_h_ettopoclus20_rvptcone_highmet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_rvptcone_highmet_signal_region_fourlayer");
  m_h_eta_ex_caloveto_zll_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_eta_ex_caloveto_zll_signal_region_fourlayer");
  m_h_eta_rvptcone_zll_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_eta_rvptcone_zll_signal_region_fourlayer");
  m_h_eta_ex_caloveto_lowmet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_eta_ex_caloveto_lowmet_signal_region_fourlayer");
  m_h_eta_rvptcone_lowmet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_eta_rvptcone_lowmet_signal_region_fourlayer");
  m_h_eta_ex_caloveto_highmet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_eta_ex_caloveto_highmet_signal_region_fourlayer");
  m_h_eta_rvptcone_highmet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_eta_rvptcone_highmet_signal_region_fourlayer");
  m_h_pt_ex_caloveto_zll_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_pt_ex_caloveto_zll_signal_region_fourlayer");
  m_h_pt_rvptcone_zll_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_pt_rvptcone_zll_signal_region_fourlayer");
  m_h_pt_ex_caloveto_lowmet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_pt_ex_caloveto_lowmet_signal_region_fourlayer");
  m_h_pt_rvptcone_lowmet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_pt_rvptcone_lowmet_signal_region_fourlayer");
  m_h_pt_ex_caloveto_highmet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_pt_ex_caloveto_highmet_signal_region_fourlayer");
  m_h_pt_rvptcone_highmet_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_pt_rvptcone_highmet_signal_region_fourlayer");
  m_h_pt_eta_ex_caloveto_zll_signal_region_fourlayer = (TH2D*)m_file_data->Get("tracklet_pt_eta_ex_caloveto_zll_signal_region_fourlayer");
  m_h_pt_eta_rvptcone_zll_signal_region_fourlayer = (TH2D*)m_file_data->Get("tracklet_pt_eta_rvptcone_zll_signal_region_fourlayer");
  m_h_pt_eta_ex_caloveto_lowmet_signal_region_fourlayer = (TH2D*)m_file_data->Get("tracklet_pt_eta_ex_caloveto_lowmet_signal_region_fourlayer");
  m_h_pt_eta_rvptcone_lowmet_signal_region_fourlayer = (TH2D*)m_file_data->Get("tracklet_pt_eta_rvptcone_lowmet_signal_region_fourlayer");
  m_h_pt_eta_ex_caloveto_highmet_signal_region_fourlayer = (TH2D*)m_file_data->Get("tracklet_pt_eta_ex_caloveto_highmet_signal_region_fourlayer");
  m_h_pt_eta_rvptcone_highmet_signal_region_fourlayer = (TH2D*)m_file_data->Get("tracklet_pt_eta_rvptcone_highmet_signal_region_fourlayer");


  m_h_ettopoclus20_zll_signal_region_threelayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_zll_signal_region_threelayer");
  m_h_ettopoclus20_rvptcone_zll_signal_region_threelayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_rvptcone_zll_signal_region_threelayer");
  m_h_ettopoclus20_zll_highmet_signal_region_threelayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_zll_highmet_signal_region_threelayer");
  m_h_ettopoclus20_rvptcone_zll_highmet_signal_region_threelayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_rvptcone_zll_highmet_signal_region_threelayer");
  m_h_ettopoclus20_zll_lowmet_signal_region_threelayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_zll_lowmet_signal_region_threelayer");
  m_h_ettopoclus20_rvptcone_zll_lowmet_signal_region_threelayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_rvptcone_zll_lowmet_signal_region_threelayer");
  m_h_ettopoclus20_lowmet_signal_region_threelayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_lowmet_signal_region_threelayer");
  m_h_ettopoclus20_rvptcone_lowmet_signal_region_threelayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_rvptcone_lowmet_signal_region_threelayer");
  m_h_ettopoclus20_highmet_signal_region_threelayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_highmet_signal_region_threelayer");
  m_h_ettopoclus20_rvptcone_highmet_signal_region_threelayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_rvptcone_highmet_signal_region_threelayer");


  // d0sideband
  m_h_ettopoclus20_zll_d0sideband_fourlayer = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_zll_d0sideband_fourlayer");
  m_h_pt_zll_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_pt_zll_signal_region_fourlayer");
  m_h_pt_zll_d0sideband_fourlayer = (TH1D*)m_file_data->Get("tracklet_pt_zll_d0sideband_fourlayer");
}
