void hist_config(TH1*, int, int, int, int, int, int);
void decorate_hist(std::vector<std::string>);
void decorate_legend(TLegend*);
TH1* make_ratio_hist(TH1*, TH1*, string);
void zll_hadron_purity_check() {
  TCanvas* c_std = new TCanvas("c_std", "c_std", 800., 600.);

  TFile* m_file_mc = TFile::Open("../../file/zmumu_official_not_skim_noweight.root");
  TFile* m_file_data = TFile::Open("../../file/dataAllYear.merged.hist.root");
  //TFile* m_file_data = TFile::Open("../file/data18.merged.hist.root");

  // ettopoclus20
  // TH1D* m_h_topoclus20_rvptcone_data = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_rvptcone_zll_signal_region_fourlayer");
  // TH1D* m_h_topoclus20_rvptcone_mc_hastruth = (TH1D*)m_file_mc->Get("tracklet_ettopoclus20_hastruth_rvptcone_zll_signal_region_fourlayer");
  // TH1D* m_h_topoclus20_rvptcone_mc_not_hastruth = (TH1D*)m_file_mc->Get("tracklet_ettopoclus20_not_hastruth_rvptcone_zll_signal_region_fourlayer");
  // double scale_ettopoclus20_rvptcone = m_h_topoclus20_rvptcone_data->Integral()/(m_h_topoclus20_rvptcone_mc_hastruth->Integral() + m_h_topoclus20_rvptcone_mc_not_hastruth->Integral());
  // hist_config(m_h_topoclus20_rvptcone_mc_hastruth, 0, 0, 1, 1, 4, 0);
  // hist_config(m_h_topoclus20_rvptcone_mc_not_hastruth, 0, 0, 1, 1, 0, 0);
  // m_h_topoclus20_rvptcone_mc_not_hastruth->Scale(scale_ettopoclus20_rvptcone);
  // m_h_topoclus20_rvptcone_mc_hastruth->Scale(scale_ettopoclus20_rvptcone);
  // THStack* m_hs_topoclus20_rvptcone_mc = new THStack("hs_topoclus20_rvptcone_mc", ";tracklet #it{E}_{T}^{topoclus20} [GeV]; Events");
  // m_hs_topoclus20_rvptcone_mc->Add(m_h_topoclus20_rvptcone_mc_not_hastruth);
  // m_hs_topoclus20_rvptcone_mc->Add(m_h_topoclus20_rvptcone_mc_hastruth);
  // m_hs_topoclus20_rvptcone_mc->Draw("hist");
  // m_h_topoclus20_rvptcone_data->Draw("same");


  // TH1D* m_h_ptcone40overpt_data = (TH1D*)m_file_data->Get("tracklet_ptcone40overpt_zll_signal_region_fourlayer");
  // TH1D* m_h_ptcone40overpt_mc_hastruth = (TH1D*)m_file_mc->Get("tracklet_ptcone40overpt_hastruth_zll_signal_region_fourlayer");
  // TH1D* m_h_ptcone40overpt_mc_not_hastruth = (TH1D*)m_file_mc->Get("tracklet_ptcone40overpt_not_hastruth_zll_signal_region_fourlayer");
  // double scale_ptcone40overpt_rvptcone = m_h_ptcone40overpt_data->Integral()/(m_h_ptcone40overpt_mc_hastruth->Integral() + m_h_ptcone40overpt_mc_not_hastruth->Integral());
  // hist_config(m_h_ptcone40overpt_mc_hastruth, 0, 0, 1, 1, 4, 0);
  // hist_config(m_h_ptcone40overpt_mc_not_hastruth, 0, 0, 1, 1, 0, 0);
  // m_h_ptcone40overpt_mc_not_hastruth->Scale(scale_ptcone40overpt_rvptcone);
  // m_h_ptcone40overpt_mc_hastruth->Scale(scale_ptcone40overpt_rvptcone);
  // THStack* m_hs_ptcone40overpt_mc = new THStack("hs_ptcone40overpt_mc", ";tracklet #it{p}_{T}^{cone40}/#it{p}_{T}; Events");
  // m_hs_ptcone40overpt_mc->Add(m_h_ptcone40overpt_mc_not_hastruth);
  // m_hs_ptcone40overpt_mc->Add(m_h_ptcone40overpt_mc_hastruth);
  // m_hs_ptcone40overpt_mc->Draw("hist");
  // m_h_ptcone40overpt_data->Draw("same");

  // TH1D* m_h_ettopoclus20_z0sideband_data = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_zll_z0sideband_fourlayer");
  // TH1D* m_h_ettopoclus20_z0sideband_mc_hastruth = (TH1D*)m_file_mc->Get("tracklet_ettopoclus20_hastruth_zll_z0sideband_fourlayer");
  // TH1D* m_h_ettopoclus20_z0sideband_mc_not_hastruth = (TH1D*)m_file_mc->Get("tracklet_ettopoclus20_not_hastruth_zll_z0sideband_fourlayer");
  // double scale_ettopoclus20_z0sideband_rvptcone = m_h_ettopoclus20_z0sideband_data->Integral()/(m_h_ettopoclus20_z0sideband_mc_hastruth->Integral() + m_h_ettopoclus20_z0sideband_mc_not_hastruth->Integral());
  // hist_config(m_h_ettopoclus20_z0sideband_mc_hastruth, 0, 0, 1, 1, 4, 0);
  // hist_config(m_h_ettopoclus20_z0sideband_mc_not_hastruth, 0, 0, 1, 1, 0, 0);
  // m_h_ettopoclus20_z0sideband_mc_not_hastruth->Scale(scale_ettopoclus20_z0sideband_rvptcone);
  // m_h_ettopoclus20_z0sideband_mc_hastruth->Scale(scale_ettopoclus20_z0sideband_rvptcone);
  // THStack* m_hs_ettopoclus20_z0sideband_mc = new THStack("hs_ettopoclus20_z0sideband_mc", ";tracklet #it{p}_{T}^{cone40}/#it{p}_{T}; Events");
  // m_hs_ettopoclus20_z0sideband_mc->Add(m_h_ettopoclus20_z0sideband_mc_not_hastruth);
  // m_hs_ettopoclus20_z0sideband_mc->Add(m_h_ettopoclus20_z0sideband_mc_hastruth);
  // m_hs_ettopoclus20_z0sideband_mc->Draw("hist");
  // m_h_ettopoclus20_z0sideband_data->Draw("same");
  
  TH1D* m_h_topoclus20_rvptcone_data = (TH1D*)m_file_data->Get("tracklet_ettopoclus20_rvptcone_zll_signal_region_threelayer");
  TH1D* m_h_topoclus20_rvptcone_mc_hastruth = (TH1D*)m_file_mc->Get("tracklet_ettopoclus20_hastruth_rvptcone_zll_signal_region_threelayer");
  TH1D* m_h_topoclus20_rvptcone_mc_not_hastruth = (TH1D*)m_file_mc->Get("tracklet_ettopoclus20_not_hastruth_rvptcone_zll_signal_region_threelayer");
  double scale_ettopoclus20_rvptcone = m_h_topoclus20_rvptcone_data->Integral()/(m_h_topoclus20_rvptcone_mc_hastruth->Integral() + m_h_topoclus20_rvptcone_mc_not_hastruth->Integral());
  hist_config(m_h_topoclus20_rvptcone_mc_hastruth, 0, 0, 1, 1, 4, 0);
  hist_config(m_h_topoclus20_rvptcone_mc_not_hastruth, 0, 0, 1, 1, 0, 0);
  m_h_topoclus20_rvptcone_mc_not_hastruth->Scale(scale_ettopoclus20_rvptcone);
  m_h_topoclus20_rvptcone_mc_hastruth->Scale(scale_ettopoclus20_rvptcone);
  THStack* m_hs_topoclus20_rvptcone_mc = new THStack("hs_topoclus20_rvptcone_mc", ";tracklet #it{E}_{T}^{topoclus20} [GeV]; Events");
  m_hs_topoclus20_rvptcone_mc->Add(m_h_topoclus20_rvptcone_mc_not_hastruth);
  m_hs_topoclus20_rvptcone_mc->Add(m_h_topoclus20_rvptcone_mc_hastruth);
  m_h_topoclus20_rvptcone_data->GetYaxis()->SetRangeUser(0., 2200.);
  m_h_topoclus20_rvptcone_data->Draw("");
  m_hs_topoclus20_rvptcone_mc->Draw("hist same");
  m_h_topoclus20_rvptcone_data->Draw("same");
  c_std->Print("canvas.png");
}

void decorate_legend(TLegend* legend){
  legend->SetBorderSize(0);
  legend->SetFillStyle(0);
  legend->SetTextSize(0.035);
  legend->SetTextFont(42);
}

void hist_config(TH1* hist, int marker_color, int marker_style, int line_color, int line_style, int fill_color, int fill_style){
  hist->SetMarkerColor(marker_color);
  hist->SetMarkerStyle(marker_style);
  hist->SetLineColor(line_color);
  hist->SetLineStyle(line_style);
  if(fill_color > 0) {
    hist->SetFillColor(fill_color);
  }
  if(fill_style > 0) {
    hist->SetFillStyle(fill_style);
  }
}

void decorate_hist(std::vector<std::string> text){
  ATLASLabel(0.2, 0.88, "Internal");
  TLatex* tex_text;
  for(unsigned ii_text = 0; ii_text < text.size(); ++ii_text) {
    tex_text = new TLatex(0.2, 0.82-0.06*(float)ii_text, text.at(ii_text).c_str());
    tex_text->SetNDC();
    tex_text->SetTextSize(0.035);
    tex_text->Draw();
  }
}

void change_hist_title(TH1* hist, std::string xtitle, std::string ytitle){
  hist->GetXaxis()->SetTitle(xtitle.c_str());
  hist->GetYaxis()->SetTitle(ytitle.c_str());
}

TH1* make_ratio_hist(TH1* numerator, TH1* denominator, string name){
  TH1D* numerator_copy = (TH1D*)numerator->Clone();
  numerator_copy->SetName(name.c_str());
  if(numerator->GetNbinsX() != denominator->GetNbinsX()) std::cout<<"nbin is not correct."<<std::endl;
  for(unsigned ii_bin = 0; ii_bin < numerator->GetNbinsX(); ++ii_bin) {
    if(denominator->GetBinContent(ii_bin+1) == 0.){
      numerator_copy->SetBinContent(ii_bin, 0);
      continue;
    }
    numerator_copy->SetBinContent(ii_bin+1, numerator->GetBinContent(ii_bin+1)/denominator->GetBinContent(ii_bin+1));
    numerator_copy->SetBinError(ii_bin+1, numerator_copy->GetBinError(ii_bin+1)*numerator_copy->GetBinContent(ii_bin+1)/numerator->GetBinContent(ii_bin+1));
  }
  numerator_copy->SetLabelSize(0.05);
  numerator_copy->SetTitleSize(0.05);
  numerator_copy->SetNdivisions(504, "Y");
  return numerator_copy;
}
