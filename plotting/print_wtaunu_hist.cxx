#include "MyAnalysis.h"

void MyAnalysis::wtaunu_manager() {
  set_color_palette();
  open_wlnu_file();
  //open_zll_file();
  get_wtaunu_hist();
  print_wtaunu_hist();
}

void MyAnalysis::print_wtaunu_hist() {
  std::vector<TH1*>* hists = new std::vector<TH1*>;
  std::vector<std::string>* hist_names = new std::vector<std::string>;
  std::vector<std::string>* hist_types = new std::vector<std::string>;
  hist_config(m_h_ettopoclus20_zll_signal_region_fourlayer, 1, 20, 1, 1, 1, 0);
  hist_config(m_h_ettopoclus20_tautrack_wtaunu_signal_region_fourlayer, 2, 20, 2, 1, 0, 0);
  hist_config(m_h_ettopoclus20_tautrack_met0to50_wtaunu_signal_region_fourlayer, 1, 20, 1, 1, 0, 0);
  hist_config(m_h_ettopoclus20_tautrack_met50to100_wtaunu_signal_region_fourlayer, 2, 20, 2, 1, 0, 0);
  hist_config(m_h_ettopoclus20_tautrack_met100to_wtaunu_signal_region_fourlayer, 4, 20, 4, 1, 0, 0);
  hist_config(m_h_ptcone40overpt_zll_signal_region_fourlayer, 1, 20, 1, 1, 1, 0);
  hist_config(m_h_ptcone40overpt_tautrack_wtaunu_signal_region_fourlayer, 2, 20, 2, 1, 0, 0);
  hist_config(m_h_pt_zll_signal_region_fourlayer, 1, 20, 1, 1, 1, 0);
  hist_config(m_h_pt_tautrack_wtaunu_signal_region_fourlayer, 2, 20, 2, 1, 0, 0);
  hist_config(m_h_event_met_wtaunu, 1, 20, 1, 1, 0, 0);
  hist_config(m_h_met_tautrack_wtaunu_signal_region_fourlayer, 2, 20, 2, 1, 0, 0);
  hist_config(m_h_eta_zll_signal_region_fourlayer, 1, 20, 1, 1, 1, 0);
  hist_config(m_h_eta_tautrack_wtaunu_signal_region_fourlayer, 2, 20, 2, 1, 0, 0);
  hist_config(m_h_reco_mt_event, 1, 20, 1, 1, 0, 0);
  hist_config(m_h_mt_tautrack_wtaunu_signal_region_fourlayer, 4, 20, 4, 1, 0, 0);
  hist_config(m_h_mt_excaloveto_tautrack_wtaunu_signal_region_fourlayer, 2, 20, 2, 1, 0, 0);



  c_std->cd();
  c_std->SetRightMargin(0.2);

  m_h_truthpt_ettopoclus20_tautrack_wtaunu_signal_region_fourlayer->Draw("colz");
  TLine* line_proportion_truthpt_ettopoclus20 = new TLine(0., 0., 50., 50.);
  line_proportion_truthpt_ettopoclus20->SetLineWidth(2);
  line_proportion_truthpt_ettopoclus20->SetLineStyle(2);
  line_proportion_truthpt_ettopoclus20->Draw();
  c_std->Print("h_truthpt_ettopoclus20_tautrack_wtaunu_signal_region_fourlayer.png");

  m_h_truthpt_ettopoclus20_nopi0_tautrack_wtaunu_signal_region_fourlayer->Draw("box");
  m_h_truthpt_ettopoclus20_pi0_tautrack_wtaunu_signal_region_fourlayer->Draw("box same");
  m_h_truthpt_ettopoclus20_nopi0_tautrack_wtaunu_signal_region_fourlayer->SetLineColor(2);
  m_h_truthpt_ettopoclus20_pi0_tautrack_wtaunu_signal_region_fourlayer->SetLineColor(4);
  line_proportion_truthpt_ettopoclus20->Draw();
  c_std->Print("h_truthpt_ettopoclus20_pi0_tautrack_wtaunu_signal_region_fourlayer.png");

  m_h_ettopoclus20_met_tautrack_wtaunu_signal_region_fourlayer->Draw("colz");
  TLine* line_proportion_ettopoclus20_met = new TLine(0., 0., 50., 50.);
  line_proportion_ettopoclus20_met->SetLineWidth(2);
  line_proportion_ettopoclus20_met->SetLineStyle(2);
  line_proportion_ettopoclus20_met->Draw();
  c_std->Print("h_ettopoclus20_met_tautrack_wtaunu_signal_region_fourlayer.png");

  m_h_ettopoclus20_ettopoclus40_tautrack_wtaunu_signal_region_fourlayer->Draw("colz");
  TLine* line_proportion_ettopoclus20_ettopoclus40 = new TLine(0., 0., 50., 50.);
  line_proportion_ettopoclus20_ettopoclus40->SetLineWidth(2);
  line_proportion_ettopoclus20_ettopoclus40->SetLineStyle(2);
  line_proportion_ettopoclus20_ettopoclus40->Draw();
  c_std->Print("h_ettopoclus20_ettopoclus40_tautrack_wtaunu_signal_region_fourlayer.png");

  m_h_truthpt_met_tautrack_wtaunu_signal_region_fourlayer->Draw("colz");
  TLine* line_proportion_truthpt_met = new TLine(0., 0., 100., 100.);
  line_proportion_truthpt_met->SetLineWidth(2);
  line_proportion_truthpt_met->SetLineStyle(2);
  line_proportion_truthpt_met->Draw();
  c_std->Print("h_truthpt_met_tautrack_wtaunu_signal_region_fourlayer.png");

  m_h_truth_pt_tautrack_truthtau_wtaunu_signal_region_fourlayer->SetLineColor(2);
  m_h_truth_pt_tautrack_truthtau_excaloveto_wtaunu_signal_region_fourlayer->SetLineColor(4);
  m_h_truth_pt_tautrack_truthtau_wtaunu_signal_region_fourlayer->Draw("box");
  m_h_truth_pt_tautrack_truthtau_excaloveto_wtaunu_signal_region_fourlayer->Draw("box same");
  TLine* line_proportion_tautrack_truthtau = new TLine(0., 0., 100., 100.);
  line_proportion_tautrack_truthtau->SetLineWidth(2);
  line_proportion_tautrack_truthtau->SetLineStyle(2);
  line_proportion_tautrack_truthtau->Draw();
  c_std->Print("h_truth_pt_tautrack_truthtau_wtaunu_signal_region_fourlayer.png");

  m_h_ettopoclus20_tautrack_tauvispt_wtaunu_signal_region_fourlayer->Draw("colz");
  TLine* line_proportion_tautrack_tauvispt = new TLine(0., 0., 50., 50.);
  line_proportion_tautrack_tauvispt->SetLineWidth(2);
  line_proportion_tautrack_tauvispt->SetLineStyle(2);
  line_proportion_tautrack_tauvispt->Draw();
  c_std->Print("h_ettopoclus20_tautrack_tauvispt_wtaunu_signal_region_fourlayer.png");

  c_std->SetRightMargin(0.1);
  normalize_hist(m_h_ettopoclus20_tautrack_met0to50_wtaunu_signal_region_fourlayer);
  normalize_hist(m_h_ettopoclus20_tautrack_met50to100_wtaunu_signal_region_fourlayer);
  normalize_hist(m_h_ettopoclus20_tautrack_met100to_wtaunu_signal_region_fourlayer);
  hists->push_back(m_h_ettopoclus20_tautrack_met0to50_wtaunu_signal_region_fourlayer);
  hists->push_back(m_h_ettopoclus20_tautrack_met50to100_wtaunu_signal_region_fourlayer);
  hists->push_back(m_h_ettopoclus20_tautrack_met100to_wtaunu_signal_region_fourlayer);
  hist_names->push_back("#it{E}_{T}^{miss} < 50 GeV");
  hist_names->push_back("50 < #it{E}_{T}^{miss} < 100 GeV");
  hist_names->push_back("#it{E}_{T}^{miss} > 100 GeV");
  make_normal_hist(hists, hist_names, false, false);
  c_std->Print("m_h_ettopoclus20_tautrack_met0to50_wtaunu_signal_region_fourlayer.png");
  hists->clear();
  hist_names->clear();

  hists->push_back(m_h_decayr_tautrack_wtaunu_signal_region_fourlayer);
  hist_names->push_back("W #rightarrow #tau#nu");
  make_normal_hist(hists, hist_names, false, false);
  c_std->Print("m_h_decayr_tautrack_wtaunu_signal_region_fourlayer.png");
  hists->clear();
  hist_names->clear();

  hists->push_back(m_h_deltaphi_met_tautrack_wtaunu_signal_region_fourlayer);
  hist_names->push_back("W #rightarrow #tau#nu mc");
  make_normal_hist(hists, hist_names, false, false);
  c_std->Print("m_h_deltaphi_met_tautrack_wtaunu_signal_region_fourlayer.png");
  hists->clear();
  hist_names->clear();

  hists->push_back(m_h_truth_pt_tautrack_wtaunu_signal_region_fourlayer);
  hists->push_back(m_h_truth_pt_truthtau_wtaunu_signal_region_fourlayer);
  hist_names->push_back("truth tau track");
  hist_names->push_back("truth tau jet");
  make_normal_hist(hists, hist_names, false, false);
  c_std->Print("m_h_pt_tautrack_taujet_wtaunu_signal_region_fourlayer.png");
  hists->clear();
  hist_names->clear();

  c_std->SetLogy();
  hists->push_back(m_h_event_met_wtaunu);
  hists->push_back(m_h_met_tautrack_wtaunu_signal_region_fourlayer);
  hist_names->push_back("all");
  hist_names->push_back("w/ signal candidate");
  make_normal_hist(hists, hist_names, false, true);
  c_std->Print("m_h_met_wtaunu_signal_region_fourlayer.png");
  hists->clear();
  hist_names->clear();
  c_std->SetLogy(0);


  m_h_reco_mt_event->Scale(10./m_h_reco_mt_event->Integral());
  normalize_hist(m_h_mt_tautrack_wtaunu_signal_region_fourlayer);
  normalize_hist(m_h_mt_excaloveto_tautrack_wtaunu_signal_region_fourlayer);
  hists->push_back(m_h_reco_mt_event);
  hists->push_back(m_h_mt_tautrack_wtaunu_signal_region_fourlayer);
  hists->push_back(m_h_mt_excaloveto_tautrack_wtaunu_signal_region_fourlayer);
  hist_names->push_back("reco tau");
  hist_names->push_back("w/ caloveto");
  hist_names->push_back("w/o caloveto");
  make_normal_hist(hists, hist_names, false, false);
  c_std->Print("m_h_mt_tautrack_wtaunu_signal_region_fourlayer.png");
  hists->clear();
  hist_names->clear();
  
  c_sqr->cd();
  normalize_hist(m_h_ettopoclus20_zll_signal_region_fourlayer);
  normalize_hist(m_h_ettopoclus20_tautrack_wtaunu_signal_region_fourlayer);
  hists->push_back((TH1*)m_h_ettopoclus20_zll_signal_region_fourlayer);
  hists->push_back((TH1*)m_h_ettopoclus20_tautrack_wtaunu_signal_region_fourlayer);
  hist_names->push_back("Z #rightarrow ll data");
  hist_names->push_back("W #rightarrow #tau#nu mc");
  hist_types->push_back("data");
  hist_types->push_back("mc");
  make_ratio_hist(hists, hist_names, hist_types, false, false);
  p_upper->cd();
  // decorate_hist({"#int L dt = 136 fb^{-1} #sqrt{s} = 13 TeV", "Z #rightarrow ll 4-layer CR"});
  c_sqr->Print("h_ettopoclus20_wtaunu_signal_region_fourlayer.png");
  hists->clear();
  hist_names->clear();
  hist_types->clear();

  normalize_hist(m_h_ptcone40overpt_zll_signal_region_fourlayer);
  normalize_hist(m_h_ptcone40overpt_tautrack_wtaunu_signal_region_fourlayer);
  hists->push_back((TH1*)m_h_ptcone40overpt_zll_signal_region_fourlayer);
  hists->push_back((TH1*)m_h_ptcone40overpt_tautrack_wtaunu_signal_region_fourlayer);
  hist_names->push_back("Z #rightarrow ll data");
  hist_names->push_back("W #rightarrow #tau#nu mc");
  hist_types->push_back("data");
  hist_types->push_back("mc");
  make_ratio_hist(hists, hist_names, hist_types, false, false);
  p_upper->cd();
  // decorate_hist({"#int L dt = 136 fb^{-1} #sqrt{s} = 13 TeV", "Z #rightarrow ll 4-layer CR"});
  c_sqr->Print("h_ptcone40overpt_wtaunu_signal_region_fourlayer.png");
  hists->clear();
  hist_names->clear();
  hist_types->clear();

  normalize_hist(m_h_pt_zll_signal_region_fourlayer);
  normalize_hist(m_h_pt_tautrack_wtaunu_signal_region_fourlayer);
  hists->push_back((TH1*)m_h_pt_zll_signal_region_fourlayer);
  hists->push_back((TH1*)m_h_pt_tautrack_wtaunu_signal_region_fourlayer);
  hist_names->push_back("Z #rightarrow ll data");
  hist_names->push_back("W #rightarrow #tau#nu mc");
  hist_types->push_back("data");
  hist_types->push_back("mc");
  make_ratio_hist(hists, hist_names, hist_types, true, true);
  p_upper->cd();
  // decorate_hist({"#int L dt = 136 fb^{-1} #sqrt{s} = 13 TeV", "Z #rightarrow ll 4-layer CR"});
  c_sqr->Print("h_pt_wtaunu_signal_region_fourlayer.png");
  hists->clear();
  hist_names->clear();
  hist_types->clear();

  normalize_hist(m_h_eta_zll_signal_region_fourlayer);
  normalize_hist(m_h_eta_tautrack_wtaunu_signal_region_fourlayer);
  hists->push_back((TH1*)m_h_eta_zll_signal_region_fourlayer);
  hists->push_back((TH1*)m_h_eta_tautrack_wtaunu_signal_region_fourlayer);
  hist_names->push_back("Z #rightarrow ll data");
  hist_names->push_back("W #rightarrow #tau#nu mc");
  hist_types->push_back("data");
  hist_types->push_back("mc");
  make_ratio_hist(hists, hist_names, hist_types, false, false);
  p_upper->cd();
  // decorate_hist({"#int L dt = 136 fb^{-1} #sqrt{s} = 13 TeV", "Z #rightarrow ll 4-layer CR"});
  c_sqr->Print("h_eta_wtaunu_signal_region_fourlayer.png");
  hists->clear();
  hist_names->clear();
  hist_types->clear();
}

void MyAnalysis::get_wtaunu_hist() {
  m_h_met_tautrack_wtaunu_signal_region_fourlayer = (TH1D*)m_file_wlnu->Get("tracklet_met_tautrack_wlnu_signal_region_fourlayer");
  m_h_event_met_wtaunu = (TH1D*)m_file_wlnu->Get("event_met");
  m_h_pt_tautrack_wtaunu_signal_region_fourlayer = (TH1D*)m_file_wlnu->Get("tracklet_pt_tautrack_wlnu_signal_region_fourlayer");
  m_h_truth_pt_truthtau_wtaunu_signal_region_fourlayer = (TH1D*)m_file_wlnu->Get("tracklet_truth_pt_truthtau_wlnu_signal_region_fourlayer");
  m_h_truth_pt_tautrack_wtaunu_signal_region_fourlayer = (TH1D*)m_file_wlnu->Get("tracklet_truth_pt_tautrack_wlnu_signal_region_fourlayer");
  m_h_reco_mt_event = (TH1D*)m_file_wlnu->Get("event_reco_mt");
  m_h_mt_tautrack_wtaunu_signal_region_fourlayer = (TH1D*)m_file_wlnu->Get("tracklet_mt_tautrack_wlnu_signal_region_fourlayer");
  m_h_mt_excaloveto_tautrack_wtaunu_signal_region_fourlayer = (TH1D*)m_file_wlnu->Get("tracklet_mt_tautrack_excaloveto_wlnu_signal_region_fourlayer");
  m_h_eta_tautrack_wtaunu_signal_region_fourlayer = (TH1D*)m_file_wlnu->Get("tracklet_eta_tautrack_wlnu_signal_region_fourlayer");
  m_h_vcpt_tautrack_wtaunu_signal_region_fourlayer = (TH1D*)m_file_wlnu->Get("tracklet_vcpt_tautrack_wlnu_signal_region_fourlayer");
  m_h_d0_tautrack_wtaunu_signal_region_fourlayer = (TH1D*)m_file_wlnu->Get("tracklet_d0_tautrack_wlnu_signal_region_fourlayer");
  m_h_z0Sin_tautrack_wtaunu_signal_region_fourlayer = (TH1D*)m_file_wlnu->Get("tracklet_z0Sin_tautrack_wlnu_signal_region_fourlayer");
  m_h_decayr_tautrack_wtaunu_signal_region_fourlayer = (TH1D*)m_file_wlnu->Get("tracklet_decayr_tautrack_wlnu_signal_region_fourlayer");
  m_h_deltaphi_met_tautrack_wtaunu_signal_region_fourlayer = (TH1D*)m_file_wlnu->Get("tracklet_deltaphi_met_tautrack_wlnu_signal_region_fourlayer");
  m_h_ptcone40overpt_tautrack_wtaunu_signal_region_fourlayer = (TH1D*)m_file_wlnu->Get("tracklet_ptcone40overpt_tautrack_wlnu_signal_region_fourlayer");
  m_h_ettopoclus20_tautrack_wtaunu_signal_region_fourlayer = (TH1D*)m_file_wlnu->Get("tracklet_ettopoclus20_tautrack_wlnu_signal_region_fourlayer");
  m_h_ettopoclus20_tautrack_met0to50_wtaunu_signal_region_fourlayer = (TH1D*)m_file_wlnu->Get("tracklet_ettopoclus20_tautrack_met0to50_wlnu_signal_region_fourlayer");
  m_h_ettopoclus20_tautrack_met50to100_wtaunu_signal_region_fourlayer = (TH1D*)m_file_wlnu->Get("tracklet_ettopoclus20_tautrack_met50to100_wlnu_signal_region_fourlayer");
  m_h_ettopoclus20_tautrack_met100to_wtaunu_signal_region_fourlayer = (TH1D*)m_file_wlnu->Get("tracklet_ettopoclus20_tautrack_met100to_wlnu_signal_region_fourlayer");
  m_h_ptcone40overpt_zll_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_ptcone40overpt_zll_signal_region_fourlayer");
  m_h_eta_zll_signal_region_fourlayer = (TH1D*)m_file_data->Get("tracklet_eta_zll_signal_region_fourlayer");
  m_h_truthpt_ettopoclus20_tautrack_wtaunu_signal_region_fourlayer = (TH2D*)m_file_wlnu->Get("tracklet_truthpt_ettopoclus20_tautrack_wlnu_signal_region_fourlayer");
  m_h_truthpt_ettopoclus20_pi0_tautrack_wtaunu_signal_region_fourlayer = (TH2D*)m_file_wlnu->Get("tracklet_truthpt_ettopoclus20_tautrack_pi0_wlnu_signal_region_fourlayer");
  m_h_truthpt_ettopoclus20_nopi0_tautrack_wtaunu_signal_region_fourlayer = (TH2D*)m_file_wlnu->Get("tracklet_truthpt_ettopoclus20_tautrack_nopi0_wlnu_signal_region_fourlayer");
  m_h_ettopoclus20_met_tautrack_wtaunu_signal_region_fourlayer = (TH2D*)m_file_wlnu->Get("tracklet_ettopoclus20_met_tautrack_wlnu_signal_region_fourlayer");
  m_h_ettopoclus20_ettopoclus40_tautrack_wtaunu_signal_region_fourlayer = (TH2D*)m_file_wlnu->Get("tracklet_ettopoclus20_ettopoclus40_tautrack_wlnu_signal_region_fourlayer");
  m_h_truthpt_met_tautrack_wtaunu_signal_region_fourlayer = (TH2D*)m_file_wlnu->Get("tracklet_truthpt_met_tautrack_wlnu_signal_region_fourlayer");
  m_h_truth_pt_tautrack_truthtau_wtaunu_signal_region_fourlayer = (TH2D*)m_file_wlnu->Get("tracklet_truth_pt_tautrack_truthtau_wlnu_signal_region_fourlayer");
  m_h_truth_pt_tautrack_truthtau_excaloveto_wtaunu_signal_region_fourlayer = (TH2D*)m_file_wlnu->Get("tracklet_truth_pt_tautrack_truthtau_excaloveto_wlnu_signal_region_fourlayer");
  m_h_ettopoclus20_tautrack_tauvispt_wtaunu_signal_region_fourlayer = (TH2D*)m_file_wlnu->Get("tracklet_ettopoclus20_tautrack_tauvispt_wlnu_signal_region_fourlayer");

}
