#include "MyAnalysis.h"

void MyAnalysis::set_color_palette() {
  const Int_t NRGBs = 5;
  const Int_t NCont = 255;
  Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
  Double_t Red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
  Double_t Green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
  Double_t Blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
  TColor::CreateGradientColorTable(NRGBs, stops, Red, Green, Blue, NCont);
  gStyle->SetNumberContours(NCont);
}

void MyAnalysis::decorate_legend(TLegend* legend){
  legend->SetBorderSize(0);
  legend->SetFillStyle(0);
  legend->SetTextSize(0.035);
  legend->SetTextFont(42);
}

void MyAnalysis::hist_config(TH1* hist, int marker_color, int marker_style, int line_color, int line_style, int fill_color, int fill_style){
  hist->SetMarkerColor(marker_color);
  hist->SetMarkerStyle(marker_style);
  hist->SetLineColor(line_color);
  hist->SetLineStyle(line_style);
  if(fill_color > 0) {
    hist->SetFillColor(fill_color);
  }
  hist->SetFillStyle(fill_style);
}

void MyAnalysis::decorate_hist(std::vector<std::string> text){
  ATLASLabel(0.2, 0.88, "Internal");
  TLatex* tex_text;
  for(unsigned ii_text = 0; ii_text < text.size(); ++ii_text) {
    tex_text = new TLatex(0.2, 0.81-0.07*(float)ii_text, text.at(ii_text).c_str());
    tex_text->SetNDC();
    tex_text->SetTextSize(0.035);
    tex_text->Draw();
  }
}

void MyAnalysis::decorate_mc_hist(std::vector<std::string> text){
  ATLASLabel(0.2, 0.88, "Simulation Internal");
  TLatex* tex_text;
  for(unsigned ii_text = 0; ii_text < text.size(); ++ii_text) {
    tex_text = new TLatex(0.2, 0.81-0.07*(float)ii_text, text.at(ii_text).c_str());
    tex_text->SetNDC();
    tex_text->SetTextSize(0.035);
    tex_text->Draw();
  }
}

void MyAnalysis::change_hist_title(TH1* hist, std::string xtitle, std::string ytitle){
  hist->GetXaxis()->SetTitle(xtitle.c_str());
  hist->GetYaxis()->SetTitle(ytitle.c_str());
}

void MyAnalysis::make_canvas() {
  c_std = new TCanvas("c_std", "c_std", 800., 600.);
  c_sqr = new TCanvas("c_sqr", "c_sqr", 800., 800.);
  p_upper = new TPad("p_upper", "p_upper", 0., 0.27, 1., 1.);
  p_lower = new TPad("p_lower", "p_lower", 0., 0., 1., 0.73);
  p_upper->SetBottomMargin(0.15);
  p_lower->SetTopMargin(0.53);
  p_lower->SetBottomMargin(0.2);
  p_lower->Draw();
  p_upper->Draw();
  p_upper->SetFillStyle(0);
}

void MyAnalysis::range_setter(const std::vector<TH1*>* hists, bool is_logy) {
  Float_t maxy(-FLT_MAX), miny(FLT_MAX);
  Float_t maxy_log(0.), miny_log(0.);
  for(auto hist : *hists) {
    if(hist->GetMaximum() > maxy) maxy = hist->GetMaximum();
    if(hist->GetMinimum() < miny && hist->GetMinimum() > 0.) miny = hist->GetMinimum();
    std::cout<<"maxy = "<<hist->GetMaximum()<<", bin = "<<hist->GetMaximumBin()<<std::endl;
  }
  if(!is_logy) hists->at(0)->GetYaxis()->SetRangeUser(0., maxy*1.5);
  if(is_logy) {
    maxy_log = log10(maxy);
    miny_log = log10(miny);
    // std:cout<<"miny_log = "<<miny_log<<", maxy_log = "<<maxy_log<<std::endl;
    //hists->at(0)->GetYaxis()->SetRangeUser(pow(10., miny_log-std::fabs(maxy_log-miny_log)*0.2), pow(10, maxy_log+std::fabs(maxy_log-miny_log)*0.5));
    hists->at(0)->GetYaxis()->SetRangeUser(1.e-5, 1.e2);
  }
}

void MyAnalysis::make_normal_hist(const std::vector<TH1*>* hists, const std::vector<std::string>* hist_names, bool is_logx, bool is_logy) {
  range_setter(hists, is_logy);

  c_std->SetLogx(is_logx);
  c_std->SetLogx(is_logx);
  c_std->cd();

  TLegend* leg = new TLegend(0.7, 0.92-hists->size()*0.06, 0.95, 0.92);
  decorate_legend(leg);

  hists->at(0)->GetXaxis()->SetLabelSize(0.05);
  hists->at(0)->GetXaxis()->SetTitleSize(0.05);
  
  for(unsigned ii_hist = 0; ii_hist < hists->size(); ++ii_hist) {
    if(ii_hist == 0) hists->at(ii_hist)->Draw("hist");
    else hists->at(ii_hist)->Draw("same hist");
    leg->AddEntry(hists->at(ii_hist), hist_names->at(ii_hist).c_str(), "l");
  }
  c_std->SetLogy(is_logy);
  leg->Draw();
  
  p_lower->cd();

  //delete leg;
  //leg = nullptr;
}

void MyAnalysis::make_normal_hist(const std::vector<TH1*>* hists, const std::vector<std::string>* hist_names, const std::vector<std::string>* hist_types, bool is_logx, bool is_logy) {
  range_setter(hists, is_logy);

  c_std->SetLogx(is_logx);
  c_std->SetLogx(is_logx);
  c_std->cd();

  TLegend* leg = new TLegend(0.5, 0.92-hists->size()*0.06, 0.95, 0.92);
  decorate_legend(leg);

  hists->at(0)->GetXaxis()->SetLabelSize(0.05);
  hists->at(0)->GetXaxis()->SetTitleSize(0.05);

  for(unsigned ii_hist = 0; ii_hist < hists->size(); ++ii_hist) {
    if(hist_types->at(ii_hist) == "data") {
      if(ii_hist == 0) hists->at(ii_hist)->Draw("pl");
      else hists->at(ii_hist)->Draw("same pl");
      leg->AddEntry(hists->at(ii_hist), hist_names->at(ii_hist).c_str(), "pl");
    }
    if(hist_types->at(ii_hist) == "mc") {
      if(ii_hist == 0) hists->at(ii_hist)->Draw("hist");
      else hists->at(ii_hist)->Draw("same hist");
      leg->AddEntry(hists->at(ii_hist), hist_names->at(ii_hist).c_str(), "l");
    }
  }
  c_std->SetLogy(is_logy);
  leg->Draw();
  
  p_lower->cd();

  //delete leg;
  //leg = nullptr;
}

void MyAnalysis::make_ratio_hist(const std::vector<TH1*>* hists, const std::vector<std::string>* hist_names, bool is_logx, bool is_logy) {
  std::vector<TH1*>* ratio_hists = new std::vector<TH1*>;

  range_setter(hists, is_logy);

  p_upper->SetLogx(is_logx);
  p_lower->SetLogx(is_logx);
  p_upper->cd();

  TLegend* leg = new TLegend(0.7, 0.92-hists->size()*0.06, 0.95, 0.92);
  decorate_legend(leg);

  for(unsigned ii_hist = 0; ii_hist < hists->size(); ++ii_hist) {
    ratio_hists->push_back(make_ratio((TH1D*)hists->at(ii_hist), (TH1D*)hists->at(0), "ratio"+(std::string)hists->at(ii_hist)->GetName()));
    hists->at(ii_hist)->GetXaxis()->SetLabelSize(0);
    hists->at(ii_hist)->GetXaxis()->SetTitleSize(0);
    if(ii_hist == 0) hists->at(ii_hist)->Draw();
    else hists->at(ii_hist)->Draw("same");
    leg->AddEntry(hists->at(ii_hist), hist_names->at(ii_hist).c_str(), "pl");
  }
  p_upper->SetLogy(is_logy);
  leg->Draw();
  
  p_lower->cd();
  for(unsigned ii_hist = 0; ii_hist < hists->size(); ++ii_hist) {
    if(ii_hist == 0) {
      ratio_hists->at(ii_hist)->GetYaxis()->SetRangeUser(0., 2.);
      ratio_hists->at(ii_hist)->GetYaxis()->SetTitle(("ratio [/"+hist_names->at(ii_hist)+"]").c_str());
      ratio_hists->at(ii_hist)->SetFillStyle(3233);
      ratio_hists->at(ii_hist)->SetMarkerSize(0);
      ratio_hists->at(ii_hist)->SetLabelSize(0.05);
      ratio_hists->at(ii_hist)->SetTitleSize(0.05);
      ratio_hists->at(ii_hist)->SetNdivisions(504, "Y");
      ratio_hists->at(ii_hist)->Draw("e2");
    } else ratio_hists->at(ii_hist)->Draw("same");
  }

  p_upper->cd();
  
  delete ratio_hists;
  ratio_hists = nullptr;
}


void MyAnalysis::make_ratio_hist(const std::vector<TH1*>* hists, const std::vector<std::string>* hist_names, const std::vector<std::string>* hist_types, bool is_logx, bool is_logy) {
  std::vector<TH1*>* ratio_hists = new std::vector<TH1*>;

  range_setter(hists, is_logy);

  p_upper->SetLogx(is_logx);
  p_lower->SetLogx(is_logx);
  p_upper->cd();

  TLegend* leg = new TLegend(0.65, 0.92-hists->size()*0.06, 0.95, 0.92);
  decorate_legend(leg);

  for(unsigned ii_hist = 0; ii_hist < hists->size(); ++ii_hist) {
    ratio_hists->push_back(make_ratio((TH1D*)hists->at(ii_hist), (TH1D*)hists->at(0), "ratio"+(std::string)hists->at(ii_hist)->GetName()));
    hists->at(ii_hist)->GetXaxis()->SetLabelSize(0);
    hists->at(ii_hist)->GetXaxis()->SetTitleSize(0);
    if(hist_types->at(ii_hist) == "data") {
      if(ii_hist == 0) hists->at(ii_hist)->Draw();
      else hists->at(ii_hist)->Draw("same");
      leg->AddEntry(hists->at(ii_hist), hist_names->at(ii_hist).c_str(), "pl");
    }
    if(hist_types->at(ii_hist) == "mc") {
      if(ii_hist == 0) hists->at(ii_hist)->Draw("hist");
      else hists->at(ii_hist)->Draw("same hist");
      leg->AddEntry(hists->at(ii_hist), hist_names->at(ii_hist).c_str(), "l");
    }
  }
  p_upper->SetLogy(is_logy);
  leg->Draw();
  
  p_lower->cd();
  for(unsigned ii_hist = 0; ii_hist < hists->size(); ++ii_hist) {
    if(ii_hist == 0) {
      ratio_hists->at(ii_hist)->GetYaxis()->SetRangeUser(0., 0.5);
      ratio_hists->at(ii_hist)->GetYaxis()->SetTitle(("ratio [/"+hist_names->at(ii_hist)+"]").c_str());
      ratio_hists->at(ii_hist)->SetFillStyle(3233);
      ratio_hists->at(ii_hist)->SetMarkerSize(0);
      ratio_hists->at(ii_hist)->SetLabelSize(0.05);
      ratio_hists->at(ii_hist)->SetTitleSize(0.05);
      ratio_hists->at(ii_hist)->SetNdivisions(504, "Y");
      ratio_hists->at(ii_hist)->Draw("e2");
    } else {
      if(hist_types->at(ii_hist) == "data") {      
	ratio_hists->at(ii_hist)->Draw("same");
      }
      if(hist_types->at(ii_hist) == "mc") {
	ratio_hists->at(ii_hist)->Draw("same hist");
      }
    }
  }

  p_upper->cd();

  delete ratio_hists;
  //delete leg;
  ratio_hists = nullptr;
  //leg = nullptr;
}


TH1D* MyAnalysis::make_ratio(TH1D* numerator, TH1D* denominator, std::string name){
  TH1D* numerator_copy = (TH1D*)numerator->Clone();
  numerator_copy->SetName(name.c_str());
  if(numerator->GetNbinsX() != denominator->GetNbinsX()) std::cout<<"nbin is not correct."<<std::endl;
  for(unsigned ii_bin = 0; ii_bin < numerator->GetNbinsX(); ++ii_bin) {
    if(denominator->GetBinContent(ii_bin+1) == 0.){
      numerator_copy->SetBinContent(ii_bin+1, 0.);
      continue;
    }
    if(numerator->GetBinContent(ii_bin+1) == 0) continue;
    numerator_copy->SetBinContent(ii_bin+1, numerator->GetBinContent(ii_bin+1)/denominator->GetBinContent(ii_bin+1));
    numerator_copy->SetBinError(ii_bin+1, numerator_copy->GetBinError(ii_bin+1)*numerator_copy->GetBinContent(ii_bin+1)/numerator->GetBinContent(ii_bin+1));
  }
  return numerator_copy;
}

TH2D* MyAnalysis::make_ratio(TH2D* numerator, TH2D* denominator, std::string name){
  TH2D* numerator_copy = (TH2D*)numerator->Clone();
  numerator_copy->SetName(name.c_str());
  if(numerator->GetNbinsX() != denominator->GetNbinsX()) std::cout<<"nbin is not correct."<<std::endl;
  for(unsigned ii_binx = 0; ii_binx < numerator->GetNbinsX(); ++ii_binx) {
    for(unsigned ii_biny = 0; ii_biny < numerator->GetNbinsY(); ++ii_biny) {
      if(denominator->GetBinContent(ii_binx+1, ii_biny+1) == 0.){
	numerator_copy->SetBinContent(ii_binx+1, ii_biny+1, 0);
	continue;
      }
      numerator_copy->SetBinContent(ii_binx+1, ii_biny+1, numerator->GetBinContent(ii_binx+1, ii_biny+1)/denominator->GetBinContent(ii_binx+1, ii_biny+1));
      numerator_copy->SetBinError(ii_binx+1, ii_biny+1, numerator_copy->GetBinError(ii_binx+1, ii_biny+1)*numerator_copy->GetBinContent(ii_binx+1, ii_biny+1)/numerator->GetBinContent(ii_binx+1, ii_biny+1));
    }
  }
  return numerator_copy;
}

void MyAnalysis::normalize_hist(TH1D* hist) {
  //hist->Sumw2();
  hist->Scale(1./hist->Integral());
}

void MyAnalysis::treat_bin(TH1* hist){
  //treat overflow bin
  int nbin = hist->GetNbinsX();
  double val_OFBin = hist->GetBinContent(nbin+1);
  double err_OFBin = hist->GetBinError(nbin+1);
  double err_LastBin = hist->GetBinError(nbin);
  double err_LastBin_fixed = TMath::Sqrt((err_LastBin*err_LastBin)+(err_OFBin*err_OFBin));
  hist->AddBinContent(nbin,val_OFBin);
  hist->SetBinError(nbin,err_LastBin_fixed);
  //treat underflow bin
  double val_UFBin = hist->GetBinContent(0);
  double err_UFBin = hist->GetBinError(0);
  double err_FirstBin =hist->GetBinError(1);
  double err_FirstBin_fixed = TMath::Sqrt((err_FirstBin*err_FirstBin)+(err_UFBin*err_UFBin));
  hist->AddBinContent(1,val_UFBin);
  hist->SetBinError(1,err_FirstBin_fixed);
}
