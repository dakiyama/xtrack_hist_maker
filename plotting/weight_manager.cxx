#include "MyAnalysis.h"

void MyAnalysis::weight_maker() {
  std::cout<<"     >>>>> This is a weight maker. Please note that you have to prepare the vanilla file. It means you cannot make the weight from already weighted histogram. (mc event weight and pileup weight are OK) <<<<<"<<std::endl;
  m_h_weight_ettopoclus20_zll_signal_region_fourlayer = make_ratio(m_h_ettopoclus20_zll_signal_region_fourlayer, m_h_ettopoclus20_rvptcone_zll_signal_region_fourlayer, "h_weight_ettopoclus20_zll_signal_region_fourlayer");
  m_h_weight_ettopoclus20_lowmet_signal_region_fourlayer = make_ratio(m_h_ettopoclus20_lowmet_signal_region_fourlayer, m_h_ettopoclus20_rvptcone_lowmet_signal_region_fourlayer, "h_weight_ettopoclus20_lowmet_signal_region_fourlayer");
  m_h_weight_ettopoclus20_highmet_signal_region_fourlayer = make_ratio(m_h_ettopoclus20_highmet_signal_region_fourlayer, m_h_ettopoclus20_rvptcone_highmet_signal_region_fourlayer, "h_weight_ettopoclus20_highmet_signal_region_fourlayer");
  m_h_weight_pt_eta_zll_signal_region_fourlayer = make_ratio(m_h_pt_eta_ex_caloveto_zll_signal_region_fourlayer, m_h_pt_eta_rvptcone_zll_signal_region_fourlayer, "h_weight_pt_eta_zll_signal_region_fourlayer");
  m_h_weight_pt_eta_lowmet_signal_region_fourlayer = make_ratio(m_h_pt_eta_ex_caloveto_lowmet_signal_region_fourlayer, m_h_pt_eta_rvptcone_lowmet_signal_region_fourlayer, "h_weight_pt_eta_lowmet_signal_region_fourlayer");
  m_h_weight_pt_eta_highmet_signal_region_fourlayer = make_ratio(m_h_pt_eta_ex_caloveto_highmet_signal_region_fourlayer, m_h_pt_eta_rvptcone_highmet_signal_region_fourlayer, "h_weight_pt_eta_highmet_signal_region_fourlayer");

  for(unsigned ii_bin = 0; ii_bin < m_h_weight_ettopoclus20_zll_signal_region_fourlayer->GetNbinsX(); ++ii_bin) {
    std::cout<<m_h_weight_ettopoclus20_zll_signal_region_fourlayer->GetBinContent(ii_bin+1)<<std::endl;
  }

  m_h_weight_ettopoclus20_zll_signal_region_fourlayer->Write();
  m_h_weight_ettopoclus20_lowmet_signal_region_fourlayer->Write();
  m_h_weight_ettopoclus20_highmet_signal_region_fourlayer->Write();
  m_h_weight_pt_eta_zll_signal_region_fourlayer->Write();
  m_h_weight_pt_eta_lowmet_signal_region_fourlayer->Write();
  m_h_weight_pt_eta_highmet_signal_region_fourlayer->Write();
}
