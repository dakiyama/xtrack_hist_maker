#include "MyAnalysis.h"

void MyAnalysis::fit_ettopoclus20() {
  handle_negative_value(m_h_ettopoclus20_zll_signal_region_fourlayer);
  handle_negative_value(m_h_ettopoclus20_rvptcone_zll_signal_region_fourlayer);
  handle_negative_value(m_h_ettopoclus20_zll_d0sideband_fourlayer);
  handle_negative_value(m_h_ettopoclus20_lowmet_signal_region_fourlayer);
  fake_template_parameter_zll = new std::vector<double>;
  get_fake_et_template();
  get_hadron_et_template();
  fit_zll_et();
  fit_lowmet_et();
}

void MyAnalysis::handle_negative_value(TH1D* hist) {
  for(unsigned ii_bin = 0; ii_bin < hist->GetNbinsX(); ++ii_bin) {
    if(hist->GetBinCenter(ii_bin+1) < 0) {
      hist->SetBinContent(ii_bin+2, hist->GetBinContent(ii_bin+1)+hist->GetBinContent(ii_bin+2));
      hist->SetBinContent(ii_bin+1, 0.);
      hist->SetBinError(ii_bin+2, sqrt(pow(hist->GetBinError(ii_bin+1), 2.)+pow(hist->GetBinError(ii_bin+2), 2.)));
      hist->SetBinError(ii_bin+1, 0.);
    }
  }
}

void MyAnalysis::get_fake_et_template() {
  c_std = new TCanvas("c_std", "c_std", 800., 600.);
  c_std->cd()->SetLogy();
  //TF1* f_expo = new TF1("f_expo", "[0]*(exp(x*[1])+[2]*exp(x*[3]))", 0., 50.);
  TF1* f_expo = new TF1("f_expo", "[0]*(exp([1]*exp(x*[2])))", 0., 50.);
  f_expo->SetParameter(0, 1.);
  f_expo->SetParameter(1, 10.);
  f_expo->SetParameter(2, -0.1);
  //f_expo->SetParameter(3, -1.);
  m_h_ettopoclus20_zll_d0sideband_fourlayer->Fit("f_expo");
  m_h_ettopoclus20_zll_d0sideband_fourlayer->Draw();
  c_std->Print("h_fit_ettopoclus20_zll_d0sideband_fourlayer.png");
  fake_template_parameter_zll->push_back(f_expo->GetParameter(0));
  fake_template_parameter_zll->push_back(f_expo->GetParameter(1));
  fake_template_parameter_zll->push_back(f_expo->GetParameter(2));
}

void MyAnalysis::get_hadron_et_template() {
  c_std->SetLogy(0);
  TF1* f_gauss = new TF1("f_gauss", "[0]*exp(-pow(x-[1], 2)/2./pow([2], 2.))", 0., 50.);
  f_gauss->SetParameter(0, 100.);
  f_gauss->SetParameter(1, 10.);
  f_gauss->SetParameter(2, 5.);
  m_h_ettopoclus20_rvptcone_zll_signal_region_fourlayer->Fit("f_gauss");
  m_h_ettopoclus20_rvptcone_zll_signal_region_fourlayer->GetXaxis()->SetLabelSize(0.05);
  m_h_ettopoclus20_rvptcone_zll_signal_region_fourlayer->GetXaxis()->SetTitleSize(0.05);
  m_h_ettopoclus20_rvptcone_zll_signal_region_fourlayer->Draw();
  c_std->Print("h_fit_ettopoclus20_rvptcone_zll_signal_region_fourlayer.png");
}

void MyAnalysis::fit_zll_et() {
  TF1* f_expo = new TF1("f_expo", "[0]*(exp([1]*exp(x*[2])))", 0., 50.);
  TF1* f_gauss = new TF1("f_gauss", "[0]*exp(-pow(x-[1], 2)/2./pow([2], 2.))", 0., 50.);
  TF1* f_et_template = new TF1("f_et_template", "f_expo+f_gauss", 0., 50.);
  f_et_template->SetParameter(0, 50.);
  f_et_template->FixParameter(1, fake_template_parameter_zll->at(1));
  f_et_template->FixParameter(2, fake_template_parameter_zll->at(2));
  f_et_template->SetParameter(3, 0.1);
  f_et_template->SetParameter(4, 13.);
  f_et_template->SetParameter(5, 12.);
  m_h_ettopoclus20_zll_signal_region_fourlayer->Fit("f_et_template", "", "", 0., 50.);
  m_h_ettopoclus20_zll_signal_region_fourlayer->GetYaxis()->SetRangeUser(0., 1.);
  m_h_ettopoclus20_zll_signal_region_fourlayer->GetXaxis()->SetLabelSize(0.05);
  m_h_ettopoclus20_zll_signal_region_fourlayer->GetXaxis()->SetTitleSize(0.05);
  m_h_ettopoclus20_zll_signal_region_fourlayer->Draw();
  f_expo->SetLineColor(2);
  f_gauss->SetLineColor(4);
  f_expo->SetLineStyle(2);
  f_gauss->SetLineStyle(2);
  f_expo->SetParameter(0, f_et_template->GetParameter(0));
  f_expo->SetParameter(1, f_et_template->GetParameter(1));
  f_expo->SetParameter(2, f_et_template->GetParameter(2));
  f_gauss->SetParameter(0, f_et_template->GetParameter(3));
  f_gauss->SetParameter(1, f_et_template->GetParameter(4));
  f_gauss->SetParameter(2, f_et_template->GetParameter(5));
  f_expo->Draw("same");
  f_gauss->Draw("same");
  c_std->Print("h_fit_ettopoclus20_zll_signal_region_fourlayer.png");
  n_fake_zll = 0.;
  n_fake_zll += f_expo->Eval(10./8.);
  n_fake_zll += f_expo->Eval(10.*3./8.);
  n_hadron_zll += f_expo->Eval(10./8.);
  n_hadron_zll += f_expo->Eval(10.*3./8.);
}

void MyAnalysis::fit_lowmet_et() {
  TF1* f_expo = new TF1("f_expo", "[0]*(exp([1]*exp(x*[2])))", 0., 50.);
  TF1* f_gauss = new TF1("f_gauss", "[0]*exp(-pow(x-[1], 2)/2./pow([2], 2.))", 0., 50.);
  TF1* f_et_template = new TF1("f_et_template", "f_expo+f_gauss", 0., 50.);
  f_et_template->SetParameter(0, 50.);
  f_et_template->FixParameter(1, fake_template_parameter_zll->at(1));
  f_et_template->FixParameter(2, fake_template_parameter_zll->at(2));
  f_et_template->SetParameter(3, 70.);
  f_et_template->SetParameter(4, 13.);
  f_et_template->SetParameter(5, 12.);
  normalize_hist(m_h_ettopoclus20_lowmet_signal_region_fourlayer);
  m_h_ettopoclus20_lowmet_signal_region_fourlayer->Fit("f_et_template", "", "", 0., 50.);
  m_h_ettopoclus20_lowmet_signal_region_fourlayer->GetYaxis()->SetRangeUser(0., 1.);
  m_h_ettopoclus20_lowmet_signal_region_fourlayer->GetXaxis()->SetLabelSize(0.05);
  m_h_ettopoclus20_lowmet_signal_region_fourlayer->GetXaxis()->SetTitleSize(0.05);
  m_h_ettopoclus20_lowmet_signal_region_fourlayer->Draw();
  f_expo->SetLineColor(2);
  f_gauss->SetLineColor(4);
  f_expo->SetLineStyle(2);
  f_gauss->SetLineStyle(2);
  f_expo->SetParameter(0, f_et_template->GetParameter(0));
  f_expo->SetParameter(1, f_et_template->GetParameter(1));
  f_expo->SetParameter(2, f_et_template->GetParameter(2));
  f_gauss->SetParameter(0, f_et_template->GetParameter(3));
  f_gauss->SetParameter(1, f_et_template->GetParameter(4));
  f_gauss->SetParameter(2, f_et_template->GetParameter(5));
  f_expo->Draw("same");
  f_gauss->Draw("same");
  c_std->Print("h_fit_ettopoclus20_lowmet_signal_region_fourlayer.png");
  n_fake_lowmet = 0.;
  n_fake_lowmet += f_expo->Eval(10./8.);
  n_fake_lowmet += f_expo->Eval(10.*3./8.);
  n_hadron_lowmet += f_expo->Eval(10./8.);
  n_hadron_lowmet += f_expo->Eval(10.*3./8.);
}

void MyAnalysis::scale_zll_pt() {
  // m_h_pt_zll_signal_region_fourlayer->Draw();
  // for() {

  // }
}
