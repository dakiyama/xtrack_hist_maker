void run(){
  std::string path = "/afs/cern.ch/work/d/dakiyama/private/DTStudy/hadron_cr/xtrack_hist_maker/plotting";
  //std::string path = "/gpfs/fs8001/dakiyama/DTStudy/DTAnalysis/second_wave/xtrack_hist_maker/xtrack_hist_maker/plotting";
  gROOT->ProcessLine((".L "+path+"/file_manager.cxx").c_str());
  gROOT->ProcessLine((".L "+path+"/hist_manager.cxx").c_str());
  gROOT->ProcessLine((".L "+path+"/weight_manager.cxx").c_str());
  gROOT->ProcessLine((".L "+path+"/MyAnalysis.cxx").c_str());
  gROOT->ProcessLine((".L "+path+"/MyAnalysis.h").c_str());
  gROOT->ProcessLine((".L "+path+"/printing_manager.cxx").c_str());
  gROOT->ProcessLine((".L "+path+"/et_fit_manager.cxx").c_str());
  gROOT->ProcessLine((".L "+path+"/print_wtaunu_hist.cxx").c_str());
  gROOT->ProcessLine((".L "+path+"/ptcone_checker.cxx").c_str());
  gROOT->ProcessLine((".L "+path+"/ptcone_abcd.cxx").c_str());
  gROOT->ProcessLine((".L "+path+"/met_bias_check.cxx").c_str());
  gROOT->ProcessLine((".L "+path+"/print_ptcone_compare_hist.cxx").c_str());
  gROOT->ProcessLine((".L "+path+"/printing_et_pt.cxx").c_str());
  gROOT->ProcessLine((".L "+path+"/et_met.cxx").c_str());
  gROOT->ProcessLine((".L "+path+"/fake_manager.cxx").c_str());
  gROOT->ProcessLine("MyAnalysis t");
  gROOT->ProcessLine("t.main_alg()");
}
