#include "MyAnalysis.h"

void MyAnalysis::open_data_file() {
  m_file_data = TFile::Open("/afs/cern.ch/work/d/dakiyama/private/DTStudy/hadron_cr/file/dataAllYear.vc.merged.hist.root");
  //m_file_data = TFile::Open("/gpfs/fs8001/dakiyama/DTStudy/DTAnalysis/second_wave/xtrack_hist_maker/datarun/dataAllYear.merged.hist.root");
}

void MyAnalysis::close_file() {
  m_file_data->Close();
}

void MyAnalysis::make_output_file() {
  TFile* file_output = new TFile("hadron_cr_weight.root", "recreate");
}

void MyAnalysis::open_zll_file() {
  m_file_zll = TFile::Open("/afs/cern.ch/work/d/dakiyama/private/DTStudy/hadron_cr/file/zmumu_official_not_skim_noweight.root");
  //m_file_zll = TFile::Open("/gpfs/fs8001/dakiyama/DTStudy/DTAnalysis/second_wave/xtrack_hist_maker/zmumu_official/zmumu_official_not_skim_noweight.root");
}

void MyAnalysis::open_wlnu_file() {
  m_file_wlnu = TFile::Open("/afs/cern.ch/work/d/dakiyama/private/DTStudy/hadron_cr/file/wtaunu.vc.root");
  //m_file_wlnu = TFile::Open("/gpfs/fs8001/dakiyama/DTStudy/DTAnalysis/second_wave/xtrack_hist_maker/wtaunurun/wtaunu.root");
}

void MyAnalysis::open_signal_file() {
  m_file_signal = TFile::Open("/afs/cern.ch/work/d/dakiyama/private/DTStudy/hadron_cr/file/wino_700.root");
  //m_file_signal = TFile::Open("/gpfs/fs8001/dakiyama/DTStudy/DTAnalysis/second_wave/xtrack_hist_maker/signalrun/wino_700.root");
}
