#ifndef __MYANALYSIS__
#define __MYANALYSIS__

#include <TObject.h>
#include <iostream>

class MyAnalysis : public TObject
{
public:
  // MyAnalysis.cxx
  MyAnalysis();
  virtual void main_alg();

  // file_namager.cxx
  virtual void open_data_file();
  virtual void open_wlnu_file();
  virtual void open_zll_file();
  virtual void open_signal_file();
  virtual void close_file();
  virtual void make_output_file();

  // hist_manager.cxx
  virtual void get_hist();
  virtual void print_hist();
  virtual TH1D* make_ratio(TH1D*, TH1D*, std::string);
  virtual TH2D* make_ratio(TH2D*, TH2D*, std::string);
  virtual void make_ratio_hist(const std::vector<TH1*>*, const std::vector<std::string>*, bool, bool);
  virtual void make_ratio_hist(const std::vector<TH1*>*, const std::vector<std::string>*, const std::vector<std::string>*, bool, bool);
  virtual void make_normal_hist(const std::vector<TH1*>*, const std::vector<std::string>*, bool, bool);
  virtual void make_normal_hist(const std::vector<TH1*>*, const std::vector<std::string>*, const std::vector<std::string>*, bool, bool);

  // printing_manager.cxx
  virtual void set_color_palette();
  virtual void hist_config(TH1*, int, int, int, int, int, int);
  virtual void decorate_legend(TLegend*);
  virtual void decorate_mc_hist(std::vector<std::string>);
  virtual void decorate_hist(std::vector<std::string>);
  virtual void change_hist_title(TH1*, std::string, std::string);
  virtual void make_canvas();
  virtual void range_setter(const std::vector<TH1*>*, bool);
  virtual void normalize_hist(TH1D*);
  virtual void treat_bin(TH1* hist);

  // weight_manager.cxx
  virtual void weight_maker();

  // et_fit_manager.cxx
  virtual void fit_ettopoclus20();
  virtual void handle_negative_value(TH1D*);
  virtual void get_fake_et_template();
  virtual void get_hadron_et_template();
  virtual void fit_zll_et();
  virtual void fit_lowmet_et();
  virtual void scale_zll_pt();

  // ptcone_compare.cxx
  virtual void ptcone_compare_manager();
  virtual void print_ptcone_compare_hist();
  virtual void get_ptcone_compare_hist();

  // print_wtaunu_hist.cxx  
  virtual void wtaunu_manager();
  virtual void print_wtaunu_hist();
  virtual void get_wtaunu_hist();

  // ptcone_checker.cxx
  virtual void ptcone_checker();
  virtual void ptcone_checker_print_hist();
  virtual void ptcone_checker_get_hist();
  virtual TH1D* hist_projectionX(TH2D* hist2d);
  virtual TH1D* hist_projectionY(TH2D* hist2d);

  // ptcone_abcd.cxx
  virtual void ptcone_abcd();
  virtual void abcd_scale();
  virtual void print_ptcone_abcd_hist();
  virtual void get_ptcone_abcd_hist();

  // et_pt
  virtual void et_pt_manager();
  virtual void print_et_pt_hist();
  virtual void get_et_pt_hist();

  // et_met
  virtual void et_met_manager();
  virtual void print_et_met_hist();
  virtual void get_et_met_hist();

  // met_bias_check.cxx
  virtual void met_bias_check();
  virtual void get_met_bias_hist();
  virtual void print_met_bias_hist();

  // fake_manager
  virtual void fake_manager();
    
private:
  TFile* m_file_data;
  TFile* m_file_wlnu;
  TFile* m_file_zll;
  TFile* m_file_signal;
  TPad* p_upper;
  TPad* p_lower;
  TCanvas* c_sqr;
  TCanvas* c_std;
  TH1D* m_h_ettopoclus20_zll_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_rvptcone_zll_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_zll_highmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_rvptcone_zll_highmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_zll_lowmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_rvptcone_zll_lowmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_lowmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_rvptcone_lowmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_highmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_rvptcone_highmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_middlemet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_verylowmet_signal_region_fourlayer;
  TH1D* m_h_eta_ex_caloveto_zll_signal_region_fourlayer;
  TH1D* m_h_eta_rvptcone_zll_signal_region_fourlayer;
  TH1D* m_h_eta_ex_caloveto_lowmet_signal_region_fourlayer;
  TH1D* m_h_eta_rvptcone_lowmet_signal_region_fourlayer;
  TH1D* m_h_eta_ex_caloveto_highmet_signal_region_fourlayer;
  TH1D* m_h_eta_rvptcone_highmet_signal_region_fourlayer;
  TH1D* m_h_pt_ex_caloveto_zll_signal_region_fourlayer;
  TH1D* m_h_pt_rvptcone_zll_signal_region_fourlayer;
  TH1D* m_h_pt_ex_caloveto_lowmet_signal_region_fourlayer;
  TH1D* m_h_pt_rvptcone_lowmet_signal_region_fourlayer;
  TH1D* m_h_pt_ex_caloveto_highmet_signal_region_fourlayer;
  TH1D* m_h_pt_rvptcone_highmet_signal_region_fourlayer;
  TH2D* m_h_pt_eta_ex_caloveto_zll_signal_region_fourlayer;
  TH2D* m_h_pt_eta_rvptcone_zll_signal_region_fourlayer;
  TH2D* m_h_pt_eta_ex_caloveto_lowmet_signal_region_fourlayer;
  TH2D* m_h_pt_eta_rvptcone_lowmet_signal_region_fourlayer;
  TH2D* m_h_pt_eta_ex_caloveto_highmet_signal_region_fourlayer;
  TH2D* m_h_pt_eta_rvptcone_highmet_signal_region_fourlayer;

  TH1D* m_h_pt_rvptcone_0p04_0p1_zll_signal_region_fourlayer;
  TH1D* m_h_eta_rvptcone_0p04_0p1_zll_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_rvptcone_0p04_0p1_zll_signal_region_fourlayer;
  TH1D* m_h_pt_rvptcone_0p1_0p5_zll_signal_region_fourlayer;
  TH1D* m_h_eta_rvptcone_0p1_0p5_zll_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_rvptcone_0p1_0p5_zll_signal_region_fourlayer;
  TH1D* m_h_pt_rvptcone_0p5_1p0_zll_signal_region_fourlayer;
  TH1D* m_h_eta_rvptcone_0p5_1p0_zll_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_rvptcone_0p5_1p0_zll_signal_region_fourlayer;
  TH1D* m_h_pt_rvptcone_1p0_zll_signal_region_fourlayer;
  TH1D* m_h_eta_rvptcone_1p0_zll_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_rvptcone_1p0_zll_signal_region_fourlayer;
  TH1D* m_h_pt_rvptcone_0p04_0p1_lowmet_signal_region_fourlayer;
  TH1D* m_h_eta_rvptcone_0p04_0p1_lowmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_rvptcone_0p04_0p1_lowmet_signal_region_fourlayer;
  TH1D* m_h_pt_rvptcone_0p1_0p5_lowmet_signal_region_fourlayer;
  TH1D* m_h_eta_rvptcone_0p1_0p5_lowmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_rvptcone_0p1_0p5_lowmet_signal_region_fourlayer;
  TH1D* m_h_pt_rvptcone_0p5_1p0_lowmet_signal_region_fourlayer;
  TH1D* m_h_eta_rvptcone_0p5_1p0_lowmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_rvptcone_0p5_1p0_lowmet_signal_region_fourlayer;
  TH1D* m_h_pt_rvptcone_1p0_lowmet_signal_region_fourlayer;
  TH1D* m_h_eta_rvptcone_1p0_lowmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_rvptcone_1p0_lowmet_signal_region_fourlayer;
  TH1D* m_h_pt_rvptcone_0p04_0p1_highmet_signal_region_fourlayer;
  TH1D* m_h_eta_rvptcone_0p04_0p1_highmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_rvptcone_0p04_0p1_highmet_signal_region_fourlayer;
  TH1D* m_h_pt_rvptcone_0p1_0p5_highmet_signal_region_fourlayer;
  TH1D* m_h_eta_rvptcone_0p1_0p5_highmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_rvptcone_0p1_0p5_highmet_signal_region_fourlayer;
  TH1D* m_h_pt_rvptcone_0p5_1p0_highmet_signal_region_fourlayer;
  TH1D* m_h_eta_rvptcone_0p5_1p0_highmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_rvptcone_0p5_1p0_highmet_signal_region_fourlayer;
  TH1D* m_h_pt_rvptcone_1p0_highmet_signal_region_fourlayer;
  TH1D* m_h_eta_rvptcone_1p0_highmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_rvptcone_1p0_highmet_signal_region_fourlayer;

  
  TH1D* m_h_ettopoclus20_zll_signal_region_threelayer;
  TH1D* m_h_ettopoclus20_rvptcone_zll_signal_region_threelayer;
  TH1D* m_h_ettopoclus20_zll_highmet_signal_region_threelayer;
  TH1D* m_h_ettopoclus20_rvptcone_zll_highmet_signal_region_threelayer;
  TH1D* m_h_ettopoclus20_zll_lowmet_signal_region_threelayer;
  TH1D* m_h_ettopoclus20_rvptcone_zll_lowmet_signal_region_threelayer;
  TH1D* m_h_ettopoclus20_lowmet_signal_region_threelayer;
  TH1D* m_h_ettopoclus20_rvptcone_lowmet_signal_region_threelayer;
  TH1D* m_h_ettopoclus20_highmet_signal_region_threelayer;
  TH1D* m_h_ettopoclus20_rvptcone_highmet_signal_region_threelayer;
  
  TH1D* m_h_weight_ettopoclus20_zll_signal_region_fourlayer;
  TH1D* m_h_weight_ettopoclus20_lowmet_signal_region_fourlayer;
  TH1D* m_h_weight_ettopoclus20_highmet_signal_region_fourlayer;
  TH2D* m_h_weight_pt_eta_zll_signal_region_fourlayer;
  TH2D* m_h_weight_pt_eta_lowmet_signal_region_fourlayer;
  TH2D* m_h_weight_pt_eta_highmet_signal_region_fourlayer;
  // TH2D* m_h2_pt_eta_rvptcone_highmet_signal_region_fourlayer;

  TH1D* m_h_ettopoclus20_zll_d0sideband_fourlayer;
  TH1D* m_h_pt_zll_signal_region_fourlayer;
  TH1D* m_h_pt_zll_d0sideband_fourlayer;

  // wtaunu
  TH1D* m_h_met_tautrack_wtaunu_signal_region_fourlayer;
  TH1D* m_h_event_met_wtaunu;
  TH1D* m_h_pt_tautrack_wtaunu_signal_region_fourlayer;
  TH1D* m_h_eta_tautrack_wtaunu_signal_region_fourlayer;
  TH1D* m_h_vcpt_tautrack_wtaunu_signal_region_fourlayer;
  TH1D* m_h_d0_tautrack_wtaunu_signal_region_fourlayer;
  TH1D* m_h_z0Sin_tautrack_wtaunu_signal_region_fourlayer;
  TH1D* m_h_ptcone40overpt_tautrack_wtaunu_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_tautrack_wtaunu_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_tautrack_met0to50_wtaunu_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_tautrack_met50to100_wtaunu_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_tautrack_met100to_wtaunu_signal_region_fourlayer;
  TH1D* m_h_ptcone40overpt_zll_signal_region_fourlayer;
  TH1D* m_h_deltaphi_met_tautrack_wtaunu_signal_region_fourlayer;
  TH1D* m_h_decayr_tautrack_wtaunu_signal_region_fourlayer;
  TH1D* m_h_truth_pt_tautrack_wtaunu_signal_region_fourlayer;
  TH1D* m_h_truth_pt_truthtau_wtaunu_signal_region_fourlayer;
  TH1D* m_h_reco_mt_event;
  TH1D* m_h_mt_tautrack_wtaunu_signal_region_fourlayer;
  TH1D* m_h_mt_excaloveto_tautrack_wtaunu_signal_region_fourlayer;
  TH1D* m_h_eta_zll_signal_region_fourlayer;

  TH2D* m_h_truthpt_ettopoclus20_tautrack_wtaunu_signal_region_fourlayer;
  TH2D* m_h_truthpt_ettopoclus20_pi0_tautrack_wtaunu_signal_region_fourlayer;
  TH2D* m_h_truthpt_ettopoclus20_nopi0_tautrack_wtaunu_signal_region_fourlayer;
  TH2D* m_h_ettopoclus20_met_tautrack_wtaunu_signal_region_fourlayer;
  TH2D* m_h_ettopoclus20_ettopoclus40_tautrack_wtaunu_signal_region_fourlayer;
  TH2D* m_h_truthpt_met_tautrack_wtaunu_signal_region_fourlayer;
  TH2D* m_h_truth_pt_tautrack_truthtau_wtaunu_signal_region_fourlayer;
  TH2D* m_h_truth_pt_tautrack_truthtau_excaloveto_wtaunu_signal_region_fourlayer;
  TH2D* m_h_ettopoclus20_tautrack_tauvispt_wtaunu_signal_region_fourlayer;

  // ptcone
  TH2D* m_h_ptcone40overpt_ptcone40_signal_wtaunu_signal_region_fourlayer;
  TH2D* m_h_ptcone40overpt_ptcone40_tautrack_wtaunu_signal_region_fourlayer;
  TH2D* m_h_ptcone40overpt_ptcone40_hastruth_wtaunu_signal_region_fourlayer;
  TH2D* m_h_ptcone40overpt_ptcone40_signal_wtaunu_signal_region_threelayer;
  TH2D* m_h_ptcone40overpt_ptcone40_tautrack_wtaunu_signal_region_threelayer;
  TH2D* m_h_ptcone40overpt_ptcone40_hastruth_wtaunu_signal_region_threelayer;
  TH2D* m_h_ptcone40overvcpt_ptcone40_signal_wtaunu_signal_region_fourlayer;
  TH2D* m_h_ptcone40overvcpt_ptcone40_tautrack_wtaunu_signal_region_fourlayer;
  TH2D* m_h_ptcone40overvcpt_ptcone40_hastruth_wtaunu_signal_region_fourlayer;
  TH2D* m_h_ptcone40overvcpt_ptcone40_signal_wtaunu_signal_region_threelayer;
  TH2D* m_h_ptcone40overvcpt_ptcone40_tautrack_wtaunu_signal_region_threelayer;
  TH2D* m_h_ptcone40overvcpt_ptcone40_hastruth_wtaunu_signal_region_threelayer;
  TH2D* m_h_pt_ptcone40_signal_wtaunu_signal_region_fourlayer;
  TH2D* m_h_pt_ptcone40_tautrack_wtaunu_signal_region_fourlayer;
  TH2D* m_h_pt_ptcone40_hastruth_wtaunu_signal_region_fourlayer;
  TH2D* m_h_pt_ptcone40_signal_wtaunu_signal_region_threelayer;
  TH2D* m_h_pt_ptcone40_tautrack_wtaunu_signal_region_threelayer;
  TH2D* m_h_pt_ptcone40_hastruth_wtaunu_signal_region_threelayer;
  TH2D* m_h_vcpt_ptcone40_signal_wtaunu_signal_region_fourlayer;
  TH2D* m_h_vcpt_ptcone40_tautrack_wtaunu_signal_region_fourlayer;
  TH2D* m_h_vcpt_ptcone40_hastruth_wtaunu_signal_region_fourlayer;
  TH2D* m_h_vcpt_ptcone40_signal_wtaunu_signal_region_threelayer;
  TH2D* m_h_vcpt_ptcone40_tautrack_wtaunu_signal_region_threelayer;
  TH2D* m_h_vcpt_ptcone40_hastruth_wtaunu_signal_region_threelayer;
  TH2D* m_h_truth_pt_ptcone40_signal_wtaunu_signal_region_fourlayer;
  TH2D* m_h_truth_pt_ptcone40_tautrack_wtaunu_signal_region_fourlayer;
  TH2D* m_h_truth_pt_ptcone40_hastruth_wtaunu_signal_region_fourlayer;
  TH2D* m_h_truth_pt_ptcone40_signal_wtaunu_signal_region_threelayer;
  TH2D* m_h_truth_pt_ptcone40_tautrack_wtaunu_signal_region_threelayer;
  TH2D* m_h_truth_pt_ptcone40_hastruth_wtaunu_signal_region_threelayer;

  // ptcone abcd
  TH1D* m_h_ptcone40overpt_data_verylowmet_signal_region_fourlayer;
  TH1D* m_h_ptcone40overpt_data_lowmet_signal_region_fourlayer;
  TH1D* m_h_ptcone40overpt_data_middlemet_signal_region_fourlayer;
  TH1D* m_h_ptcone40overpt_data_highmet_signal_region_fourlayer;
  TH1D* m_h_ptcone40overpt_excaloveto_data_verylowmet_signal_region_fourlayer;
  TH1D* m_h_ptcone40overpt_excaloveto_data_lowmet_signal_region_fourlayer;
  TH1D* m_h_ptcone40overpt_excaloveto_data_middlemet_signal_region_fourlayer;
  TH1D* m_h_ptcone40overpt_excaloveto_data_highmet_signal_region_fourlayer;
  TH1D* m_h_ptcone40overpt_hastruth_met0to50_wtaunu_signal_region_fourlayer;
  TH1D* m_h_ptcone40overpt_hastruth_met50to100_wtaunu_signal_region_fourlayer;
  TH1D* m_h_ptcone40overpt_hastruth_met100to_wtaunu_signal_region_fourlayer;
  TH1D* m_h_ptcone40overpt_excaloveto_hastruth_met0to50_wtaunu_signal_region_fourlayer;
  TH1D* m_h_ptcone40overpt_excaloveto_hastruth_met50to100_wtaunu_signal_region_fourlayer;
  TH1D* m_h_ptcone40overpt_excaloveto_hastruth_met100to_wtaunu_signal_region_fourlayer;
  TH1D* m_h_ptcone40overpt_tautrack_met0to50_wtaunu_signal_region_fourlayer;
  TH1D* m_h_ptcone40overpt_tautrack_met50to100_wtaunu_signal_region_fourlayer;
  TH1D* m_h_ptcone40overpt_tautrack_met100to_wtaunu_signal_region_fourlayer;
  TH1D* m_h_ptcone40overpt_excaloveto_tautrack_met0to50_wtaunu_signal_region_fourlayer;
  TH1D* m_h_ptcone40overpt_excaloveto_tautrack_met50to100_wtaunu_signal_region_fourlayer;
  TH1D* m_h_ptcone40overpt_excaloveto_tautrack_met100to_wtaunu_signal_region_fourlayer;

  TH1D* m_h_pt_ptcone0p1to0p5_data_verylowmet_signal_region_fourlayer;
  TH1D* m_h_pt_ptcone0p1to0p5_data_lowmet_signal_region_fourlayer;
  TH1D* m_h_pt_ptcone0p1to0p5_data_middlemet_signal_region_fourlayer;
  TH1D* m_h_pt_ptcone0p1to0p5_data_highmet_signal_region_fourlayer;
  TH1D* m_h_pt_excaloveto_ptcone0p1to0p5_data_verylowmet_signal_region_fourlayer;
  TH1D* m_h_pt_excaloveto_ptcone0p1to0p5_data_lowmet_signal_region_fourlayer;
  TH1D* m_h_pt_excaloveto_ptcone0p1to0p5_data_middlemet_signal_region_fourlayer;
  TH1D* m_h_pt_excaloveto_ptcone0p1to0p5_data_highmet_signal_region_fourlayer;
  TH1D* m_h_pt_ptcone0p1to0p5_met0to50_hastruth_wtaunu_signal_region_fourlayer;
  TH1D* m_h_pt_ptcone0p1to0p5_met50to100_hastruth_wtaunu_signal_region_fourlayer;
  TH1D* m_h_pt_ptcone0p1to0p5_met100to_hastruth_wtaunu_signal_region_fourlayer;
  TH1D* m_h_pt_ptcone0p1to0p5_met0to50_tautrack_wtaunu_signal_region_fourlayer;
  TH1D* m_h_pt_ptcone0p1to0p5_met50to100_tautrack_wtaunu_signal_region_fourlayer;
  TH1D* m_h_pt_ptcone0p1to0p5_met100to_tautrack_wtaunu_signal_region_fourlayer;
  TH1D* m_h_pt_excaloveto_ptcone0p1to0p5_met0to50_hastruth_wtaunu_signal_region_fourlayer;
  TH1D* m_h_pt_excaloveto_ptcone0p1to0p5_met50to100_hastruth_wtaunu_signal_region_fourlayer;
  TH1D* m_h_pt_excaloveto_ptcone0p1to0p5_met100to_hastruth_wtaunu_signal_region_fourlayer;
  TH1D* m_h_pt_excaloveto_ptcone0p1to0p5_met0to50_tautrack_wtaunu_signal_region_fourlayer;
  TH1D* m_h_pt_excaloveto_ptcone0p1to0p5_met50to100_tautrack_wtaunu_signal_region_fourlayer;
  TH1D* m_h_pt_excaloveto_ptcone0p1to0p5_met100to_tautrack_wtaunu_signal_region_fourlayer;

  // et_pt
  TH1D* m_h_ettopoclus20_pt_20_40_highmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_pt_40_60_highmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_pt_60_100_highmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_pt_100_highmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_pt_20_40_lowmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_pt_40_60_lowmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_pt_60_100_lowmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_pt_100_lowmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_pt_20_40_zll_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_pt_40_60_zll_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_pt_60_100_zll_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_pt_100_zll_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_pt_20_40_tautrack_wlnu_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_pt_40_60_tautrack_wlnu_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_pt_60_100_tautrack_wlnu_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_pt_100_tautrack_wlnu_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_pt_20_40_qcd_wlnu_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_pt_40_60_qcd_wlnu_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_pt_60_100_qcd_wlnu_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_pt_100_qcd_wlnu_signal_region_fourlayer;

  TH1D* m_h_ettopoclus20_vcpt_20_30_highmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_vcpt_30_40_highmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_vcpt_40_60_highmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_vcpt_60_highmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_vcpt_20_30_lowmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_vcpt_30_40_lowmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_vcpt_40_60_lowmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_vcpt_60_lowmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_vcpt_20_30_zll_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_vcpt_30_40_zll_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_vcpt_40_60_zll_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_vcpt_60_zll_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_vcpt_20_30_tautrack_wlnu_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_vcpt_30_40_tautrack_wlnu_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_vcpt_40_60_tautrack_wlnu_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_vcpt_60_tautrack_wlnu_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_vcpt_20_30_qcd_wlnu_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_vcpt_30_40_qcd_wlnu_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_vcpt_40_60_qcd_wlnu_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_vcpt_60_qcd_wlnu_signal_region_fourlayer;

  TH1D* m_h_pt_rough_lowmet_signal_region_fourlayer;
  TH1D* m_h_pt_rough_highmet_signal_region_fourlayer;
  TH1D* m_h_pt_rough_zll_signal_region_fourlayer;
  TH1D* m_h_pt_rough_tautrack_wlnu_signal_region_fourlayer;
  TH1D* m_h_pt_rough_qcd_wlnu_signal_region_fourlayer;
  TH1D* m_h_pt_rough_et10_lowmet_signal_region_fourlayer;
  TH1D* m_h_pt_rough_et10_highmet_signal_region_fourlayer;
  TH1D* m_h_pt_rough_et10_zll_signal_region_fourlayer;
  TH1D* m_h_pt_rough_et10_tautrack_wlnu_signal_region_fourlayer;
  TH1D* m_h_pt_rough_et10_qcd_wlnu_signal_region_fourlayer;

  TH1D* m_h_vcpt_rough_lowmet_signal_region_fourlayer;
  TH1D* m_h_vcpt_rough_highmet_signal_region_fourlayer;
  TH1D* m_h_vcpt_rough_zll_signal_region_fourlayer;
  TH1D* m_h_vcpt_rough_tautrack_wlnu_signal_region_fourlayer;
  TH1D* m_h_vcpt_rough_qcd_wlnu_signal_region_fourlayer;
  TH1D* m_h_vcpt_rough_et10_lowmet_signal_region_fourlayer;
  TH1D* m_h_vcpt_rough_et10_highmet_signal_region_fourlayer;
  TH1D* m_h_vcpt_rough_et10_zll_signal_region_fourlayer;
  TH1D* m_h_vcpt_rough_et10_tautrack_wlnu_signal_region_fourlayer;
  TH1D* m_h_vcpt_rough_et10_qcd_wlnu_signal_region_fourlayer;

  TH1D* m_h_pt_data_verylowmet_signal_region_fourlayer;
  TH1D* m_h_pt_data_lowmet_signal_region_fourlayer;
  TH1D* m_h_pt_data_middlemet_signal_region_fourlayer;
  TH1D* m_h_pt_data_highmet_signal_region_fourlayer;
  TH1D* m_h_pt_excaloveto_data_verylowmet_signal_region_fourlayer;
  TH1D* m_h_pt_excaloveto_data_lowmet_signal_region_fourlayer;
  TH1D* m_h_pt_excaloveto_data_middlemet_signal_region_fourlayer;
  TH1D* m_h_pt_excaloveto_data_highmet_signal_region_fourlayer;

  TH2D* m_h_ettopoclus20_met_data_allmet_signal_region_fourlayer;
  TH2D* m_h_ettopoclus20_subtractmet_data_allmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_data_verylowmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_data_lowmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_data_middlemet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_data_highmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_data_verylowsubtractmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_data_lowsubtractmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_data_middlesubtractmet_signal_region_fourlayer;
  TH1D* m_h_ettopoclus20_data_highsubtractmet_signal_region_fourlayer;

  // fake
  TH1D* m_h_vcpt_fake_rough_highmet_signal_region_fourlayer;
  TH1D* m_h_vcpt_fake_rough_lowmet_signal_region_fourlayer;
  TH1D* m_h_vcpt_fake_rough_highmet_signal_region_threelayer;
  TH1D* m_h_vcpt_fake_rough_lowmet_signal_region_threelayer;


  std::vector<double>* fake_template_parameter_zll;
  double n_fake_zll;
  double n_hadron_zll;
  double n_fake_lowmet;
  double n_hadron_lowmet;
};
#endif
