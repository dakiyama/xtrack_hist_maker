#include "MyAnalysis.h"

void MyAnalysis::fake_manager() {
  std::vector<double> PtBin = {20., 40., 60., 100., 140.};
  m_h_vcpt_fake_rough_highmet_signal_region_fourlayer = new TH1D("h_vcpt_fake_rough_highmet_signal_region_fourlayer", ";Tracklet #it{p}_{T};Events", PtBin.size() - 1, (double*)&(PtBin.at(0)));
  m_h_vcpt_fake_rough_lowmet_signal_region_fourlayer = new TH1D("h_vcpt_fake_rough_lowmet_signal_region_fourlayer", ";Tracklet #it{p}_{T};Events", PtBin.size() - 1, (double*)&(PtBin.at(0)));
  m_h_vcpt_fake_rough_highmet_signal_region_threelayer = new TH1D("h_vcpt_fake_rough_highmet_signal_region_threelayer", ";Tracklet #it{p}_{T};Events", PtBin.size() - 1, (double*)&(PtBin.at(0)));
  m_h_vcpt_fake_rough_lowmet_signal_region_threelayer = new TH1D("h_vcpt_fake_rough_lowmet_signal_region_threelayer", ";Tracklet #it{p}_{T};Events", PtBin.size() - 1, (double*)&(PtBin.at(0)));

  m_h_vcpt_fake_rough_highmet_signal_region_fourlayer->SetBinContent(1, 0.);
  m_h_vcpt_fake_rough_highmet_signal_region_fourlayer->SetBinContent(2, 0.7);
  m_h_vcpt_fake_rough_highmet_signal_region_fourlayer->SetBinContent(3, 0.7);
  m_h_vcpt_fake_rough_highmet_signal_region_fourlayer->SetBinContent(4, 1.3);
  m_h_vcpt_fake_rough_highmet_signal_region_fourlayer->SetBinError(1, 0.);
  m_h_vcpt_fake_rough_highmet_signal_region_fourlayer->SetBinError(2, 0.8);
  m_h_vcpt_fake_rough_highmet_signal_region_fourlayer->SetBinError(3, 0.8);
  m_h_vcpt_fake_rough_highmet_signal_region_fourlayer->SetBinError(4, 1.2);

  m_h_vcpt_fake_rough_lowmet_signal_region_fourlayer->SetBinContent(1, 9.3);
  m_h_vcpt_fake_rough_lowmet_signal_region_fourlayer->SetBinContent(2, 1.3);
  m_h_vcpt_fake_rough_lowmet_signal_region_fourlayer->SetBinContent(3, 2.);
  m_h_vcpt_fake_rough_lowmet_signal_region_fourlayer->SetBinContent(4, 4.7);
  m_h_vcpt_fake_rough_lowmet_signal_region_fourlayer->SetBinError(1, 3.1);
  m_h_vcpt_fake_rough_lowmet_signal_region_fourlayer->SetBinError(2, 1.2);
  m_h_vcpt_fake_rough_lowmet_signal_region_fourlayer->SetBinError(3, 1.4);
  m_h_vcpt_fake_rough_lowmet_signal_region_fourlayer->SetBinError(4, 2.2);

  m_h_vcpt_fake_rough_highmet_signal_region_threelayer->SetBinContent(1, 285.3);
  m_h_vcpt_fake_rough_highmet_signal_region_threelayer->SetBinContent(2, 128.7);
  m_h_vcpt_fake_rough_highmet_signal_region_threelayer->SetBinContent(3, 121.3);
  m_h_vcpt_fake_rough_highmet_signal_region_threelayer->SetBinContent(4, 224.7);
  m_h_vcpt_fake_rough_highmet_signal_region_threelayer->SetBinError(1, 16.9);
  m_h_vcpt_fake_rough_highmet_signal_region_threelayer->SetBinError(2, 11.3);
  m_h_vcpt_fake_rough_highmet_signal_region_threelayer->SetBinError(3, 11.);
  m_h_vcpt_fake_rough_highmet_signal_region_threelayer->SetBinError(4, 15.);

  m_h_vcpt_fake_rough_lowmet_signal_region_threelayer->SetBinContent(1, 1552.);
  m_h_vcpt_fake_rough_lowmet_signal_region_threelayer->SetBinContent(2, 688.);
  m_h_vcpt_fake_rough_lowmet_signal_region_threelayer->SetBinContent(3, 628.);
  m_h_vcpt_fake_rough_lowmet_signal_region_threelayer->SetBinContent(4, 1146.);
  m_h_vcpt_fake_rough_lowmet_signal_region_threelayer->SetBinError(1, 39.4);
  m_h_vcpt_fake_rough_lowmet_signal_region_threelayer->SetBinError(2, 26.2);
  m_h_vcpt_fake_rough_lowmet_signal_region_threelayer->SetBinError(3, 25.1);
  m_h_vcpt_fake_rough_lowmet_signal_region_threelayer->SetBinError(4, 33.9);
}
