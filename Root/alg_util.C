#include "MyAnalysis.h"

#ifdef MyAnalysis_cxx
MyAnalysis::MyAnalysis(TTree *tree) :
  fChain(0),
  zmass(91.1876),
  emass(0.51100e-3)
{
  // if parameter tree is not specified (or zero), connect the file
  // used to generate this class and read the Tree.
  if (tree == 0) {

#ifdef SINGLE_TREE
    // The following code should be used if you want this class to access
    // a single tree instead of a chain
    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Memory Directory");
    if (!f || !f->IsOpen()) {
      f = new TFile("Memory Directory");
    }
    f->GetObject("MyTree",tree);

#else // SINGLE_TREE

      // The following code should be used if you want this class to access a chain
      // of trees.
    TChain * chain = new TChain("MyTree","");
    chain->Add("/gpfs/fs7001/dakiyama/DTStudy/DTAnalysis/second_wave/private_nt_maker/xtrack_ntuple_maker/run/xtrack_Histograms.root/MyTree");
    tree = chain;
#endif // SINGLE_TREE

  }
  Init(tree);
}

MyAnalysis::~MyAnalysis()
{
  if (!fChain) return;
  delete fChain->GetCurrentFile();
}

Int_t MyAnalysis::GetEntry(Long64_t entry)
{
  // Read contents of entry.
  if (!fChain) return 0;
  return fChain->GetEntry(entry);
}
Long64_t MyAnalysis::LoadTree(Long64_t entry)
{
  // Set the environment to read one entry
  if (!fChain) return -5;
  Long64_t centry = fChain->LoadTree(entry);
  if (centry < 0) return centry;
  if (fChain->GetTreeNumber() != fCurrent) {
    fCurrent = fChain->GetTreeNumber();
    Notify();
  }
  return centry;
}

void MyAnalysis::Init(TTree *tree)
{
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the branch addresses and branch
  // pointers of the tree will be set.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).

  m_integrated_luminosity = 0.;

  // Set object pointer
  muon_phi = 0;
  muon_eta = 0;
  muon_pt = 0;
  muon_e = 0;
  muon_charge = 0;
  muon_ptVarCone20OverPt = 0;
  muon_ptVarCone30OverPt = 0;
  muon_ptVarCone40OverPt = 0;
  muon_eTTopoCone20 = 0;
  muon_eTTopoCone30 = 0;
  muon_eTTopoCone40 = 0;
  muon_quality = 0;
  muon_isTrigMatch = 0;
  muon_isSignal = 0;
  muon_key = 0;
  electron_phi = 0;
  electron_eta = 0;
  electron_pt = 0;
  electron_e = 0;
  electron_charge = 0;
  electron_ptVarCone20OverPt = 0;
  electron_eTTopoCone20 = 0;
  electron_quality = 0;
  electron_isTrigMatch = 0;
  electron_isSignal = 0;
  electron_key = 0;
  tau_phi = 0;
  tau_eta = 0;
  tau_pt = 0;
  tau_e = 0;
  tau_charge = 0;
  tau_nprongs = 0;
  tau_BDTJetScore = 0;
  tau_isMedium = 0;
  tau_track_phi = 0;
  tau_track_eta = 0;
  tau_track_pt = 0;
  tau_track_e = 0;
  jet_phi = 0;
  jet_eta = 0;
  jet_pt = 0;
  jet_e = 0;
  jet_isBJet = 0;
  msTrack_phi = 0;
  msTrack_eta = 0;
  msTrack_pt = 0;
  msTrack_e = 0;
  msTrack_charge = 0;
  caloCluster_phi = 0;
  caloCluster_eta = 0;
  caloCluster_pt = 0;
  caloCluster_e = 0;
  caloCluster_clusterSize = 0;
  truthjet_phi = 0;
  truthjet_eta = 0;
  truthjet_pt = 0;
  truthjet_e = 0;
  truth_phi = 0;
  truth_eta = 0;
  truth_pt = 0;
  truth_e = 0;
  truth_vis_phi = 0;
  truth_vis_eta = 0;
  truth_vis_pt = 0;
  truth_vis_e = 0;
  truth_charge = 0;
  truth_pdgid = 0;
  truth_barcode = 0;
  truth_nChildren = 0;
  truth_nTauProngs = 0;
  truth_nPi0 = 0;
  truth_isTauHad = 0;
  truth_parent_barcode = 0;
  truth_properTime = 0;
  truth_decayr = 0;
  track_isTracklet = 0;
  track_phi = 0;
  track_eta = 0;
  track_pt = 0;
  track_e = 0;
  track_charge = 0;
  track_KVUPhi = 0;
  track_KVUEta = 0;
  track_KVUPt = 0;
  track_KVUCharge = 0;
  track_KVUChi2Prob = 0;
  track_pixelBarrel_0 = 0;
  track_pixelBarrel_1 = 0;
  track_pixelBarrel_2 = 0;
  track_pixelBarrel_3 = 0;
  track_sctBarrel_0 = 0;
  track_sctBarrel_1 = 0;
  track_sctBarrel_2 = 0;
  track_sctBarrel_3 = 0;
  track_sctEndcap_0 = 0;
  track_sctEndcap_1 = 0;
  track_sctEndcap_2 = 0;
  track_sctEndcap_3 = 0;
  track_sctEndcap_4 = 0;
  track_sctEndcap_5 = 0;
  track_sctEndcap_6 = 0;
  track_sctEndcap_7 = 0;
  track_sctEndcap_8 = 0;
  track_nPixelEndcap = 0;
  track_nPixHits = 0;
  track_nPixHoles = 0;
  track_nPixSharedHits = 0;
  track_nPixOutliers = 0;
  track_nPixDeadSensors = 0;
  track_nContribPixelLayers = 0;
  track_nGangedFlaggedFakes = 0;
  track_nPixelSpoiltHits = 0;
  track_nSCTHits = 0;
  track_nSCTHoles = 0;
  track_nSCTSharedHits = 0;
  track_nSCTOutliers = 0;
  track_nSCTDeadSensors = 0;
  track_nTRTHits = 0;
  track_nTRTHoles = 0;
  track_nTRTSharedHits = 0;
  track_nTRTDeadSensors = 0;
  track_nTRTOutliers = 0;
  track_dEdx = 0;
  track_ptCone20OverPt = 0;
  track_ptCone30OverPt = 0;
  track_ptCone40OverPt = 0;
  track_ptCone20OverKVUPt = 0;
  track_ptCone30OverKVUPt = 0;
  track_ptCone40OverKVUPt = 0;
  track_eTTopoClus20 = 0;
  track_eTTopoClus30 = 0;
  track_eTTopoClus40 = 0;
  track_eTTopoCone20 = 0;
  track_eTTopoCone30 = 0;
  track_eTTopoCone40 = 0;
  track_dRJet20 = 0;
  track_dRJet50 = 0;
  track_dRMSTrack = 0;
  track_dRMuon = 0;
  track_dRElectron = 0;
  track_d0 = 0;
  track_d0Sig = 0;
  track_z0 = 0;
  track_z0PV = 0;
  track_z0PVSinTheta = 0;
  track_chi2Prob = 0;
  track_parameterCovMatrix = 0;
  track_electron_key = 0;
  track_muon_key = 0;
  track_truth_phi = 0;
  track_truth_eta = 0;
  track_truth_pt = 0;
  track_truth_e = 0;
  track_truth_charge = 0;
  track_truth_pdgid = 0;
  track_truth_barcode = 0;
  track_truth_nChildren = 0;
  track_truth_properTime = 0;
  track_truth_decayr = 0;
  track_truth_matchingProb = 0;
  track_truth_parent_phi = 0;
  track_truth_parent_eta = 0;
  track_truth_parent_pt = 0;
  track_truth_parent_e = 0;
  track_truth_parent_charge = 0;
  track_truth_parent_pdgid = 0;
  track_truth_parent_barcode = 0;
  track_truth_parent_nChildren = 0;
  track_truth_parent_decayr = 0;
  // Set branch addresses and branch pointers
  if (!tree) return;
  fChain = tree;
  fCurrent = -1;
  fChain->SetMakeClass(1);

  fChain->SetBranchAddress("is_data", &is_data, &b_is_data);
  fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
  fChain->SetBranchAddress("randomRunNumber", &randomRunNumber, &b_randomRunNumber);
  fChain->SetBranchAddress("lumiBlock", &lumiBlock, &b_lumiBlock);
  fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
  fChain->SetBranchAddress("mc_eventweight", &mc_eventweight, &b_mc_eventweight);
  fChain->SetBranchAddress("prw_weight", &prw_weight, &b_prw_weight);
  fChain->SetBranchAddress("average_interaction_per_crossing", &average_interaction_per_crossing, &b_average_interaction_per_crossing);
  fChain->SetBranchAddress("actual_interaction_per_crossing", &actual_interaction_per_crossing, &b_actual_interaction_per_crossing);
  fChain->SetBranchAddress("corrected_average_interaction_per_crossing", &corrected_average_interaction_per_crossing, &b_corrected_average_interaction_per_crossing);
  fChain->SetBranchAddress("pass_electron", &pass_electron, &b_pass_electron);
  fChain->SetBranchAddress("pass_muon", &pass_muon, &b_pass_muon);
  fChain->SetBranchAddress("pass_missingET", &pass_missingET, &b_pass_missingET);
  fChain->SetBranchAddress("has_pv", &has_pv, &b_has_pv);
  fChain->SetBranchAddress("pass_event_cleaning", &pass_event_cleaning, &b_pass_event_cleaning);
  fChain->SetBranchAddress("is_bad_muon_missingET", &is_bad_muon_missingET, &b_is_bad_muon_missingET);
  fChain->SetBranchAddress("has_bad_jet", &has_bad_jet, &b_has_bad_jet);
  fChain->SetBranchAddress("nMuons", &nMuons, &b_nMuons);
  fChain->SetBranchAddress("nElectrons", &nElectrons, &b_nElectrons);
  fChain->SetBranchAddress("nTaus", &nTaus, &b_nTaus);
  fChain->SetBranchAddress("nJets", &nJets, &b_nJets);
  fChain->SetBranchAddress("nTruthJets", &nTruthJets, &b_nTruthJets);
  fChain->SetBranchAddress("nMSTracks", &nMSTracks, &b_nMSTracks);
  fChain->SetBranchAddress("nCaloClusters", &nCaloClusters, &b_nCaloClusters);
  fChain->SetBranchAddress("nTracks", &nTracks, &b_nTracks);
  fChain->SetBranchAddress("nTruths", &nTruths, &b_nTruths);
  fChain->SetBranchAddress("muon_phi", &muon_phi, &b_muon_phi);
  fChain->SetBranchAddress("muon_eta", &muon_eta, &b_muon_eta);
  fChain->SetBranchAddress("muon_pt", &muon_pt, &b_muon_pt);
  fChain->SetBranchAddress("muon_e", &muon_e, &b_muon_e);
  fChain->SetBranchAddress("muon_charge", &muon_charge, &b_muon_charge);
  fChain->SetBranchAddress("muon_ptVarCone20OverPt", &muon_ptVarCone20OverPt, &b_muon_ptVarCone20OverPt);
  fChain->SetBranchAddress("muon_ptVarCone30OverPt", &muon_ptVarCone30OverPt, &b_muon_ptVarCone30OverPt);
  fChain->SetBranchAddress("muon_ptVarCone40OverPt", &muon_ptVarCone40OverPt, &b_muon_ptVarCone40OverPt);
  fChain->SetBranchAddress("muon_eTTopoCone20", &muon_eTTopoCone20, &b_muon_eTTopoCone20);
  fChain->SetBranchAddress("muon_eTTopoCone30", &muon_eTTopoCone30, &b_muon_eTTopoCone30);
  fChain->SetBranchAddress("muon_eTTopoCone40", &muon_eTTopoCone40, &b_muon_eTTopoCone40);
  fChain->SetBranchAddress("muon_quality", &muon_quality, &b_muon_quality);
  fChain->SetBranchAddress("muon_isSignal", &muon_isSignal, &b_muon_isSignal);
  fChain->SetBranchAddress("muon_isTrigMatch", &muon_isTrigMatch, &b_muon_isTrigMatch);
  fChain->SetBranchAddress("muon_key", &muon_key, &b_muon_key);
  fChain->SetBranchAddress("electron_phi", &electron_phi, &b_electron_phi);
  fChain->SetBranchAddress("electron_eta", &electron_eta, &b_electron_eta);
  fChain->SetBranchAddress("electron_pt", &electron_pt, &b_electron_pt);
  fChain->SetBranchAddress("electron_e", &electron_e, &b_electron_e);
  fChain->SetBranchAddress("electron_charge", &electron_charge, &b_electron_charge);
  fChain->SetBranchAddress("electron_ptVarCone20OverPt", &electron_ptVarCone20OverPt, &b_electron_ptVarCone20OverPt);
  fChain->SetBranchAddress("electron_eTTopoCone20", &electron_eTTopoCone20, &b_electron_eTTopoCone20);
  fChain->SetBranchAddress("electron_quality", &electron_quality, &b_electron_quality);
  fChain->SetBranchAddress("electron_isSignal", &electron_isSignal, &b_electron_isSignal);
  fChain->SetBranchAddress("electron_isTrigMatch", &electron_isTrigMatch, &b_electron_isTrigMatch);
  fChain->SetBranchAddress("electron_key", &electron_key, &b_electron_key);
  fChain->SetBranchAddress("tau_phi", &tau_phi, &b_tau_phi);
  fChain->SetBranchAddress("tau_eta", &tau_eta, &b_tau_eta);
  fChain->SetBranchAddress("tau_pt", &tau_pt, &b_tau_pt);
  fChain->SetBranchAddress("tau_e", &tau_e, &b_tau_e);
  fChain->SetBranchAddress("tau_charge", &tau_charge, &b_tau_charge);
  fChain->SetBranchAddress("tau_nprongs", &tau_nprongs, &b_tau_nprongs);
  fChain->SetBranchAddress("tau_BDTJetScore", &tau_BDTJetScore, &b_tau_BDTJetScore);
  fChain->SetBranchAddress("tau_isMedium", &tau_isMedium, &b_tau_isMedium);
  // fChain->SetBranchAddress("tau_track_phi", &tau_track_phi, &b_tau_track_phi);
  // fChain->SetBranchAddress("tau_track_eta", &tau_track_eta, &b_tau_track_eta);
  // fChain->SetBranchAddress("tau_track_pt", &tau_track_pt, &b_tau_track_pt);
  // fChain->SetBranchAddress("tau_track_e", &tau_track_e, &b_tau_track_e);
  fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
  fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
  fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
  fChain->SetBranchAddress("jet_e", &jet_e, &b_jet_e);
  fChain->SetBranchAddress("jet_isBJet", &jet_isBJet, &b_jet_isBJet);
  fChain->SetBranchAddress("msTrack_phi", &msTrack_phi, &b_msTrack_phi);
  fChain->SetBranchAddress("msTrack_eta", &msTrack_eta, &b_msTrack_eta);
  fChain->SetBranchAddress("msTrack_pt", &msTrack_pt, &b_msTrack_pt);
  fChain->SetBranchAddress("msTrack_e", &msTrack_e, &b_msTrack_e);
  fChain->SetBranchAddress("msTrack_charge", &msTrack_charge, &b_msTrack_charge);
  fChain->SetBranchAddress("caloCluster_phi", &caloCluster_phi, &b_caloCluster_phi);
  fChain->SetBranchAddress("caloCluster_eta", &caloCluster_eta, &b_caloCluster_eta);
  fChain->SetBranchAddress("caloCluster_pt", &caloCluster_pt, &b_caloCluster_pt);
  fChain->SetBranchAddress("caloCluster_e", &caloCluster_e, &b_caloCluster_e);
  fChain->SetBranchAddress("caloCluster_clusterSize", &caloCluster_clusterSize, &b_caloCluster_clusterSize);
  fChain->SetBranchAddress("missingET_TST_pt", &missingET_TST_pt, &b_missingET_TST_pt);
  fChain->SetBranchAddress("missingET_TST_phi", &missingET_TST_phi, &b_missingET_TST_phi);
  fChain->SetBranchAddress("TST_pt", &TST_pt, &b_TST_pt);
  fChain->SetBranchAddress("TST_phi", &TST_phi, &b_TST_phi);
  fChain->SetBranchAddress("missingET_CST_pt", &missingET_CST_pt, &b_missingET_CST_pt);
  fChain->SetBranchAddress("missingET_CST_phi", &missingET_CST_phi, &b_missingET_CST_phi);
  fChain->SetBranchAddress("CST_pt", &CST_pt, &b_CST_pt);
  fChain->SetBranchAddress("CST_phi", &CST_phi, &b_CST_phi);
  fChain->SetBranchAddress("missingET_TST_leleVeto_pt", &missingET_TST_leleVeto_pt, &b_missingET_TST_leleVeto_pt);
  fChain->SetBranchAddress("missingET_TST_leleVeto_phi", &missingET_TST_leleVeto_phi, &b_missingET_TST_leleVeto_phi);
  fChain->SetBranchAddress("missingET_TST_lmuVeto_pt", &missingET_TST_lmuVeto_pt, &b_missingET_TST_lmuVeto_pt);
  fChain->SetBranchAddress("missingET_TST_lmuVeto_phi", &missingET_TST_lmuVeto_phi, &b_missingET_TST_lmuVeto_phi);
  fChain->SetBranchAddress("truthjet_phi", &truthjet_phi, &b_truthjet_phi);
  fChain->SetBranchAddress("truthjet_eta", &truthjet_eta, &b_truthjet_eta);
  fChain->SetBranchAddress("truthjet_pt", &truthjet_pt, &b_truthjet_pt);
  fChain->SetBranchAddress("truthjet_e", &truthjet_e, &b_truthjet_e);
  fChain->SetBranchAddress("truth_phi", &truth_phi, &b_truth_phi);
  fChain->SetBranchAddress("truth_eta", &truth_eta, &b_truth_eta);
  fChain->SetBranchAddress("truth_pt", &truth_pt, &b_truth_pt);
  fChain->SetBranchAddress("truth_e", &truth_e, &b_truth_e);
  fChain->SetBranchAddress("truth_vis_phi", &truth_vis_phi, &b_truth_vis_phi);
  fChain->SetBranchAddress("truth_vis_eta", &truth_vis_eta, &b_truth_vis_eta);
  fChain->SetBranchAddress("truth_vis_pt", &truth_vis_pt, &b_truth_vis_pt);
  fChain->SetBranchAddress("truth_vis_e", &truth_vis_e, &b_truth_vis_e);
  fChain->SetBranchAddress("truth_charge", &truth_charge, &b_truth_charge);
  fChain->SetBranchAddress("truth_pdgid", &truth_pdgid, &b_truth_pdgid);
  fChain->SetBranchAddress("truth_barcode", &truth_barcode, &b_truth_barcode);
  fChain->SetBranchAddress("truth_nChildren", &truth_nChildren, &b_truth_nChildren);
  fChain->SetBranchAddress("truth_nTauProngs", &truth_nTauProngs, &b_truth_nTauProngs);
  fChain->SetBranchAddress("truth_nPi0", &truth_nPi0, &b_truth_nPi0);
  fChain->SetBranchAddress("truth_isTauHad", &truth_isTauHad, &b_truth_isTauHad);
  fChain->SetBranchAddress("truth_parent_barcode", &truth_parent_barcode, &b_truth_parent_barcode);
  fChain->SetBranchAddress("truth_properTime", &truth_properTime, &b_truth_properTime);
  fChain->SetBranchAddress("truth_decayr", &truth_decayr, &b_truth_decayr);
  fChain->SetBranchAddress("track_isTracklet", &track_isTracklet, &b_track_isTracklet);
  fChain->SetBranchAddress("track_phi", &track_phi, &b_track_phi);
  fChain->SetBranchAddress("track_eta", &track_eta, &b_track_eta);
  fChain->SetBranchAddress("track_pt", &track_pt, &b_track_pt);
  fChain->SetBranchAddress("track_e", &track_e, &b_track_e);
  fChain->SetBranchAddress("track_charge", &track_charge, &b_track_charge);
  fChain->SetBranchAddress("track_KVUPhi", &track_KVUPhi, &b_track_KVUPhi);
  fChain->SetBranchAddress("track_KVUEta", &track_KVUEta, &b_track_KVUEta);
  fChain->SetBranchAddress("track_KVUPt", &track_KVUPt, &b_track_KVUPt);
  fChain->SetBranchAddress("track_KVUCharge", &track_KVUCharge, &b_track_KVUCharge);
  fChain->SetBranchAddress("track_KVUChi2Prob", &track_KVUChi2Prob, &b_track_KVUChi2Prob);
  fChain->SetBranchAddress("track_pixelBarrel_0", &track_pixelBarrel_0, &b_track_pixelBarrel_0);
  fChain->SetBranchAddress("track_pixelBarrel_1", &track_pixelBarrel_1, &b_track_pixelBarrel_1);
  fChain->SetBranchAddress("track_pixelBarrel_2", &track_pixelBarrel_2, &b_track_pixelBarrel_2);
  fChain->SetBranchAddress("track_pixelBarrel_3", &track_pixelBarrel_3, &b_track_pixelBarrel_3);
  fChain->SetBranchAddress("track_sctBarrel_0", &track_sctBarrel_0, &b_track_sctBarrel_0);
  fChain->SetBranchAddress("track_sctBarrel_1", &track_sctBarrel_1, &b_track_sctBarrel_1);
  fChain->SetBranchAddress("track_sctBarrel_2", &track_sctBarrel_2, &b_track_sctBarrel_2);
  fChain->SetBranchAddress("track_sctBarrel_3", &track_sctBarrel_3, &b_track_sctBarrel_3);
  fChain->SetBranchAddress("track_sctEndcap_0", &track_sctEndcap_0, &b_track_sctEndcap_0);
  fChain->SetBranchAddress("track_sctEndcap_1", &track_sctEndcap_1, &b_track_sctEndcap_1);
  fChain->SetBranchAddress("track_sctEndcap_2", &track_sctEndcap_2, &b_track_sctEndcap_2);
  fChain->SetBranchAddress("track_sctEndcap_3", &track_sctEndcap_3, &b_track_sctEndcap_3);
  fChain->SetBranchAddress("track_sctEndcap_4", &track_sctEndcap_4, &b_track_sctEndcap_4);
  fChain->SetBranchAddress("track_sctEndcap_5", &track_sctEndcap_5, &b_track_sctEndcap_5);
  fChain->SetBranchAddress("track_sctEndcap_6", &track_sctEndcap_6, &b_track_sctEndcap_6);
  fChain->SetBranchAddress("track_sctEndcap_7", &track_sctEndcap_7, &b_track_sctEndcap_7);
  fChain->SetBranchAddress("track_sctEndcap_8", &track_sctEndcap_8, &b_track_sctEndcap_8);
  fChain->SetBranchAddress("track_nPixelEndcap", &track_nPixelEndcap, &b_track_nPixelEndcap);
  fChain->SetBranchAddress("track_nPixHits", &track_nPixHits, &b_track_nPixHits);
  fChain->SetBranchAddress("track_nPixHoles", &track_nPixHoles, &b_track_nPixHoles);
  fChain->SetBranchAddress("track_nPixSharedHits", &track_nPixSharedHits, &b_track_nPixSharedHits);
  fChain->SetBranchAddress("track_nPixOutliers", &track_nPixOutliers, &b_track_nPixOutliers);
  fChain->SetBranchAddress("track_nPixDeadSensors", &track_nPixDeadSensors, &b_track_nPixDeadSensors);
  fChain->SetBranchAddress("track_nContribPixelLayers", &track_nContribPixelLayers, &b_track_nContribPixelLayers);
  fChain->SetBranchAddress("track_nGangedFlaggedFakes", &track_nGangedFlaggedFakes, &b_track_nGangedFlaggedFakes);
  fChain->SetBranchAddress("track_nPixelSpoiltHits", &track_nPixelSpoiltHits, &b_track_nPixelSpoiltHits);
  fChain->SetBranchAddress("track_nSCTHits", &track_nSCTHits, &b_track_nSCTHits);
  fChain->SetBranchAddress("track_nSCTHoles", &track_nSCTHoles, &b_track_nSCTHoles);
  fChain->SetBranchAddress("track_nSCTSharedHits", &track_nSCTSharedHits, &b_track_nSCTSharedHits);
  fChain->SetBranchAddress("track_nSCTOutliers", &track_nSCTOutliers, &b_track_nSCTOutliers);
  fChain->SetBranchAddress("track_nSCTDeadSensors", &track_nSCTDeadSensors, &b_track_nSCTDeadSensors);
  fChain->SetBranchAddress("track_nTRTHits", &track_nTRTHits, &b_track_nTRTHits);
  fChain->SetBranchAddress("track_nTRTHoles", &track_nTRTHoles, &b_track_nTRTHoles);
  fChain->SetBranchAddress("track_nTRTSharedHits", &track_nTRTSharedHits, &b_track_nTRTSharedHits);
  fChain->SetBranchAddress("track_nTRTDeadSensors", &track_nTRTDeadSensors, &b_track_nTRTDeadSensors);
  fChain->SetBranchAddress("track_nTRTOutliers", &track_nTRTOutliers, &b_track_nTRTOutliers);
  fChain->SetBranchAddress("track_dEdx", &track_dEdx, &b_track_dEdx);
  fChain->SetBranchAddress("track_ptCone20OverPt", &track_ptCone20OverPt, &b_track_ptCone20OverPt);
  fChain->SetBranchAddress("track_ptCone30OverPt", &track_ptCone30OverPt, &b_track_ptCone30OverPt);
  fChain->SetBranchAddress("track_ptCone40OverPt", &track_ptCone40OverPt, &b_track_ptCone40OverPt);
  fChain->SetBranchAddress("track_ptCone20OverKVUPt", &track_ptCone20OverKVUPt, &b_track_ptCone20OverKVUPt);
  fChain->SetBranchAddress("track_ptCone30OverKVUPt", &track_ptCone30OverKVUPt, &b_track_ptCone30OverKVUPt);
  fChain->SetBranchAddress("track_ptCone40OverKVUPt", &track_ptCone40OverKVUPt, &b_track_ptCone40OverKVUPt);
  fChain->SetBranchAddress("track_eTTopoClus20", &track_eTTopoClus20, &b_track_eTTopoClus20);
  fChain->SetBranchAddress("track_eTTopoClus30", &track_eTTopoClus30, &b_track_eTTopoClus30);
  fChain->SetBranchAddress("track_eTTopoClus40", &track_eTTopoClus40, &b_track_eTTopoClus40);
  fChain->SetBranchAddress("track_eTTopoCone20", &track_eTTopoCone20, &b_track_eTTopoCone20);
  fChain->SetBranchAddress("track_eTTopoCone30", &track_eTTopoCone30, &b_track_eTTopoCone30);
  fChain->SetBranchAddress("track_eTTopoCone40", &track_eTTopoCone40, &b_track_eTTopoCone40);
  fChain->SetBranchAddress("track_dRJet20", &track_dRJet20, &b_track_dRJet20);
  fChain->SetBranchAddress("track_dRJet50", &track_dRJet50, &b_track_dRJet50);
  fChain->SetBranchAddress("track_dRMSTrack", &track_dRMSTrack, &b_track_dRMSTrack);
  fChain->SetBranchAddress("track_dRMuon", &track_dRMuon, &b_track_dRMuon);
  fChain->SetBranchAddress("track_dRElectron", &track_dRElectron, &b_track_dRElectron);
  fChain->SetBranchAddress("track_d0", &track_d0, &b_track_d0);
  fChain->SetBranchAddress("track_d0Sig", &track_d0Sig, &b_track_d0Sig);
  fChain->SetBranchAddress("track_z0", &track_z0, &b_track_z0);
  fChain->SetBranchAddress("track_z0PV", &track_z0PV, &b_track_z0PV);
  fChain->SetBranchAddress("track_z0PVSinTheta", &track_z0PVSinTheta, &b_track_z0PVSinTheta);
  fChain->SetBranchAddress("track_chi2Prob", &track_chi2Prob, &b_track_chi2Prob);
  // fChain->SetBranchAddress("track_parameterCovMatrix", &track_parameterCovMatrix, &b_track_parameterCovMatrix);
  fChain->SetBranchAddress("track_electron_key", &track_electron_key, &b_track_electron_key);
  fChain->SetBranchAddress("track_muon_key", &track_muon_key, &b_track_muon_key);
  fChain->SetBranchAddress("track_truth_phi", &track_truth_phi, &b_track_truth_phi);
  fChain->SetBranchAddress("track_truth_eta", &track_truth_eta, &b_track_truth_eta);
  fChain->SetBranchAddress("track_truth_pt", &track_truth_pt, &b_track_truth_pt);
  fChain->SetBranchAddress("track_truth_e", &track_truth_e, &b_track_truth_e);
  fChain->SetBranchAddress("track_truth_charge", &track_truth_charge, &b_track_truth_charge);
  fChain->SetBranchAddress("track_truth_pdgid", &track_truth_pdgid, &b_track_truth_pdgid);
  fChain->SetBranchAddress("track_truth_barcode", &track_truth_barcode, &b_track_truth_barcode);
  fChain->SetBranchAddress("track_truth_nChildren", &track_truth_nChildren, &b_track_truth_nChildren);
  fChain->SetBranchAddress("track_truth_properTime", &track_truth_properTime, &b_track_truth_properTime);
  fChain->SetBranchAddress("track_truth_decayr", &track_truth_decayr, &b_track_truth_decayr);
  fChain->SetBranchAddress("track_truth_matchingProb", &track_truth_matchingProb, &b_track_truth_matchingProb);
  fChain->SetBranchAddress("track_truth_parent_phi", &track_truth_parent_phi, &b_track_truth_parent_phi);
  fChain->SetBranchAddress("track_truth_parent_eta", &track_truth_parent_eta, &b_track_truth_parent_eta);
  fChain->SetBranchAddress("track_truth_parent_pt", &track_truth_parent_pt, &b_track_truth_parent_pt);
  fChain->SetBranchAddress("track_truth_parent_e", &track_truth_parent_e, &b_track_truth_parent_e);
  fChain->SetBranchAddress("track_truth_parent_charge", &track_truth_parent_charge, &b_track_truth_parent_charge);
  fChain->SetBranchAddress("track_truth_parent_pdgid", &track_truth_parent_pdgid, &b_track_truth_parent_pdgid);
  fChain->SetBranchAddress("track_truth_parent_barcode", &track_truth_parent_barcode, &b_track_truth_parent_barcode);
  fChain->SetBranchAddress("track_truth_parent_nChildren", &track_truth_parent_nChildren, &b_track_truth_parent_nChildren);
  fChain->SetBranchAddress("track_truth_parent_decayr", &track_truth_parent_decayr, &b_track_truth_parent_decayr);

  Notify();
}

Bool_t MyAnalysis::Notify()
{
  // The Notify() function is called when a new file is opened. This
  // can be either for a new TTree in a TChain or when when a new TTree
  // is started when using PROOF. It is normally not necessary to make changes
  // to the generated code, but the routine can be extended by the
  // user if needed. The return value is currently not used.

  return kTRUE;
}

void MyAnalysis::Show(Long64_t entry)
{
  // Print contents of entry.
  // If entry is not specified, print current entry
  if (!fChain) return;
  fChain->Show(entry);
}
Int_t MyAnalysis::Cut(Long64_t entry)
{
  // This function may be called from Loop.
  // returns  1 if entry is accepted.
  // returns -1 otherwise.
  return 1;
}
#endif // #ifdef MyAnalysis_cxx

