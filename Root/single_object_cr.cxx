#include "MyAnalysis.h"

void MyAnalysis::electron_cr(struct track* track){
  if(!electrons->size()) return;


  TLorentzVector electron_vec, track_vec;
  electron_vec.SetPtEtaPhiE(electrons->at(0)->pt, electrons->at(0)->eta, electrons->at(0)->phi, electrons->at(0)->e);
  track_vec.SetPtEtaPhiE(track->pt, track->eta, track->phi, track->e);
  bool pass_jet100(false);
  bool pass_jet150(false);
  if(jets->size() > 0) {
    if(jets->at(0)->pt > 100.) pass_jet100 = true;
    if(jets->at(0)->pt > 150.) pass_jet150 = true;
  }

  if( nMuons == 0 &&
      missingET_TST_leleVeto_pt > 200. &&
      m_min_dphi_jet_met &&
      pass_jet100 &&
      has_bad_jet == 0 &&
      !is_bad_muon_missingET) {
    if(track->ptCone40OverPt < 0.04) {
      m_h_track_pt_single_e_highmet->Fill(track->pt, m_EventWeight);
      m_h_track_pt_oldbin_single_e_highmet->Fill(track->pt, m_EventWeight);
    }
    if( electrons->at(0)->key == track->electron_key ) {
      m_h_track_pt_single_e_leading_wo_trackiso_highmet->Fill(track->pt, m_EventWeight);
      m_h_track_pt_oldbin_single_e_leading_wo_trackiso_highmet->Fill(track->pt, m_EventWeight);
      if(track->ptCone40OverPt < 0.04) {
	m_h_track_pt_single_e_leading_highmet->Fill(track->pt, m_EventWeight);
	m_h_track_pt_oldbin_single_e_leading_highmet->Fill(track->pt, m_EventWeight);
      }
    }
  }


  if( nMuons == 0 &&
      missingET_TST_leleVeto_pt > 220. &&
      m_min_dphi_jet_met &&
      pass_jet150 &&
      has_bad_jet == 0 &&
      !is_bad_muon_missingET) {
    if( electrons->at(0)->key == track->electron_key ) {
      m_h_track_pt_single_e_leading_wo_trackiso_new_highmet->Fill(track->pt, m_EventWeight);
      m_h_track_pt_oldbin_single_e_leading_wo_trackiso_new_highmet->Fill(track->pt, m_EventWeight);
      if(track->ptCone40OverPt < 0.04) {
	m_h_track_pt_single_e_leading_new_highmet->Fill(track->pt, m_EventWeight);
	m_h_track_pt_oldbin_single_e_leading_new_highmet->Fill(track->pt, m_EventWeight);
      }
    }
  }
}
