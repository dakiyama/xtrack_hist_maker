#include "MyAnalysis.h"

void MyAnalysis::proxy_setter(){
  tracks = new std::vector<struct track*>(nTracks);
  unsigned ii_track(0);
  for(auto &track : *tracks) {
    track = new struct track;
    track->isTracklet = track_isTracklet->at(ii_track);
    track->phi = track_phi->at(ii_track);
    track->eta = track_eta->at(ii_track);
    track->pt = track_pt->at(ii_track);
    track->e = track_e->at(ii_track);
    track->charge = track_charge->at(ii_track);
    track->KVUPhi = track_KVUPhi->at(ii_track);
    track->KVUEta = track_KVUEta->at(ii_track);
    track->KVUPt = track_KVUPt->at(ii_track);
    track->KVUCharge = track_KVUCharge->at(ii_track);
    track->KVUChi2Prob = track_KVUChi2Prob->at(ii_track);
    track->pixelBarrel_0 = track_pixelBarrel_0->at(ii_track);
    track->pixelBarrel_1 = track_pixelBarrel_1->at(ii_track);
    track->pixelBarrel_2 = track_pixelBarrel_2->at(ii_track);
    track->pixelBarrel_3 = track_pixelBarrel_3->at(ii_track);
    track->sctBarrel_0 = track_sctBarrel_0->at(ii_track);
    track->sctBarrel_1 = track_sctBarrel_1->at(ii_track);
    track->sctBarrel_2 = track_sctBarrel_2->at(ii_track);
    track->sctBarrel_3 = track_sctBarrel_3->at(ii_track);
    track->sctEndcap_0 = track_sctEndcap_0->at(ii_track);
    track->sctEndcap_1 = track_sctEndcap_1->at(ii_track);
    track->sctEndcap_2 = track_sctEndcap_2->at(ii_track);
    track->sctEndcap_3 = track_sctEndcap_3->at(ii_track);
    track->sctEndcap_4 = track_sctEndcap_4->at(ii_track);
    track->sctEndcap_5 = track_sctEndcap_5->at(ii_track);
    track->sctEndcap_6 = track_sctEndcap_6->at(ii_track);
    track->sctEndcap_7 = track_sctEndcap_7->at(ii_track);
    track->sctEndcap_8 = track_sctEndcap_8->at(ii_track);
    track->nPixelEndcap = track_nPixelEndcap->at(ii_track);
    track->nPixHits = track_nPixHits->at(ii_track);
    track->nPixHoles = track_nPixHoles->at(ii_track);
    track->nPixSharedHits = track_nPixSharedHits->at(ii_track);
    track->nPixOutliers = track_nPixOutliers->at(ii_track);
    track->nPixDeadSensors = track_nPixDeadSensors->at(ii_track);
    track->nContribPixelLayers = track_nContribPixelLayers->at(ii_track);
    track->nGangedFlaggedFakes = track_nGangedFlaggedFakes->at(ii_track);
    track->nPixelSpoiltHits = track_nPixelSpoiltHits->at(ii_track);
    track->nSCTHits = track_nSCTHits->at(ii_track);
    track->nSCTHoles = track_nSCTHoles->at(ii_track);
    track->nSCTSharedHits = track_nSCTSharedHits->at(ii_track);
    track->nSCTOutliers = track_nSCTOutliers->at(ii_track);
    track->nSCTDeadSensors = track_nSCTDeadSensors->at(ii_track);
    track->nTRTHits = track_nTRTHits->at(ii_track);
    track->nTRTHoles = track_nTRTHoles->at(ii_track);
    track->nTRTSharedHits = track_nTRTSharedHits->at(ii_track);
    track->nTRTDeadSensors = track_nTRTDeadSensors->at(ii_track);
    track->nTRTOutliers = track_nTRTOutliers->at(ii_track);
    track->dEdx = track_dEdx->at(ii_track);
    track->ptCone20OverPt = track_ptCone20OverPt->at(ii_track);
    track->ptCone30OverPt = track_ptCone30OverPt->at(ii_track);
    track->ptCone40OverPt = track_ptCone40OverPt->at(ii_track);
    track->ptCone20OverKVUPt = track_ptCone20OverKVUPt->at(ii_track);
    track->ptCone30OverKVUPt = track_ptCone30OverKVUPt->at(ii_track);
    track->ptCone40OverKVUPt = track_ptCone40OverKVUPt->at(ii_track);
    track->eTTopoClus20 = track_eTTopoClus20->at(ii_track);
    track->eTTopoClus30 = track_eTTopoClus30->at(ii_track);
    track->eTTopoClus40 = track_eTTopoClus40->at(ii_track);
    track->eTTopoCone20 = track_eTTopoCone20->at(ii_track);
    track->eTTopoCone30 = track_eTTopoCone30->at(ii_track);
    track->eTTopoCone40 = track_eTTopoCone40->at(ii_track);
    track->dRJet20 = track_dRJet20->at(ii_track);
    track->dRJet50 = track_dRJet50->at(ii_track);
    track->dRMSTrack = track_dRMSTrack->at(ii_track);
    track->dRMuon = track_dRMuon->at(ii_track);
    track->dRElectron = track_dRElectron->at(ii_track);
    track->d0 = track_d0->at(ii_track);
    track->d0Sig = track_d0Sig->at(ii_track);
    track->z0 = track_z0->at(ii_track);
    track->z0PV = track_z0PV->at(ii_track);
    track->z0PVSinTheta = track_z0PVSinTheta->at(ii_track);
    track->chi2Prob = track_chi2Prob->at(ii_track);
    // track->parameterCovMatrix = track_parameterCovMatrix->at(ii_track);
    if(track_electron_key != nullptr) track->electron_key = track_electron_key->at(ii_track);
    if(track_muon_key != nullptr) track->muon_key = track_muon_key->at(ii_track);
    if(!is_data){
      track->truth_phi = track_truth_phi->at(ii_track);
      track->truth_eta = track_truth_eta->at(ii_track);
      track->truth_pt = track_truth_pt->at(ii_track);
      track->truth_e = track_truth_e->at(ii_track);
      track->truth_charge = track_truth_charge->at(ii_track);
      track->truth_pdgid = track_truth_pdgid->at(ii_track);
      track->truth_barcode = track_truth_barcode->at(ii_track);
      track->truth_nChildren = track_truth_nChildren->at(ii_track);
      track->truth_properTime = track_truth_properTime->at(ii_track);
      track->truth_decayr = track_truth_decayr->at(ii_track);
      track->truth_matchingProb = track_truth_matchingProb->at(ii_track);
      track->truth_parent_phi = track_truth_parent_phi->at(ii_track);
      track->truth_parent_eta = track_truth_parent_eta->at(ii_track);
      track->truth_parent_pt = track_truth_parent_pt->at(ii_track);
      track->truth_parent_e = track_truth_parent_e->at(ii_track);
      track->truth_parent_charge = track_truth_parent_charge->at(ii_track);
      track->truth_parent_pdgid = track_truth_parent_pdgid->at(ii_track);
      track->truth_parent_barcode = track_truth_parent_barcode->at(ii_track);
      track->truth_parent_nChildren = track_truth_parent_nChildren->at(ii_track);
      track->truth_parent_decayr = track_truth_parent_decayr->at(ii_track);
    } else {
      track->truth_phi = -999;
      track->truth_eta = -999;
      track->truth_pt = -999;
      track->truth_e = -999;
      track->truth_charge = -999;
      track->truth_pdgid = -999;
      track->truth_barcode = -999;
      track->truth_nChildren = -999;
      track->truth_properTime = -999;
      track->truth_decayr = -999;
      track->truth_matchingProb = -999;
      track->truth_parent_phi = -999;
      track->truth_parent_eta = -999;
      track->truth_parent_pt = -999;
      track->truth_parent_e = -999;
      track->truth_parent_charge = -999;
      track->truth_parent_pdgid = -999;
      track->truth_parent_barcode = -999;
      track->truth_parent_nChildren = -999;
      track->truth_parent_decayr = -999;
    }
    track->truth_flag = false;
    track->truth_deltar = -999.;
    ++ii_track;
  }

  truths = new std::vector<struct truth*>(nTruths);
  unsigned ii_truth(0);
  if(!is_data){
    for(auto &truth : *truths) {
      truth = new struct truth;
      truth->phi = truth_phi->at(ii_truth);
      truth->eta = truth_eta->at(ii_truth);
      truth->pt = truth_pt->at(ii_truth);
      truth->e = truth_e->at(ii_truth);
      truth->charge = truth_charge->at(ii_truth);
      truth->pdgid = truth_pdgid->at(ii_truth);
      truth->barcode = truth_barcode->at(ii_truth);
      truth->nChildren = truth_nChildren->at(ii_truth);
      truth->vis_phi = truth_vis_phi->at(ii_truth);
      truth->vis_eta = truth_vis_eta->at(ii_truth);
      truth->vis_pt = truth_vis_pt->at(ii_truth);
      truth->vis_e = truth_vis_e->at(ii_truth);
      truth->nTauProngs = truth_nTauProngs->at(ii_truth);
      truth->isTauHad = truth_isTauHad->at(ii_truth);
      truth->nPi0 = truth_nPi0->at(ii_truth); // new variable take care
      truth->parent_barcode = truth_parent_barcode->at(ii_truth);
      truth->properTime = truth_properTime->at(ii_truth);
      truth->decayr = truth_decayr->at(ii_truth);
      ++ii_truth;
    }
  }

  muons = new std::vector<struct lepton*>(nMuons);
  unsigned ii_muon(0);
  for(auto &muon : *muons){
    muon = new struct lepton;
    muon->phi = muon_phi->at(ii_muon);
    muon->eta = muon_eta->at(ii_muon);
    muon->pt = muon_pt->at(ii_muon);
    muon->e = muon_e->at(ii_muon);
    muon->charge = muon_charge->at(ii_muon);
    muon->ptVarCone20OverPt = muon_ptVarCone20OverPt->at(ii_muon);
    muon->ptVarCone30OverPt = muon_ptVarCone30OverPt->at(ii_muon);
    muon->ptVarCone40OverPt = muon_ptVarCone40OverPt->at(ii_muon);
    muon->eTTopoCone20 = muon_eTTopoCone20->at(ii_muon);
    muon->eTTopoCone30 = muon_eTTopoCone30->at(ii_muon);
    muon->eTTopoCone40 = muon_eTTopoCone40->at(ii_muon);
    muon->quality = muon_quality->at(ii_muon);
    if(muon_isSignal != nullptr) muon->isSignal = muon_isSignal->at(ii_muon);
    muon->isTrigMatch = muon_isTrigMatch->at(ii_muon);
    if(muon_key != nullptr) muon->key = muon_key->at(ii_muon);
    ++ii_muon;
  }

  muon_tracks = new std::vector<struct lepton_track*>(nMuons);
  unsigned ii_muon_track(0);
  for(auto &muon_track : *muon_tracks){
    muon_track = new struct lepton_track;
    muon_track->lepton_phi = muon_phi->at(ii_muon_track);
    muon_track->lepton_eta = muon_eta->at(ii_muon_track);
    muon_track->lepton_pt = muon_pt->at(ii_muon_track);
    muon_track->lepton_e = muon_e->at(ii_muon_track);
    muon_track->lepton_quality = muon_quality->at(ii_muon_track);
    muon_track->lepton_isTrigMatch = muon_isTrigMatch->at(ii_muon_track);
    muon_track->mstrack_phi = 0.;
    muon_track->mstrack_eta = 0.;
    muon_track->mstrack_pt = 0.;
    muon_track->mstrack_e = 0.;
    muon_track->mstrack_charge = 0;
    muon_track->stdtrack_phi = 0.;
    muon_track->stdtrack_eta = 0.;
    muon_track->stdtrack_pt = 0.;
    muon_track->stdtrack_e = 0.;
    muon_track->stdtrack_charge = 0;
    muon_track->stdtrack_d0 = 0.;
    muon_track->stdtrack_deltaR = 1000.;
    muon_track->stdtrack_ptcone40overpt = 0.;
    muon_track->stdtrack_layer = "";
    muon_track->tracklet_phi = 0.;
    muon_track->tracklet_eta = 0.;
    muon_track->tracklet_vcpt = 0.;
    muon_track->tracklet_pt = 0.;
    muon_track->tracklet_e = 0.;
    muon_track->tracklet_charge = 0;
    muon_track->tracklet_vccharge = 0;
    muon_track->tracklet_d0 = 0.;
    muon_track->tracklet_deltaR = 1000.;
    muon_track->tracklet_ptcone40overpt = 0.;
    muon_track->tracklet_layer = "";
    ++ii_muon_track;
  }
  electron_tracks = new std::vector<struct lepton_track*>(nElectrons);
  unsigned ii_electron_track(0);
  for(auto &electron_track : *electron_tracks){
    electron_track = new struct lepton_track;
    electron_track->lepton_phi = electron_phi->at(ii_electron_track);
    electron_track->lepton_eta = electron_eta->at(ii_electron_track);
    electron_track->lepton_pt = electron_pt->at(ii_electron_track);
    electron_track->lepton_e = electron_e->at(ii_electron_track);
    electron_track->lepton_quality = electron_quality->at(ii_electron_track);
    electron_track->lepton_isTrigMatch = electron_isTrigMatch->at(ii_electron_track);
    electron_track->mstrack_phi = 0.;
    electron_track->mstrack_eta = 0.;
    electron_track->mstrack_pt = 0.;
    electron_track->mstrack_e = 0.;
    electron_track->mstrack_charge = 0.;
    electron_track->stdtrack_phi = 0.;
    electron_track->stdtrack_eta = 0.;
    electron_track->stdtrack_pt = 0.;
    electron_track->stdtrack_e = 0.;
    electron_track->stdtrack_charge = 0.;
    electron_track->stdtrack_chi2Prob = 0.;
    electron_track->stdtrack_deltaR_MS = 1000.;
    electron_track->stdtrack_deltaR = 1000.;
    electron_track->stdtrack_ptcone40overpt = 0.;
    electron_track->stdtrack_chi2Prob = 0.;
    electron_track->stdtrack_layer = "";
    electron_track->tracklet_phi = 0.;
    electron_track->tracklet_eta = 0.;
    electron_track->tracklet_pt = 0.;
    electron_track->tracklet_deltaR_MS = 1000.;
    electron_track->tracklet_vcphi = 0.;
    electron_track->tracklet_vcpt = 0.;
    electron_track->tracklet_vceta = 0.;
    electron_track->tracklet_e = 0.;
    electron_track->tracklet_charge = 0.;
    electron_track->tracklet_chi2Prob = 0.;
    electron_track->tracklet_chi2Prob = 0.;
    electron_track->tracklet_vccharge = 0.;
    electron_track->tracklet_d0 = 0.;
    electron_track->tracklet_deltaR = 1000.;
    electron_track->tracklet_ptcone40overpt = 0.;
    electron_track->tracklet_layer = "";
    ++ii_electron_track;
  }
  electrons = new std::vector<struct lepton*>(nElectrons);
  unsigned ii_electron(0);
  for(auto &electron : *electrons){
    electron = new struct lepton;
    electron->phi = electron_phi->at(ii_electron);
    electron->eta = electron_eta->at(ii_electron);
    electron->pt =  electron_pt->at(ii_electron);
    electron->e = electron_e->at(ii_electron);
    electron->charge = electron_charge->at(ii_electron);
    electron->ptVarCone20OverPt = electron_ptVarCone20OverPt->at(ii_electron);
    electron->ptVarCone30OverPt = -999.;
    electron->ptVarCone40OverPt = -999.;
    electron->eTTopoCone20 = electron_eTTopoCone20->at(ii_electron);
    electron->eTTopoCone30 = -999.;
    electron->eTTopoCone40 = -999.;
    electron->quality = electron_quality->at(ii_electron);
    if(electron_isSignal != nullptr) electron->isSignal = electron_isSignal->at(ii_electron);
    electron->isTrigMatch = electron_isTrigMatch->at(ii_electron);
    if(electron_key != nullptr) electron->key = electron_key->at(ii_electron);
    ++ii_electron;
  }
  taus = new std::vector<struct tau*>(nTaus);
  unsigned ii_tau(0);
  for(auto &tau : *taus){
    tau = new struct tau;
    tau->phi = tau_phi->at(ii_tau);
    tau->eta = tau_eta->at(ii_tau);
    tau->pt =  tau_pt->at(ii_tau);
    tau->e = tau_e->at(ii_tau);
    tau->charge = tau_charge->at(ii_tau);
    tau->nprongs = tau_nprongs->at(ii_tau);
    tau->BDTJetScore = tau_BDTJetScore->at(ii_tau);
    tau->isMedium = tau_isMedium->at(ii_tau);
    // tau->track_phi = tau_track_phi->at(ii_tau);
    // tau->track_eta = tau_track_eta->at(ii_tau);
    // tau->track_pt = tau_track_pt->at(ii_tau);
    // tau->track_e = tau_track_e->at(ii_tau);
    ++ii_tau;
  }
  jets = new std::vector<struct jet*>(nJets);
  unsigned ii_jet(0);
  for(auto &jet : *jets){
    jet = new struct jet;
    jet->phi = jet_phi->at(ii_jet);
    jet->eta = jet_eta->at(ii_jet);
    jet->pt = jet_pt->at(ii_jet);
    jet->e = jet_e->at(ii_jet);
    // jet->isBJet = jet_isBJet->at(ii_jet);
    ++ii_jet;
  }

  truthjets = new std::vector<struct jet*>(nTruthJets);
  unsigned ii_truthjet(0);
  for(auto &truthjet : *truthjets){
    truthjet = new struct jet;
    truthjet->phi = truthjet_phi->at(ii_truthjet);
    truthjet->eta = truthjet_eta->at(ii_truthjet);
    truthjet->pt = truthjet_pt->at(ii_truthjet);
    truthjet->e = truthjet_e->at(ii_truthjet);
    ++ii_jet;
  }

  caloClusters = new std::vector<struct jet*>(nCaloClusters);
  unsigned ii_cluster(0);
  for(auto &caloCluster : *caloClusters){
    caloCluster = new struct jet;
    caloCluster->phi = caloCluster_phi->at(ii_cluster);
    caloCluster->eta = caloCluster_eta->at(ii_cluster);
    caloCluster->pt = caloCluster_pt->at(ii_cluster);
    caloCluster->e = caloCluster_e->at(ii_cluster);
    ++ii_cluster;
  }

  msTracks = new std::vector<struct msTrack*>(nMSTracks);
  unsigned ii_msTrack(0);
  for(auto &msTrack : *msTracks){
    msTrack = new struct msTrack;
    msTrack->phi = msTrack_phi->at(ii_msTrack);
    msTrack->eta = msTrack_eta->at(ii_msTrack);
    msTrack->pt = msTrack_pt->at(ii_msTrack);
    msTrack->e = msTrack_e->at(ii_msTrack);
    //msTrack->charge = msTrack_charge->at(ii_msTrack);
    ++ii_msTrack;
  }
  std::sort(tracks->begin()   , tracks->end()   , [](struct track*  a, struct track*  b){return a->pt > b->pt;});
  std::sort(muons->begin()    , muons->end()    , [](struct lepton* a, struct lepton* b){return a->pt > b->pt;});
  std::sort(muon_tracks->begin(), muon_tracks->end(), [](struct lepton_track* a, struct lepton_track* b){return a->lepton_pt > b->lepton_pt;});
  std::sort(electron_tracks->begin(), electron_tracks->end(), [](struct lepton_track* a, struct lepton_track* b){return a->lepton_pt > b->lepton_pt;});
  std::sort(electrons->begin(), electrons->end(), [](struct lepton* a, struct lepton* b){return a->pt > b->pt;});
  std::sort(taus->begin()     , taus->end()     , [](struct tau*    a, struct tau*    b){return a->pt > b->pt;});
  std::sort(jets->begin()     , jets->end()     , [](struct jet*    a, struct jet*    b){return a->pt > b->pt;});
  std::sort(caloClusters->begin(), caloClusters->end(), [](struct jet*    a, struct jet*    b){return a->pt > b->pt;});
  std::sort(truthjets->begin(), truthjets->end(), [](struct jet*    a, struct jet*    b){return a->pt > b->pt;});
  std::sort(msTracks->begin() , msTracks->end() , [](struct msTrack*a, struct msTrack*b){return a->pt > b->pt;});
}

void MyAnalysis::struct_clear(){

  if(tracks != nullptr){
    for(auto &track : *tracks){
      delete track;
      track = nullptr;
    }
  }
  if(truths != nullptr){
    for(auto &truth : *truths){
      delete truth;
      truth = nullptr;
    }
  }
  if(muons != nullptr){
    for(auto &muon : *muons){
      delete muon;
      muon = nullptr;
    }
  }
  if(muon_tracks != nullptr){
    for(auto &muon_track : *muon_tracks){
      delete muon_track;
      muon_track = nullptr;
    }
  }
  if(electron_tracks != nullptr){
    for(auto &electron_track : *electron_tracks){
      delete electron_track;
      electron_track = nullptr;
    }
  }
  if(electrons != nullptr){
    for(auto &electron : *electrons){
      delete electron;
      electron = nullptr;
    }
  }
  if(taus != nullptr){
    for(auto &tau : *taus){
      delete tau;
      tau = nullptr;
    }
  }
  if(jets != nullptr){
    for(auto &jet : *jets){
      delete jet;
      jet = nullptr;
    }
  }
  if(truthjets != nullptr){
    for(auto &truthjet : *truthjets){
      delete truthjet;
      truthjet = nullptr;
    }
  }
  if(caloClusters != nullptr){
    for(auto &caloCluster : *caloClusters){
      delete caloCluster;
      caloCluster = nullptr;
    }
  }
  if(msTracks != nullptr){
    for(auto &msTrack : *msTracks){
      delete msTrack;
      msTrack = nullptr;
    }
  }

  delete tracks;
  delete truths;
  delete muons;
  delete muon_tracks;
  delete electron_tracks;
  delete electrons;
  delete taus;
  delete jets;
  delete caloClusters;
  delete truthjets;
  delete msTracks;
  tracks = nullptr;
  truths = nullptr;
  muons = nullptr;
  muon_tracks = nullptr;
  electron_tracks = nullptr;
  electrons = nullptr;
  taus = nullptr;
  jets = nullptr;
  caloClusters = nullptr;
  truthjets = nullptr;
  msTracks = nullptr;
}
