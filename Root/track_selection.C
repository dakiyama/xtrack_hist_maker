#include "MyAnalysis.h"

bool MyAnalysis::preselection(struct track* track, std::string purpose) {

  // ===== common selection ===================================================== //
  //  pt > 10 GeV && ganged+outlier+spoilt == 0 && chi2Prob > 0.1 && dRjet > 0.4  //
  // ============================================================================ //

  if(track->pt > 10.)         {} else return false;
  if(track->nPixHoles + track->nSCTHoles == 0) {} else return false;
  if(track->nGangedFlaggedFakes == 0 && track->nPixOutliers == 0 && track->nPixelSpoiltHits == 0) {} else return false;
  if(m_selected_KindOfTrack != "threelayer") {
    if(track->chi2Prob > 0.1)   {} else return false;
  }

  // for signal region
  if(purpose == "sr") {
    if(track->dRJet50 > 0.4)    {} else return false;
    if(track->dRMSTrack > 0.4)  {} else return false;
    if(track->dRMuon > 0.4)     {} else return false;
    if(track->dRElectron > 0.4) {} else return false;
  }

  // for hadron CR (smearing)
  if(purpose == "smearing") {
    if(fabs(track->eta) > 0.1 && fabs(track->eta) < 1.9) {} else return false;
    if(track->nTRTHits >= 15) {} else return false;
    if(track->ptCone40OverPt < 0.04) {} else return false;
    if(track->eTTopoCone20 > 3.) {} else return false;
    if(track->eTTopoClus40 / (track->ptCone40OverPt * track->pt) > 0.5) {} else return false;
    if(track->dRJet50 > 0.4)    {} else return false;
    if(track->dRMSTrack > 0.4)  {} else return false;
    if(track->dRMuon > 0.4)     {} else return false;
    if(track->dRElectron > 0.4) {} else return false;
    if(fabs(track->d0Sig) < 1.5) {} else return false;
    if(fabs(track->z0PVSinTheta) < 0.5) {} else return false;
  }

  // single lepton region
  if(purpose == "single_ele") {
    if(fabs(track->eta) > 0.1 && fabs(track->eta) < 1.9) {} else return false;
    //if(track->ptCone40OverPt < 0.04) {} else return false;
    if(track->dRJet50 > 0.4)    {} else return false;
    if(track->dRMSTrack > 0.4)  {} else return false;
    if(track->dRMuon > 0.4)     {} else return false;
    if(track->electron_key > 0) {} else return false;
    if(fabs(track->d0Sig) < 1.5) {} else return false;
    if(fabs(track->z0PVSinTheta) < 0.5) {} else return false;
  }

  // for tfpix only
  if(purpose == "tfpixonly_track") {
    if(fabs(track->eta) > 0.1 && fabs(track->eta) < 1.9) {} else return false;
    // if(track->ptCone40OverPt < 0.04) {} else return false;
    if(track->dRJet50 > 0.4)    {} else return false;
    if(track->dRMSTrack > 0.4)  {} else return false;
    if(track->dRMuon > 0.4)     {} else return false;
    if(fabs(track->d0Sig) < 1.5) {} else return false;
    if(fabs(track->z0PVSinTheta) < 0.5) {} else return false;
  }

  if(purpose == "tfpixonly_tracklet") {
    if(track->nSCTHits == 0) {} else return false;
    if(fabs(track->eta) > 0.1 && fabs(track->eta) < 1.9) {} else return false;
    // if(track->ptCone40OverPt < 0.04) {} else return false;
    if(track->dRJet50 > 0.4)    {} else return false;
    if(track->dRMSTrack > 0.4)  {} else return false;
    if(track->dRMuon > 0.4)     {} else return false;
    if(fabs(track->d0Sig) < 1.5) {} else return false;
    if(fabs(track->z0PVSinTheta) < 0.5) {} else return false;
  }

  // for tfcaloveto (electron)
  if(purpose == "tfcaloveto") {
    if(fabs(track->eta) > 0.1 && fabs(track->eta) < 1.9) {} else return false;
    if(track->ptCone40OverPt < 0.04) {} else return false;
    if(track->dRJet50 > 0.4)    {} else return false;
    if(track->dRMSTrack > 0.4)  {} else return false;
    if(track->dRMuon > 0.4)     {} else return false;
    if(fabs(track->d0Sig) < 1.5) {} else return false;
    if(fabs(track->z0PVSinTheta) < 0.5) {} else return false;
  }

  // for crcaloveto
  if(purpose == "crcaloveto") {
    if(fabs(track->eta) > 0.1 && fabs(track->eta) < 1.9) {} else return false;
    if(track->ptCone40OverPt < 0.04) {} else return false;
    if(track->dRMSTrack > 0.4)  {} else return false;
    if(track->dRMuon > 0.4)     {} else return false;
    // if(fabs(track->d0Sig) < 1.5) {} else return false;
    // if(fabs(track->z0PVSinTheta) < 0.5) {} else return false;
  }

  // for retracking leptons
  if(purpose == "retracking") {
    if(track->dRJet50 > 0.4)    {} else return false;
    if(m_selected_KinematicalRegion == "zee") {
      if(track->dRMuon > 0.4) {} else return false;
      if(track->dRMSTrack > 0.4) {} else return false;
    }
    if(m_selected_KinematicalRegion == "zmumu") {
      if(track->dRElectron > 0.4) {} else return false;
    }
    if(std::fabs(track->d0Sig) < 1.5) {} else return false;
    if(std::fabs(track->z0PVSinTheta) < 0.5) {} else return false;
  }

  return true;
}


bool MyAnalysis::blind(struct track* track) {
  bool is_blinded(false);
  if(!m_is_vc_default) {
    if(is_data &&
       m_selected_KinematicalRegion == "highmet" && 
       (m_selected_KindOfTrack == "fourlayer" ||
	m_selected_KindOfTrack == "threelayer") &&
       fabs(track->eta) > 0.1 && fabs(track->eta) < 1.9 &&
       track->ptCone40OverPt < 0.04 &&
       track->dRJet50 > 0.4 &&
       fabs(track->d0Sig) < 1.5 &&
       fabs(track->z0PVSinTheta) < 0.5 &&
       track->eTTopoClus20 < 5. &&
       track->pt > 60.)
      is_blinded = true;
  }
  if(m_is_vc_default) {
    if(is_data &&
       m_selected_KinematicalRegion == "highmet" && 
       (m_selected_KindOfTrack == "fourlayer" ||
	m_selected_KindOfTrack == "threelayer") &&
       fabs(track->KVUEta) > 0.1 && fabs(track->KVUEta) < 1.9 &&
       track->ptCone40OverKVUPt < 0.04 &&
       track->dRJet50 > 0.4 &&
       fabs(track->d0Sig) < 1.5 &&
       fabs(track->z0PVSinTheta) < 0.5 &&
       track->eTTopoClus20 < 5. &&
       track->KVUPt > 60.)
      is_blinded = true;
  }
  return is_blinded;
}

void MyAnalysis::track_layer_finder (struct track* track) {
  m_selected_KindOfTrack = "none";
  if(track->pixelBarrel_0 && track->pixelBarrel_1 && track->pixelBarrel_2 && track->nPixelEndcap == 0) {
    if(track->pixelBarrel_3 && track->nSCTHits >= 6) {
      m_selected_KindOfTrack = "stdtrack";
    }
    if(track->nSCTHits == 0){
      if(!track->pixelBarrel_3) m_selected_KindOfTrack = "threelayer";
      if(track->pixelBarrel_3)  m_selected_KindOfTrack = "fourlayer";
      // } else if(!track->sctEndcap_0 && !track->sctEndcap_1 && !track->sctEndcap_2 && !track->sctEndcap_3 && !track->sctEndcap_4 && !track->sctEndcap_5 && !track->sctEndcap_6 && !track->sctEndcap_7 && !track->sctEndcap_8 && track->isTracklet) {
      //   if(track->sctBarrel_0 && !track->sctBarrel_1 && !track->sctBarrel_2 && !track->sctBarrel_3) m_selected_KindOfTrack = "fivelayer";
      //   if(track->sctBarrel_0 && track->sctBarrel_1 && !track->sctBarrel_2 && !track->sctBarrel_3) m_selected_KindOfTrack = "sixlayer";
    }
  }
}
