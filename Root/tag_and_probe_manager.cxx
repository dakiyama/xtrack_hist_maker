#include "MyAnalysis.h"

bool MyAnalysis::tag_and_probe_manager(struct track *track) {
  if(nMuons) return false;
  if(!nElectrons) return false;
  if(!pass_electron) return false;
  //if(has_bad_jet) return false;
  //if(is_bad_muon_missingET) return false;

  string trackType("");
  if(preselection(track, "tfpixonly_tracklet")) trackType = "tracklet";
  else if(preselection(track, "tfpixonly_track")) trackType = "track";
  else return false;

  for(const auto &electron : *electrons) {
    //if(electron->isSignal && electron->pt > 30. && electron->isTrigMatch) {} else continue;
    if(electron->quality >= 2 && electron->pt > 30.) {} else continue;
    TLorentzVector *vec_cluster = get_probe_cluster(track);
    if(vec_cluster == nullptr) continue;

    TLorentzVector *vec_electron = new TLorentzVector();
    vec_electron->SetPtEtaPhiE(electron->pt, electron->eta, electron->phi, electron->e);
    double mass_e_cluster = (*vec_cluster + *vec_electron).M();

    // overlap check
    bool is_electron(false);
    if(electron_key != nullptr) {
      if(electron->key == track->electron_key) continue;
      if(track->electron_key > 0) is_electron = true;
    }
    TLorentzVector vec_track;
    vec_track.SetPtEtaPhiE(track->pt, track->eta, track->phi, track->e);
    if(vec_electron->DeltaR(vec_track) < 0.001) continue;

    // fill histogram
    if(trackType == "tracklet") {
      if(track->ptCone40OverPt < 0.04) {
      std::cout<<"hoge"<<std::endl;

	if(track->charge * electron->charge == 1) {
	  if(m_selected_KindOfTrack == "fourlayer") {
	    m_h_zmass_4ltracklet_ss_tfpix->Fill(mass_e_cluster, m_EventWeight);
	    if(std::fabs(zmass - mass_e_cluster) < 10.) {
	      m_h_probe_4ltracklet_pt_ss_tfpix->Fill(vec_cluster->Pt(), m_EventWeight);
	      m_h_probe_4ltracklet_pt_eta_ss_tfpix->Fill(vec_cluster->Pt(), vec_cluster->Eta(), m_EventWeight);
	    }
	  }
	  if(m_selected_KindOfTrack == "threelayer") {
	    m_h_zmass_3ltracklet_ss_tfpix->Fill(mass_e_cluster, m_EventWeight);
	    if(std::fabs(zmass - mass_e_cluster) < 10.) {
	      m_h_probe_3ltracklet_pt_ss_tfpix->Fill(vec_cluster->Pt(), m_EventWeight);
	      m_h_probe_3ltracklet_pt_eta_ss_tfpix->Fill(vec_cluster->Pt(), vec_cluster->Eta(), m_EventWeight);
	    }
	  }
	} else if (track->charge * electron->charge == -1) {
	  if(m_selected_KindOfTrack == "fourlayer") {
	    m_h_zmass_4ltracklet_os_tfpix->Fill(mass_e_cluster, m_EventWeight);
	    if(std::fabs(zmass - mass_e_cluster) < 10.) {
	      m_h_probe_4ltracklet_pt_os_tfpix->Fill(vec_cluster->Pt(), m_EventWeight);
	      m_h_probe_4ltracklet_pt_eta_os_tfpix->Fill(vec_cluster->Pt(), vec_cluster->Eta(), m_EventWeight);
	    }
	  }
	  if(m_selected_KindOfTrack == "threelayer") {
	    m_h_zmass_3ltracklet_os_tfpix->Fill(mass_e_cluster, m_EventWeight);
	    if(std::fabs(zmass - mass_e_cluster) < 10.) {
	      m_h_probe_3ltracklet_pt_os_tfpix->Fill(vec_cluster->Pt(), m_EventWeight);
	      m_h_probe_3ltracklet_pt_eta_os_tfpix->Fill(vec_cluster->Pt(), vec_cluster->Eta(), m_EventWeight);
	    }
	  }
	} else std::cout<<"invalid charge"<<std::endl;
      }
    }

    if(trackType == "track" && is_electron) {
      if(track->charge * electron->charge == 1) {
	m_h_zmass_track_ss_wo_trackiso_tfpix->Fill(mass_e_cluster, m_EventWeight);
	if(track->ptCone40OverPt < 0.04) {
	  m_h_zmass_track_ss_tfpix->Fill(mass_e_cluster, m_EventWeight);
	}
	if(std::fabs(zmass - mass_e_cluster) < 10.) {
	  m_h_probe_track_pt_ss_wo_trackiso_tfpix->Fill(vec_cluster->Pt(), m_EventWeight);
	  m_h_probe_track_pt_eta_ss_wo_trackiso_tfpix->Fill(vec_cluster->Pt(), vec_cluster->Eta(), m_EventWeight);
	  if(track->ptCone40OverPt < 0.04) {
	    m_h_probe_track_pt_ss_tfpix->Fill(vec_cluster->Pt(), m_EventWeight);
	    m_h_probe_track_pt_eta_ss_tfpix->Fill(vec_cluster->Pt(), vec_cluster->Eta(), m_EventWeight);
	  }
	}
      } else if (track->charge * electron->charge == -1) {
	m_h_zmass_track_os_wo_trackiso_tfpix->Fill(mass_e_cluster, m_EventWeight);
	if(track->ptCone40OverPt < 0.04) {
	  m_h_zmass_track_os_tfpix->Fill(mass_e_cluster, m_EventWeight);
	}
	if(std::fabs(zmass - mass_e_cluster) < 10.) {
	  m_h_probe_track_pt_os_wo_trackiso_tfpix->Fill(vec_cluster->Pt(), m_EventWeight);
	  m_h_probe_track_pt_eta_os_wo_trackiso_tfpix->Fill(vec_cluster->Pt(), vec_cluster->Eta(), m_EventWeight);
	  if(track->ptCone40OverPt < 0.04) {
	    m_h_probe_track_pt_os_tfpix->Fill(vec_cluster->Pt(), m_EventWeight);
	    m_h_probe_track_pt_eta_os_tfpix->Fill(vec_cluster->Pt(), vec_cluster->Eta(), m_EventWeight);
	  }
	}
      } else std::cout<<"invalid charge"<<std::endl;
    }

    if(vec_cluster != nullptr) {
      delete vec_cluster;
      vec_cluster = nullptr;
    }
  }

  return true;
}

TLorentzVector* MyAnalysis::get_probe_cluster(struct track *track) {
  TLorentzVector *vec_cluster = new TLorentzVector();
  TLorentzVector *vec_cluster_min_dr;
  TLorentzVector vec_track;
  vec_track.SetPtEtaPhiM(track->pt, track->eta, track->phi, emass);
  double min_dr(999.);

  for(const auto &cluster : *caloClusters) {
    if(cluster->pt > 10.) {} else continue;
    if(std::fabs(cluster->eta) > 0.1 && std::fabs(cluster->eta) < 1.9) {} else continue;
    vec_cluster->SetPtEtaPhiM(cluster->pt, cluster->eta, cluster->phi, emass);
    if(vec_cluster->DeltaR(vec_track) < min_dr) {
      min_dr = vec_cluster->DeltaR(vec_track);
      vec_cluster_min_dr = vec_cluster;
    }
  }

  if(min_dr > 0.2) {
    delete vec_cluster; vec_cluster = nullptr;
    vec_cluster_min_dr = nullptr;
  }

  return vec_cluster_min_dr;
}
