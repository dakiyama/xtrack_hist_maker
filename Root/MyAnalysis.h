//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Apr 29 00:01:31 2022 by ROOT version 6.24/06
// from TChain MyTree/
//////////////////////////////////////////////////////////

#ifndef MyAnalysis_h
#define MyAnalysis_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <iostream>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "TH1.h"
#include "TH2.h"

class MyAnalysis {
 public :
  TTree          *fChain;   //!pointer to the analyzed TTree or TChain
  Int_t           fCurrent; //!current Tree number in a TChain

  // Fixed size dimensions of array or collections stored in the TTree if any.
  TFile* m_file_skim;
  TFile* m_file_tf_caloveto;
  TTree* m_skim_tree_signal;
  TTree* m_skim_tree_fake;
  TTree* m_skim_tree_hadfake;
  TH2D* m_h_ele_caloiso_432to0;
  TH1D* m_h_had_corr_caloiso_432to0;
  TH1D* m_h_tracklet_z0Sin_nocut;
  TH1D* m_h_tracklet_z0Sin_4l;
  TH1D* m_h_tracklet_layer;
  TH1D* m_h_event_met;
  TH2D* m_h_event_met_jetpt;
  TH1D* m_h_event_jetpt;
  TH1D* m_h_event_deltaphi_jet_met;
  TH1D* m_h_event_tau_nprongs;
  TH1D* m_h_event_tau_lephad;
  TH1D* m_h_event_truth_mt;
  TH1D* m_h_event_reco_mt;
  TH1D* m_h_event_single_lepton_mass_3l;
  TH1D* m_h_event_single_lepton_mass_4l;
  TH1D* m_h_event_single_lepton_mass_3l_SS;
  TH1D* m_h_event_single_lepton_mass_4l_SS;
  TH1D* m_h_event_single_lepton_mass_3l_OS;
  TH1D* m_h_event_single_lepton_mass_4l_OS;
  TH1D* m_h_event_single_lepton_collinear_mass_3l;
  TH1D* m_h_event_single_lepton_collinear_mass_4l;
  TH1D* m_h_event_single_lepton_collinear_mass_3l_SS;
  TH1D* m_h_event_single_lepton_collinear_mass_4l_SS;
  TH1D* m_h_event_single_lepton_collinear_mass_3l_OS;
  TH1D* m_h_event_single_lepton_collinear_mass_4l_OS;
  TH1D* m_h_event_truth_z_truth_mass;
  TH1D* m_h_event_truth_z_visible_mass_lephad;
  TH1D* m_h_event_truth_z_visible_mass_hadhad;
  TH1D* m_h_event_truth_z_collinear_mass_lephad;
  TH1D* m_h_event_truth_z_collinear_mass_hadhad;
  TH1D* m_h_event_truth_z_tracklet_visible_mass;
  TH1D* m_h_event_truth_z_tracklet_collinear_mass;
  TH1D* m_h_event_reco_z_tracklet_visible_mass;
  TH1D* m_h_event_reco_z_tracklet_collinear_mass;
  TH1D* m_h_event_truth_eta;
  TH1D* m_h_event_truth_pt;
  TH1D* m_h_event_truth_pt_wide;
  TH1D* m_h_event_truth_pt_bin7;
  TH1D* m_h_event_truthjet_eta;
  TH1D* m_h_event_truthjet_pt;
  TH1D* m_h_event_truthjet_pt_wide;
  TH1D* m_h_event_truthjet_pt_bin7;
  TH1D* m_h_event_truth_pt_tau;
  TH1D* m_h_event_truth_pt_tau_1p0n;
  TH1D* m_h_event_truth_pt_tau_1p1n;
  TH1D* m_h_event_truth_pt_tau_3p0n;
  TH1D* m_h_event_truth_vis_pt_tau;
  TH1D* m_h_event_truth_vis_pt_tau_1p0n;
  TH1D* m_h_event_truth_vis_pt_tau_1p1n;
  TH1D* m_h_event_truth_vis_pt_tau_3p0n;

  // hadron
  TH2D* m_h_track_pt_eta_hadron_highmet;
  TH2D* m_h_track_pt_eta_hadron_lowmet;
  TH2D* m_h_track_smeared_pt_eta_hadron_highmet;
  TH2D* m_h_track_smeared_pt_eta_hadron_lowmet;
  TH2D* m_h_track_smeared_qoverpt_eta_hadron_highmet;
  TH2D* m_h_track_smeared_qoverpt_eta_hadron_lowmet;
  TH2D* m_h_track_pt_eta_caloveto_hadron_highmet;
  TH2D* m_h_track_pt_eta_caloveto_hadron_lowmet;
  TH2D* m_h_track_smeared_pt_eta_caloveto_hadron_highmet;
  TH2D* m_h_track_smeared_pt_eta_caloveto_hadron_lowmet;
  TH2D* m_h_track_smeared_qoverpt_eta_caloveto_hadron_highmet;
  TH2D* m_h_track_smeared_qoverpt_eta_caloveto_hadron_lowmet;
  TH2D* m_h_track_smeared_pt_eta_caloveto_corr_hadron_highmet;
  TH2D* m_h_track_smeared_pt_eta_caloveto_corr_hadron_lowmet;
  TH2D* m_h_track_smeared_qoverpt_eta_caloveto_corr_hadron_highmet;
  TH2D* m_h_track_smeared_qoverpt_eta_caloveto_corr_hadron_lowmet;
  TH2D* m_h_track_smeared_linear_pt_eta_caloveto_corr_hadron_highmet;
  TH2D* m_h_track_smeared_linear_pt_eta_caloveto_corr_hadron_lowmet;
  TH2D* m_h_track_smeared_linear_qoverpt_eta_caloveto_corr_hadron_highmet;
  TH2D* m_h_track_smeared_linear_qoverpt_eta_caloveto_corr_hadron_lowmet;
  TH1D* m_h_track_smeared_qoverpt_hadron_highmet;
  TH1D* m_h_track_smeared_qoverpt_hadron_lowmet;
  TH1D* m_h_track_smeared_pt_hadron_highmet;
  TH1D* m_h_track_smeared_pt_hadron_lowmet;
  TH1D* m_h_track_met_hadron;
  TH1D* m_h_track_eta_hadron_highmet;
  TH1D* m_h_track_eta_hadron_lowmet;
  TH1D* m_h_track_d0Sig_hadron_highmet;
  TH1D* m_h_track_d0Sig_hadron_lowmet;
  TH1D* m_h_track_ptcone40overpt_hadron_highmet;
  TH1D* m_h_track_ptcone40overpt_hadron_lowmet;
  TH1D* m_h_track_ettopoclus20_hadron_highmet;
  TH1D* m_h_track_ettopoclus20_hadron_lowmet;

  // tfcaloveto
  TH1D* m_h_tfcaloveto_dilepton_mass;
  TH1D* m_h_tfcaloveto_dilepton_mass_SS;
  TH1D* m_h_tfcaloveto_dilepton_mass_OS;
  TH1D* m_h_tfcaloveto_dilepton_mass_SS_et_less_5;
  TH1D* m_h_tfcaloveto_dilepton_mass_OS_et_less_5;
  TH1D* m_h_tfcaloveto_dilepton_mass_SS_et_greater_10;
  TH1D* m_h_tfcaloveto_dilepton_mass_OS_et_greater_10;
  TH1D* m_h_tfcaloveto_dilepton_mass_SS_et_less_5_mt70;
  TH1D* m_h_tfcaloveto_dilepton_mass_OS_et_less_5_mt70;
  TH1D* m_h_tfcaloveto_dilepton_mass_SS_et_greater_10_mt70;
  TH1D* m_h_tfcaloveto_dilepton_mass_OS_et_greater_10_mt70;
  TH1D* m_h_tfcaloveto_dilepton_mass_SS_et_less_5_mt60;
  TH1D* m_h_tfcaloveto_dilepton_mass_OS_et_less_5_mt60;
  TH1D* m_h_tfcaloveto_dilepton_mass_SS_et_greater_10_mt60;
  TH1D* m_h_tfcaloveto_dilepton_mass_OS_et_greater_10_mt60;
  TH1D* m_h_tfcaloveto_dilepton_mass_SS_et_less_5_mt50;
  TH1D* m_h_tfcaloveto_dilepton_mass_OS_et_less_5_mt50;
  TH1D* m_h_tfcaloveto_dilepton_mass_SS_et_greater_10_mt50;
  TH1D* m_h_tfcaloveto_dilepton_mass_OS_et_greater_10_mt50;
  TH1D* m_h_tfcaloveto_dilepton_mass_SS_et_less_5_mt40;
  TH1D* m_h_tfcaloveto_dilepton_mass_OS_et_less_5_mt40;
  TH1D* m_h_tfcaloveto_dilepton_mass_SS_et_greater_10_mt40;
  TH1D* m_h_tfcaloveto_dilepton_mass_OS_et_greater_10_mt40;
  TH2D* m_h_tfcaloveto_dilepton_mass_mt_SS_et_less_5;
  TH2D* m_h_tfcaloveto_dilepton_mass_mt_OS_et_less_5;
  TH2D* m_h_tfcaloveto_dilepton_mass_mt_SS_et_greater_10;
  TH2D* m_h_tfcaloveto_dilepton_mass_mt_OS_et_greater_10;
  TH1D* m_h_tfcaloveto_ettopoclus20_SS;
  TH1D* m_h_tfcaloveto_ettopoclus20_OS;
  TH1D* m_h_tfcaloveto_mt_SS_et_less_5;
  TH1D* m_h_tfcaloveto_mt_OS_et_less_5;
  TH1D* m_h_tfcaloveto_mt_SS_et_greater_10;
  TH1D* m_h_tfcaloveto_mt_OS_et_greater_10;
  TH1D* m_h_tfcaloveto_ettopoclus20_SS_greater_zmass;
  TH1D* m_h_tfcaloveto_ettopoclus20_OS_greater_zmass;
  TH1D* m_h_tfcaloveto_ettopoclus20_SS_less_zmass;
  TH1D* m_h_tfcaloveto_ettopoclus20_OS_less_zmass;
  TH2D* m_h_tfcaloveto_ettopoclus20_deltar_SS;
  TH2D* m_h_tfcaloveto_ettopoclus20_deltar_OS;
  TH2D* m_h_tfcaloveto_ettopoclus20_deltaphi_SS;
  TH2D* m_h_tfcaloveto_ettopoclus20_deltaphi_OS;
  TH2D* m_h_tfcaloveto_pt_eta_SS_et_less_5;
  TH2D* m_h_tfcaloveto_pt_eta_OS_et_less_5;
  TH2D* m_h_tfcaloveto_pt_eta_SS_et_greater_10;
  TH2D* m_h_tfcaloveto_pt_eta_OS_et_greater_10;
  TH1D* m_h_tfcaloveto_pt_SS_et_less_5;
  TH1D* m_h_tfcaloveto_pt_OS_et_less_5;
  TH1D* m_h_tfcaloveto_pt_SS_et_greater_10;
  TH1D* m_h_tfcaloveto_pt_OS_et_greater_10;
  TH2D* m_h_tfcaloveto_pt_mt_SS_et_less_5;
  TH2D* m_h_tfcaloveto_pt_mt_OS_et_less_5;
  TH2D* m_h_tfcaloveto_pt_mt_SS_et_greater_10;
  TH2D* m_h_tfcaloveto_pt_mt_OS_et_greater_10;
  TH1D* m_h_tfcaloveto_met_SS_et_less_5;
  TH1D* m_h_tfcaloveto_met_OS_et_less_5;
  TH1D* m_h_tfcaloveto_met_SS_et_greater_10;
  TH1D* m_h_tfcaloveto_met_OS_et_greater_10;
  TH2D* m_h_tfcaloveto_met_mt_SS_et_less_5;
  TH2D* m_h_tfcaloveto_met_mt_OS_et_less_5;
  TH2D* m_h_tfcaloveto_met_mt_SS_et_greater_10;
  TH2D* m_h_tfcaloveto_met_mt_OS_et_greater_10;
  TH1D* m_h_tfcaloveto_eta_SS_et_less_5;
  TH1D* m_h_tfcaloveto_eta_OS_et_less_5;
  TH1D* m_h_tfcaloveto_eta_SS_et_greater_10;
  TH1D* m_h_tfcaloveto_eta_OS_et_greater_10;
  TH1D* m_h_crcaloveto_ettopoclus20_stdtrack;
  TH1D* m_h_crcaloveto_ettopoclus20_fourlayer;
  TH1D* m_h_crcaloveto_ettopoclus20_threelayer;
  TH1D* m_h_crcaloveto_ettopoclus20_stdtrack_weight;
  TH1D* m_h_crcaloveto_ettopoclus20_fourlayer_weight;
  TH1D* m_h_crcaloveto_ettopoclus20_threelayer_weight;
  TH1D* m_h_crcaloveto_truthpt_stdtrack_et_all_drmatching;
  TH1D* m_h_crcaloveto_truthpt_stdtrack_et_all_nosel;
  TH1D* m_h_crcaloveto_truthpt_stdtrack_et_all_drjet;
  TH1D* m_h_crcaloveto_truthpt_stdtrack_et_all_chi2;
  TH1D* m_h_crcaloveto_truthpt_stdtrack_et_all_ptcone;
  TH1D* m_h_crcaloveto_truthpt_stdtrack_et_all_drmuon;
  TH1D* m_h_crcaloveto_truthpt_fourlayer_et_all_drmatching;
  TH1D* m_h_crcaloveto_truthpt_fourlayer_et_all_nosel;
  TH1D* m_h_crcaloveto_truthpt_fourlayer_et_all_drjet;
  TH1D* m_h_crcaloveto_truthpt_fourlayer_et_all_chi2;
  TH1D* m_h_crcaloveto_truthpt_fourlayer_et_all_ptcone;
  TH1D* m_h_crcaloveto_truthpt_fourlayer_et_all_drmuon;
  TH1D* m_h_crcaloveto_truthpt_threelayer_et_all_drmatching;
  TH1D* m_h_crcaloveto_truthpt_threelayer_et_all_nosel;
  TH1D* m_h_crcaloveto_truthpt_threelayer_et_all_drjet;
  TH1D* m_h_crcaloveto_truthpt_threelayer_et_all_chi2;
  TH1D* m_h_crcaloveto_truthpt_threelayer_et_all_ptcone;
  TH1D* m_h_crcaloveto_truthpt_threelayer_et_all_drmuon;
  TH1D* m_h_crcaloveto_truthpt_stdtrack_et_all;
  TH1D* m_h_crcaloveto_truthpt_stdtrack_et_less_5;
  TH1D* m_h_crcaloveto_truthpt_stdtrack_et_greater_10;
  TH2D* m_h_crcaloveto_truthpt_pt_stdtrack_et_all;
  TH2D* m_h_crcaloveto_truthpt_pt_stdtrack_et_less_5;
  TH2D* m_h_crcaloveto_truthpt_pt_stdtrack_et_greater_10;
  TH1D* m_h_crcaloveto_truthpt_fourlayer_et_all;
  TH1D* m_h_crcaloveto_truthpt_fourlayer_et_less_5;
  TH1D* m_h_crcaloveto_truthpt_fourlayer_et_greater_10;
  TH1D* m_h_crcaloveto_truthpt_threelayer_et_all;
  TH1D* m_h_crcaloveto_truthpt_threelayer_et_less_5;
  TH1D* m_h_crcaloveto_truthpt_threelayer_et_greater_10;
  TH1D* m_h_crcaloveto_truthpt_stdtrack_et_all_ex_jetdr;
  TH1D* m_h_crcaloveto_truthpt_stdtrack_et_less_5_ex_jetdr;
  TH1D* m_h_crcaloveto_truthpt_stdtrack_et_greater_10_ex_jetdr;
  TH2D* m_h_crcaloveto_truthpt_pt_stdtrack_et_all_ex_jetdr;
  TH2D* m_h_crcaloveto_truthpt_pt_stdtrack_et_less_5_ex_jetdr;
  TH2D* m_h_crcaloveto_truthpt_pt_stdtrack_et_greater_10_ex_jetdr;
  TH1D* m_h_crcaloveto_truthpt_fourlayer_et_all_ex_jetdr;
  TH1D* m_h_crcaloveto_truthpt_fourlayer_et_less_5_ex_jetdr;
  TH1D* m_h_crcaloveto_truthpt_fourlayer_et_greater_10_ex_jetdr;
  TH1D* m_h_crcaloveto_truthpt_threelayer_et_all_ex_jetdr;
  TH1D* m_h_crcaloveto_truthpt_threelayer_et_less_5_ex_jetdr;
  TH1D* m_h_crcaloveto_truthpt_threelayer_et_greater_10_ex_jetdr;
  TH2D* m_h_crcaloveto_truthpt_trutheta_stdtrack_et_all_ex_jetdr;
  TH2D* m_h_crcaloveto_truthpt_trutheta_stdtrack_et_less_5_ex_jetdr;
  TH2D* m_h_crcaloveto_truthpt_trutheta_stdtrack_et_greater_10_ex_jetdr;
  TH2D* m_h_crcaloveto_truthpt_trutheta_fourlayer_et_all_ex_jetdr;
  TH2D* m_h_crcaloveto_truthpt_trutheta_fourlayer_et_less_5_ex_jetdr;
  TH2D* m_h_crcaloveto_truthpt_trutheta_fourlayer_et_greater_10_ex_jetdr;
  TH2D* m_h_crcaloveto_truthpt_trutheta_threelayer_et_all_ex_jetdr;
  TH2D* m_h_crcaloveto_truthpt_trutheta_threelayer_et_less_5_ex_jetdr;
  TH2D* m_h_crcaloveto_truthpt_trutheta_threelayer_et_greater_10_ex_jetdr;
  TH1D* m_h_crcaloveto_truthpt_stdtrack_et_all_weight;
  TH1D* m_h_crcaloveto_truthpt_stdtrack_et_less_5_weight;
  TH1D* m_h_crcaloveto_truthpt_stdtrack_et_greater_10_weight;
  TH2D* m_h_crcaloveto_truthpt_pt_stdtrack_et_all_weight;
  TH2D* m_h_crcaloveto_truthpt_pt_stdtrack_et_less_5_weight;
  TH2D* m_h_crcaloveto_truthpt_pt_stdtrack_et_greater_10_weight;
  TH1D* m_h_crcaloveto_truthpt_fourlayer_et_all_weight;
  TH1D* m_h_crcaloveto_truthpt_fourlayer_et_less_5_weight;
  TH1D* m_h_crcaloveto_truthpt_fourlayer_et_greater_10_weight;
  TH1D* m_h_crcaloveto_truthpt_threelayer_et_all_weight;
  TH1D* m_h_crcaloveto_truthpt_threelayer_et_less_5_weight;
  TH1D* m_h_crcaloveto_truthpt_threelayer_et_greater_10_weight;

  // single lepton
  TH1D* m_h_track_pt_single_e_highmet;
  TH1D* m_h_track_pt_oldbin_single_e_highmet;
  TH1D* m_h_track_pt_single_e_leading_highmet;
  TH1D* m_h_track_pt_oldbin_single_e_leading_highmet;
  TH1D* m_h_track_pt_single_e_leading_wo_trackiso_highmet;
  TH1D* m_h_track_pt_oldbin_single_e_leading_wo_trackiso_highmet;
  TH1D* m_h_track_pt_single_e_leading_new_highmet;
  TH1D* m_h_track_pt_oldbin_single_e_leading_new_highmet;
  TH1D* m_h_track_pt_single_e_leading_wo_trackiso_new_highmet;
  TH1D* m_h_track_pt_oldbin_single_e_leading_wo_trackiso_new_highmet;
  TH1D* m_h_electron_pt_single_e_highmet;
  TH1D* m_h_electron_pt_oldbin_single_e_highmet;
  TH1D* m_h_electron_pt_single_e_leading_highmet;
  TH1D* m_h_electron_pt_oldbin_single_e_leading_highmet;

  // tf pix only
  TH1D* m_h_zmass_track_ss_tfpix;
  TH1D* m_h_zmass_track_os_tfpix;
  TH1D* m_h_zmass_track_ss_wo_trackiso_tfpix;
  TH1D* m_h_zmass_track_os_wo_trackiso_tfpix;
  TH1D* m_h_zmass_4ltracklet_ss_tfpix;
  TH1D* m_h_zmass_4ltracklet_os_tfpix;
  TH1D* m_h_zmass_3ltracklet_ss_tfpix;
  TH1D* m_h_zmass_3ltracklet_os_tfpix;
  TH1D* m_h_probe_track_pt_ss_tfpix;
  TH1D* m_h_probe_track_pt_os_tfpix;
  TH1D* m_h_probe_track_pt_ss_wo_trackiso_tfpix;
  TH1D* m_h_probe_track_pt_os_wo_trackiso_tfpix;
  TH1D* m_h_probe_4ltracklet_pt_ss_tfpix;
  TH1D* m_h_probe_4ltracklet_pt_os_tfpix;
  TH1D* m_h_probe_3ltracklet_pt_ss_tfpix;
  TH1D* m_h_probe_3ltracklet_pt_os_tfpix;
  TH2D* m_h_probe_track_pt_eta_ss_tfpix;
  TH2D* m_h_probe_track_pt_eta_os_tfpix;
  TH2D* m_h_probe_track_pt_eta_ss_wo_trackiso_tfpix;
  TH2D* m_h_probe_track_pt_eta_os_wo_trackiso_tfpix;
  TH2D* m_h_probe_4ltracklet_pt_eta_ss_tfpix;
  TH2D* m_h_probe_4ltracklet_pt_eta_os_tfpix;
  TH2D* m_h_probe_3ltracklet_pt_eta_ss_tfpix;
  TH2D* m_h_probe_3ltracklet_pt_eta_os_tfpix;

  // retracking
  TH1D* m_h_retracking_matched_track[2];
  TH1D* m_h_retracking_deltar_lepton_tracklet[2][2];
  TH1D* m_h_retracking_deltar_lepton_tracklet_chargeflip[2][2];
  TH1D* m_h_retracking_deltar_lepton_stdtrack[2];
  TH1D* m_h_retracking_deltar_lepton_stdtrack_chargeflip[2];
  TH1D* m_h_retracking_stdtrack_pt[2][2];
  TH1D* m_h_retracking_tracklet_pt[2][2];
  TH1D* m_h_retracking_tracklet_vcpt[2][2];
  TH1D* m_h_retracking_smeared_stdtrack_pt[2][2];
  TH1D* m_h_retracking_smeared_stdtrack_pt_1stwave[2][2];
  TH1D* m_h_retracking_smeared_stdtrack_vcpt[2][2];
  TH1D* m_h_retracking_smeared_stdtrack_vcpt_sigma1up[2][2];
  TH1D* m_h_retracking_smeared_stdtrack_vcpt_sigma1down[2][2];
  TH1D* m_h_retracking_smeared_stdtrack_vcpt_alpha1up[2][2];
  TH1D* m_h_retracking_smeared_stdtrack_vcpt_alpha1down[2][2];
  TH1D* m_h_retracking_smeared_stdtrack_vcpt_beta1up[2][2];
  TH1D* m_h_retracking_smeared_stdtrack_vcpt_beta1down[2][2];
  TH1D* m_h_retracking_smeared_stdtrack_vcpt_gamma1up[2][2];
  TH1D* m_h_retracking_smeared_stdtrack_vcpt_gamma1down[2][2];
  TH1D* m_h_retracking_smeared_stdtrack_vcpt_mean[2][2];
  TH1D* m_h_retracking_smeared_stdtrack_vcpt_linear[2][2];
  TH1D* m_h_retracking_smeared_stdtrack_qoverpt[2][2];
  TH1D* m_h_retracking_smeared_stdtrack_qoverpt_mean[2][2];
  TH1D* m_h_retracking_smeared_stdtrack_qoverpt_1stwave[2][2];
  TH1D* m_h_retracking_smeared_stdtrack_qovervcpt[2][2];
  TH1D* m_h_retracking_smeared_stdtrack_qovervcpt_sigma1up[2][2];
  TH1D* m_h_retracking_smeared_stdtrack_qovervcpt_sigma1down[2][2];
  TH1D* m_h_retracking_smeared_stdtrack_qovervcpt_alpha1up[2][2];
  TH1D* m_h_retracking_smeared_stdtrack_qovervcpt_alpha1down[2][2];
  TH1D* m_h_retracking_smeared_stdtrack_qovervcpt_beta1up[2][2];
  TH1D* m_h_retracking_smeared_stdtrack_qovervcpt_beta1down[2][2];
  TH1D* m_h_retracking_smeared_stdtrack_qovervcpt_gamma1up[2][2];
  TH1D* m_h_retracking_smeared_stdtrack_qovervcpt_gamma1down[2][2];
  TH1D* m_h_retracking_smeared_stdtrack_qovervcpt_linear[2][2];
  TH1D* m_h_retracking_smeared_stdtrack_qovervcpt_mean[2][2];
  TH2D* m_h_retracking_deltaqoverpt_d0[2][2];
  TH2D* m_h_retracking_deltaqovervcpt_stdtrackd0[2][2];
  TH2D* m_h_retracking_deltaqovervcpt_trackletd0[2][2];
  TH2D* m_h_retracking_deltaqoverpt_stdpt[2][2];
  TH1D* m_h_retracking_deltaqoverpt[2][2][3][3];
  TH1D* m_h_retracking_deltaqoverpt_pt25to35[2][2][3][3];
  TH1D* m_h_retracking_deltaqoverpt_pt35to45[2][2][3][3];
  TH1D* m_h_retracking_deltaqoverpt_pt45to60[2][2][3][3];
  TH1D* m_h_retracking_deltaqoverpt_pt60to100[2][2][3][3];
  TH1D* m_h_retracking_deltaqoverpt_pt100to[2][2][3][3];
  TH1D* m_h_retracking_deltaqovervcpt[2][2][3][3];
  TH1D* m_h_retracking_deltaqovervcpt_pt25to35[2][2][3][3];
  TH1D* m_h_retracking_deltaqovervcpt_pt35to45[2][2][3][3];
  TH1D* m_h_retracking_deltaqovervcpt_pt45to60[2][2][3][3];
  TH1D* m_h_retracking_deltaqovervcpt_pt60to100[2][2][3][3];
  TH1D* m_h_retracking_deltaqovervcpt_pt100to[2][2][3][3];
  TH1D* m_h_retracking_deltaqovervcpt_abs[2][2][3][3];
  TH1D* m_h_retracking_deltaqovervcpt_abs_pt25to35[2][2][3][3];
  TH1D* m_h_retracking_deltaqovervcpt_abs_pt35to45[2][2][3][3];
  TH1D* m_h_retracking_deltaqovervcpt_abs_pt45to60[2][2][3][3];
  TH1D* m_h_retracking_deltaqovervcpt_abs_pt60to100[2][2][3][3];
  TH1D* m_h_retracking_deltaqovervcpt_abs_pt100to[2][2][3][3];
  TH1D* m_h_retracking_deltaqovervcpt_pt25to35_mu10to20[2][2][3][3];
  TH1D* m_h_retracking_deltaqovervcpt_pt25to35_mu20to30[2][2][3][3];
  TH1D* m_h_retracking_deltaqovervcpt_pt25to35_mu30to40[2][2][3][3];
  TH1D* m_h_retracking_deltaqovervcpt_pt25to35_mu40to50[2][2][3][3];
  TH1D* m_h_retracking_deltaqovervcpt_pt25to35_mu50to60[2][2][3][3];
  TH1D* m_h_retracking_deltaqovervcpt_abs_mu10to20[2][2][3][3];
  TH1D* m_h_retracking_deltaqovervcpt_abs_mu20to30[2][2][3][3];
  TH1D* m_h_retracking_deltaqovervcpt_abs_mu30to40[2][2][3][3];
  TH1D* m_h_retracking_deltaqovervcpt_abs_mu40to50[2][2][3][3];
  TH1D* m_h_retracking_deltaqovervcpt_abs_mu50to60[2][2][3][3];
  TH1D* m_h_retracking_deltaqoverpt_d0_0to0p02[2][2];
  TH1D* m_h_retracking_deltaqoverpt_d0_0p02to0p05[2][2];
  TH1D* m_h_retracking_deltaqoverpt_d0_0p05to0p1[2][2];
  TH1D* m_h_retracking_deltaqovervcpt_d0_0to0p02[2][2];
  TH1D* m_h_retracking_deltaqovervcpt_d0_0p02to0p05[2][2];
  TH1D* m_h_retracking_deltaqovervcpt_d0_0p05to0p1[2][2];
  TH2D* m_h_retracking_stdpt_vcpt[2][2];
  TH2D* m_h_retracking_stdqoverpt_qovervcpt[2][2];
  TH2D* m_h_retracking_stdqoverpt_qoverpt[2][2];
  TH2D* m_h_retracking_stdqoverpt_qoverpt_strange[2][2];
  TH2D* m_h_retracking_stdqoverpt_deltaqovervcpt[2][2];
  TH2D* m_h_retracking_stdqoverpt_deltaqoverpt[2][2];
  TH2D* m_h_retracking_stdqoverpt_qovervcpt_sub[2][2];
  TH2D* m_h_retracking_stdqoverpt_qoverpt_sub[2][2];
  TH1D* m_h_retracking_stdtrack_chi2Prob[2][2];
  TH1D* m_h_retracking_stdtrack_dRMSTrack[2][2];
  TH1D* m_h_retracking_stdtrack_SCTHits[2][2];
  TH1D* m_h_retracking_stdtrack_chi2Prob_strange[2][2];
  TH1D* m_h_retracking_stdtrack_dRMSTrack_strange[2][2];
  TH1D* m_h_retracking_stdtrack_SCTHits_strange[2][2];
  TH1D* m_h_retracking_tracklet_chi2Prob[2][2];
  TH1D* m_h_retracking_tracklet_dRMSTrack[2][2];
  TH1D* m_h_retracking_tracklet_chi2Prob_strange[2][2];
  TH1D* m_h_retracking_tracklet_dRMSTrack_strange[2][2];

  unsigned count_pt;
  unsigned count_loose_4l;
  unsigned count_loose_3l;
  unsigned count_sr_4l;
  unsigned count_sr_3l;

  Float_t CovMat00;
  Float_t CovMat10;
  Float_t CovMat11;
  Float_t CovMat20;
  Float_t CovMat21;
  Float_t CovMat22;
  Float_t CovMat30;
  Float_t CovMat31;
  Float_t CovMat32;
  Float_t CovMat33;
  Float_t CovMat40;
  Float_t CovMat41;
  Float_t CovMat42;
  Float_t CovMat43;
  Float_t CovMat44;
  Float_t CorMat00;
  Float_t CorMat10;
  Float_t CorMat11;
  Float_t CorMat20;
  Float_t CorMat21;
  Float_t CorMat22;
  Float_t CorMat30;
  Float_t CorMat31;
  Float_t CorMat32;
  Float_t CorMat33;
  Float_t CorMat40;
  Float_t CorMat41;
  Float_t CorMat42;
  Float_t CorMat43;
  Float_t CorMat44;
  Float_t pt;
  Float_t eta;
  Float_t phi;
  Float_t e;
  Float_t charge;
  Float_t KVUPt;
  Float_t KVUEta;
  Float_t KVUPhi;
  Float_t KVUCharge;
  Float_t KVUChi2Prob;
  Float_t deltaqoverpt;
  Float_t deltaeta;
  Float_t deltaphi;
  Float_t d0;
  Float_t d0Sig;
  Float_t z0SinThetaPV;
  Float_t eTTopoClus20;
  Float_t eTTopoClus30;
  Float_t eTTopoClus40;
  Float_t ptCone20OverPt;
  Float_t ptCone30OverPt;
  Float_t ptCone40OverPt;
  Float_t dRJet20;
  Float_t dRJet50;
  Float_t dRMSTrack;
  Float_t chi2Prob;
  Float_t mc_weight;
  Float_t rand_weight;
  Int_t   n_layer;

  // Declaration of leaf types
  Int_t           is_data;
  Int_t           runNumber;
  Int_t           randomRunNumber;
  Int_t           lumiBlock;
  Long64_t        eventNumber;
  Float_t         mc_eventweight;
  Float_t         prw_weight;
  Float_t         average_interaction_per_crossing;
  Float_t         actual_interaction_per_crossing;
  Float_t         corrected_average_interaction_per_crossing;
  Int_t           pass_electron;
  Int_t           pass_muon;
  Int_t           pass_missingET;
  Int_t           has_pv;
  Int_t           pass_event_cleaning;
  Int_t           is_bad_muon_missingET;
  Int_t           has_bad_jet;
  Int_t           nMuons;
  Int_t           nElectrons;
  Int_t           nTaus;
  Int_t           nJets;
  Int_t           nCaloClusters;
  Int_t           nTruthJets;
  Int_t           nMSTracks;
  Int_t           nTracks;
  Int_t           nTruths;
  std::vector<float>   *muon_phi;
  std::vector<float>   *muon_eta;
  std::vector<float>   *muon_pt;
  std::vector<float>   *muon_e;
  std::vector<int>     *muon_charge;
  std::vector<float>   *muon_ptVarCone20OverPt;
  std::vector<float>   *muon_ptVarCone30OverPt;
  std::vector<float>   *muon_ptVarCone40OverPt;
  std::vector<float>   *muon_eTTopoCone20;
  std::vector<float>   *muon_eTTopoCone30;
  std::vector<float>   *muon_eTTopoCone40;
  std::vector<int>     *muon_quality;
  std::vector<int>     *muon_isSignal;
  std::vector<int>     *muon_isTrigMatch;
  std::vector<int>     *muon_key;
  std::vector<float>   *electron_phi;
  std::vector<float>   *electron_eta;
  std::vector<float>   *electron_pt;
  std::vector<float>   *electron_e;
  std::vector<int>     *electron_charge;
  std::vector<float>   *electron_ptVarCone20OverPt;
  std::vector<float>   *electron_eTTopoCone20;
  std::vector<int>     *electron_quality;
  std::vector<int>     *electron_isSignal;
  std::vector<int>     *electron_isTrigMatch;
  std::vector<int>     *electron_key;
  std::vector<float>   *tau_phi;
  std::vector<float>   *tau_eta;
  std::vector<float>   *tau_pt;
  std::vector<float>   *tau_e;
  std::vector<int>     *tau_charge;
  std::vector<int>     *tau_nprongs;
  std::vector<float>   *tau_BDTJetScore;
  std::vector<int>     *tau_isMedium;
  std::vector<std::vector<float>> *tau_track_phi;
  std::vector<std::vector<float>> *tau_track_eta;
  std::vector<std::vector<float>> *tau_track_pt;
  std::vector<std::vector<float>> *tau_track_e;
  std::vector<float>   *jet_phi;
  std::vector<float>   *jet_eta;
  std::vector<float>   *jet_pt;
  std::vector<float>   *jet_e;
  std::vector<float>   *jet_isBJet;
  std::vector<float>   *msTrack_phi;
  std::vector<float>   *msTrack_eta;
  std::vector<float>   *msTrack_pt;
  std::vector<float>   *msTrack_e;
  std::vector<int>     *msTrack_charge;
  std::vector<float>   *caloCluster_phi;
  std::vector<float>   *caloCluster_eta;
  std::vector<float>   *caloCluster_pt;
  std::vector<float>   *caloCluster_e;
  std::vector<float>   *caloCluster_clusterSize;
  Float_t         missingET_TST_pt;
  Float_t         missingET_TST_phi;
  Float_t         TST_pt;
  Float_t         TST_phi;
  Float_t         missingET_CST_pt;
  Float_t         missingET_CST_phi;
  Float_t         CST_pt;
  Float_t         CST_phi;
  Float_t         missingET_TST_leleVeto_pt;
  Float_t         missingET_TST_leleVeto_phi;
  Float_t         missingET_TST_lmuVeto_pt;
  Float_t         missingET_TST_lmuVeto_phi;
  std::vector<float>   *truthjet_phi;
  std::vector<float>   *truthjet_eta;
  std::vector<float>   *truthjet_pt;
  std::vector<float>   *truthjet_e;
  std::vector<float>   *truth_phi;
  std::vector<float>   *truth_eta;
  std::vector<float>   *truth_pt;
  std::vector<float>   *truth_e;
  std::vector<float>   *truth_vis_phi;
  std::vector<float>   *truth_vis_eta;
  std::vector<float>   *truth_vis_pt;
  std::vector<float>   *truth_vis_e;
  std::vector<int>     *truth_charge;
  std::vector<int>     *truth_pdgid;
  std::vector<int>     *truth_barcode;
  std::vector<int>     *truth_nChildren;
  std::vector<int>     *truth_nTauProngs;
  std::vector<int>     *truth_nPi0;
  std::vector<int>     *truth_isTauHad;
  std::vector<int>     *truth_parent_barcode;
  std::vector<float>   *truth_properTime;
  std::vector<float>   *truth_decayr;
  std::vector<int>     *track_isTracklet;
  std::vector<float>   *track_phi;
  std::vector<float>   *track_eta;
  std::vector<float>   *track_pt;
  std::vector<float>   *track_e;
  std::vector<int>     *track_charge;
  std::vector<float>   *track_KVUPhi;
  std::vector<float>   *track_KVUEta;
  std::vector<float>   *track_KVUPt;
  std::vector<int>     *track_KVUCharge;
  std::vector<float>   *track_KVUChi2Prob;
  std::vector<int>     *track_pixelBarrel_0;
  std::vector<int>     *track_pixelBarrel_1;
  std::vector<int>     *track_pixelBarrel_2;
  std::vector<int>     *track_pixelBarrel_3;
  std::vector<int>     *track_sctBarrel_0;
  std::vector<int>     *track_sctBarrel_1;
  std::vector<int>     *track_sctBarrel_2;
  std::vector<int>     *track_sctBarrel_3;
  std::vector<int>     *track_sctEndcap_0;
  std::vector<int>     *track_sctEndcap_1;
  std::vector<int>     *track_sctEndcap_2;
  std::vector<int>     *track_sctEndcap_3;
  std::vector<int>     *track_sctEndcap_4;
  std::vector<int>     *track_sctEndcap_5;
  std::vector<int>     *track_sctEndcap_6;
  std::vector<int>     *track_sctEndcap_7;
  std::vector<int>     *track_sctEndcap_8;
  std::vector<int>     *track_nPixelEndcap;
  std::vector<int>     *track_nPixHits;
  std::vector<int>     *track_nPixHoles;
  std::vector<int>     *track_nPixSharedHits;
  std::vector<int>     *track_nPixOutliers;
  std::vector<int>     *track_nPixDeadSensors;
  std::vector<int>     *track_nContribPixelLayers;
  std::vector<int>     *track_nGangedFlaggedFakes;
  std::vector<int>     *track_nPixelSpoiltHits;
  std::vector<int>     *track_nSCTHits;
  std::vector<int>     *track_nSCTHoles;
  std::vector<int>     *track_nSCTSharedHits;
  std::vector<int>     *track_nSCTOutliers;
  std::vector<int>     *track_nSCTDeadSensors;
  std::vector<int>     *track_nTRTHits;
  std::vector<int>     *track_nTRTHoles;
  std::vector<int>     *track_nTRTSharedHits;
  std::vector<int>     *track_nTRTDeadSensors;
  std::vector<int>     *track_nTRTOutliers;
  std::vector<float>   *track_dEdx;
  std::vector<float>   *track_ptCone20OverPt;
  std::vector<float>   *track_ptCone30OverPt;
  std::vector<float>   *track_ptCone40OverPt;
  std::vector<float>   *track_ptCone20OverKVUPt;
  std::vector<float>   *track_ptCone30OverKVUPt;
  std::vector<float>   *track_ptCone40OverKVUPt;
  std::vector<float>   *track_eTTopoClus20;
  std::vector<float>   *track_eTTopoClus30;
  std::vector<float>   *track_eTTopoClus40;
  std::vector<float>   *track_eTTopoCone20;
  std::vector<float>   *track_eTTopoCone30;
  std::vector<float>   *track_eTTopoCone40;
  std::vector<float>   *track_dRJet20;
  std::vector<float>   *track_dRJet50;
  std::vector<float>   *track_dRMSTrack;
  std::vector<float>   *track_dRMuon;
  std::vector<float>   *track_dRElectron;
  std::vector<float>   *track_d0;
  std::vector<float>   *track_d0Sig;
  std::vector<float>   *track_z0;
  std::vector<float>   *track_z0PV;
  std::vector<float>   *track_z0PVSinTheta;
  std::vector<float>   *track_chi2Prob;
  std::vector<std::vector<float>>   *track_parameterCovMatrix;
  std::vector<int>     *track_electron_key;
  std::vector<int>     *track_muon_key;
  std::vector<float>   *track_truth_phi;
  std::vector<float>   *track_truth_eta;
  std::vector<float>   *track_truth_pt;
  std::vector<float>   *track_truth_e;
  std::vector<int>     *track_truth_charge;
  std::vector<int>     *track_truth_pdgid;
  std::vector<int>     *track_truth_barcode;
  std::vector<int>     *track_truth_nChildren;
  std::vector<float>   *track_truth_properTime;
  std::vector<float>   *track_truth_decayr;
  std::vector<float>   *track_truth_matchingProb;
  std::vector<float>   *track_truth_parent_phi;
  std::vector<float>   *track_truth_parent_eta;
  std::vector<float>   *track_truth_parent_pt;
  std::vector<float>   *track_truth_parent_e;
  std::vector<int>     *track_truth_parent_charge;
  std::vector<int>     *track_truth_parent_pdgid;
  std::vector<int>     *track_truth_parent_barcode;
  std::vector<int>     *track_truth_parent_nChildren;
  std::vector<float>   *track_truth_parent_decayr;

  // List of branches
  TBranch        *b_is_data;   //!
  TBranch        *b_runNumber;   //!
  TBranch        *b_randomRunNumber;   //!
  TBranch        *b_lumiBlock;   //!
  TBranch        *b_eventNumber;   //!
  TBranch        *b_mc_eventweight;   //!
  TBranch        *b_prw_weight;   //!
  TBranch        *b_average_interaction_per_crossing;   //!
  TBranch        *b_actual_interaction_per_crossing;   //!
  TBranch        *b_corrected_average_interaction_per_crossing;   //!
  TBranch        *b_pass_electron;   //!
  TBranch        *b_pass_muon;   //!
  TBranch        *b_pass_missingET;   //!
  TBranch        *b_has_pv;   //!
  TBranch        *b_pass_event_cleaning;   //!
  TBranch        *b_is_bad_muon_missingET;   //!
  TBranch        *b_has_bad_jet;   //!
  TBranch        *b_nMuons;   //!
  TBranch        *b_nElectrons;   //!
  TBranch        *b_nTaus;   //!
  TBranch        *b_nJets;   //!
  TBranch        *b_nCaloClusters;   //!
  TBranch        *b_nTruthJets;   //!
  TBranch        *b_nMSTracks;   //!
  TBranch        *b_nTracks;   //!
  TBranch        *b_nTruths;   //!
  TBranch        *b_muon_phi;   //!
  TBranch        *b_muon_eta;   //!
  TBranch        *b_muon_pt;   //!
  TBranch        *b_muon_e;   //!
  TBranch        *b_muon_charge;   //!
  TBranch        *b_muon_ptVarCone20OverPt;   //!
  TBranch        *b_muon_ptVarCone30OverPt;   //!
  TBranch        *b_muon_ptVarCone40OverPt;   //!
  TBranch        *b_muon_eTTopoCone20;   //!
  TBranch        *b_muon_eTTopoCone30;   //!
  TBranch        *b_muon_eTTopoCone40;   //!
  TBranch        *b_muon_quality;   //!
  TBranch        *b_muon_isSignal;   //!
  TBranch        *b_muon_isTrigMatch;   //!
  TBranch        *b_muon_key;   //!
  TBranch        *b_electron_phi;   //!
  TBranch        *b_electron_eta;   //!
  TBranch        *b_electron_pt;   //!
  TBranch        *b_electron_e;   //!
  TBranch        *b_electron_charge;   //!
  TBranch        *b_electron_ptVarCone20OverPt;   //!
  TBranch        *b_electron_eTTopoCone20;   //!
  TBranch        *b_electron_quality;   //!
  TBranch        *b_electron_isSignal;   //!
  TBranch        *b_electron_isTrigMatch;   //!
  TBranch        *b_electron_key;   //!
  TBranch        *b_tau_phi;   //!
  TBranch        *b_tau_eta;   //!
  TBranch        *b_tau_pt;   //!
  TBranch        *b_tau_e;   //!
  TBranch        *b_tau_charge;   //!
  TBranch        *b_tau_nprongs;   //!
  TBranch        *b_tau_BDTJetScore;   //!
  TBranch        *b_tau_isMedium;   //!
  TBranch        *b_tau_track_phi;   //!
  TBranch        *b_tau_track_eta;   //!
  TBranch        *b_tau_track_pt;   //!
  TBranch        *b_tau_track_e;   //!
  TBranch        *b_jet_phi;   //!
  TBranch        *b_jet_eta;   //!
  TBranch        *b_jet_pt;   //!
  TBranch        *b_jet_e;   //!
  TBranch        *b_jet_isBJet;   //!
  TBranch        *b_msTrack_phi;   //!
  TBranch        *b_msTrack_eta;   //!
  TBranch        *b_msTrack_pt;   //!
  TBranch        *b_msTrack_e;   //!
  TBranch        *b_msTrack_charge;   //!
  TBranch        *b_caloCluster_phi;   //!
  TBranch        *b_caloCluster_eta;   //!
  TBranch        *b_caloCluster_pt;   //!
  TBranch        *b_caloCluster_e;   //!
  TBranch        *b_caloCluster_clusterSize;   //!
  TBranch        *b_missingET_TST_pt;   //!
  TBranch        *b_missingET_TST_phi;   //!
  TBranch        *b_TST_pt;   //!
  TBranch        *b_TST_phi;   //!
  TBranch        *b_missingET_CST_pt;   //!
  TBranch        *b_missingET_CST_phi;   //!
  TBranch        *b_CST_pt;   //!
  TBranch        *b_CST_phi;   //!
  TBranch        *b_missingET_TST_leleVeto_pt;   //!
  TBranch        *b_missingET_TST_leleVeto_phi;   //!
  TBranch        *b_missingET_TST_lmuVeto_pt;   //!
  TBranch        *b_missingET_TST_lmuVeto_phi;   //!
  TBranch        *b_truthjet_phi;   //!
  TBranch        *b_truthjet_eta;   //!
  TBranch        *b_truthjet_pt;   //!
  TBranch        *b_truthjet_e;   //!
  TBranch        *b_truth_phi;   //!
  TBranch        *b_truth_eta;   //!
  TBranch        *b_truth_pt;   //!
  TBranch        *b_truth_e;   //!
  TBranch        *b_truth_vis_phi;   //!
  TBranch        *b_truth_vis_eta;   //!
  TBranch        *b_truth_vis_pt;   //!
  TBranch        *b_truth_vis_e;   //!
  TBranch        *b_truth_charge;   //!
  TBranch        *b_truth_pdgid;   //!
  TBranch        *b_truth_barcode;   //!
  TBranch        *b_truth_nChildren;   //!
  TBranch        *b_truth_nTauProngs;   //!
  TBranch        *b_truth_nPi0;   //!
  TBranch        *b_truth_isTauHad;   //!
  TBranch        *b_truth_parent_barcode;   //!
  TBranch        *b_truth_properTime;   //!
  TBranch        *b_truth_decayr;   //!
  TBranch        *b_track_isTracklet;   //!
  TBranch        *b_track_phi;   //!
  TBranch        *b_track_eta;   //!
  TBranch        *b_track_pt;   //!
  TBranch        *b_track_e;   //!
  TBranch        *b_track_charge;   //!
  TBranch        *b_track_KVUPhi;   //!
  TBranch        *b_track_KVUEta;   //!
  TBranch        *b_track_KVUPt;   //!
  TBranch        *b_track_KVUCharge;   //!
  TBranch        *b_track_KVUChi2Prob;   //!
  TBranch        *b_track_pixelBarrel_0;   //!
  TBranch        *b_track_pixelBarrel_1;   //!
  TBranch        *b_track_pixelBarrel_2;   //!
  TBranch        *b_track_pixelBarrel_3;   //!
  TBranch        *b_track_sctBarrel_0;   //!
  TBranch        *b_track_sctBarrel_1;   //!
  TBranch        *b_track_sctBarrel_2;   //!
  TBranch        *b_track_sctBarrel_3;   //!
  TBranch        *b_track_sctEndcap_0;   //!
  TBranch        *b_track_sctEndcap_1;   //!
  TBranch        *b_track_sctEndcap_2;   //!
  TBranch        *b_track_sctEndcap_3;   //!
  TBranch        *b_track_sctEndcap_4;   //!
  TBranch        *b_track_sctEndcap_5;   //!
  TBranch        *b_track_sctEndcap_6;   //!
  TBranch        *b_track_sctEndcap_7;   //!
  TBranch        *b_track_sctEndcap_8;   //!
  TBranch        *b_track_nPixelEndcap;   //!
  TBranch        *b_track_nPixHits;   //!
  TBranch        *b_track_nPixHoles;   //!
  TBranch        *b_track_nPixSharedHits;   //!
  TBranch        *b_track_nPixOutliers;   //!
  TBranch        *b_track_nPixDeadSensors;   //!
  TBranch        *b_track_nContribPixelLayers;   //!
  TBranch        *b_track_nGangedFlaggedFakes;   //!
  TBranch        *b_track_nPixelSpoiltHits;   //!
  TBranch        *b_track_nSCTHits;   //!
  TBranch        *b_track_nSCTHoles;   //!
  TBranch        *b_track_nSCTSharedHits;   //!
  TBranch        *b_track_nSCTOutliers;   //!
  TBranch        *b_track_nSCTDeadSensors;   //!
  TBranch        *b_track_nTRTHits;   //!
  TBranch        *b_track_nTRTHoles;   //!
  TBranch        *b_track_nTRTSharedHits;   //!
  TBranch        *b_track_nTRTDeadSensors;   //!
  TBranch        *b_track_nTRTOutliers;   //!
  TBranch        *b_track_dEdx;   //!
  TBranch        *b_track_ptCone20OverPt;   //!
  TBranch        *b_track_ptCone30OverPt;   //!
  TBranch        *b_track_ptCone40OverPt;   //!
  TBranch        *b_track_ptCone20OverKVUPt;   //!
  TBranch        *b_track_ptCone30OverKVUPt;   //!
  TBranch        *b_track_ptCone40OverKVUPt;   //!
  TBranch        *b_track_eTTopoClus20;   //!
  TBranch        *b_track_eTTopoClus30;   //!
  TBranch        *b_track_eTTopoClus40;   //!
  TBranch        *b_track_eTTopoCone20;   //!
  TBranch        *b_track_eTTopoCone30;   //!
  TBranch        *b_track_eTTopoCone40;   //!
  TBranch        *b_track_dRJet20;   //!
  TBranch        *b_track_dRJet50;   //!
  TBranch        *b_track_dRMSTrack;   //!
  TBranch        *b_track_dRMuon;   //!
  TBranch        *b_track_dRElectron;   //!
  TBranch        *b_track_d0;   //!
  TBranch        *b_track_d0Sig;   //!
  TBranch        *b_track_z0;   //!
  TBranch        *b_track_z0PV;   //!
  TBranch        *b_track_z0PVSinTheta;   //!
  TBranch        *b_track_chi2Prob;   //!
  TBranch        *b_track_parameterCovMatrix;   //!
  TBranch        *b_track_electron_key;   //!
  TBranch        *b_track_muon_key;   //!
  TBranch        *b_track_truth_phi;   //!
  TBranch        *b_track_truth_eta;   //!
  TBranch        *b_track_truth_pt;   //!
  TBranch        *b_track_truth_e;   //!
  TBranch        *b_track_truth_charge;   //!
  TBranch        *b_track_truth_pdgid;   //!
  TBranch        *b_track_truth_barcode;   //!
  TBranch        *b_track_truth_nChildren;   //!
  TBranch        *b_track_truth_properTime;   //!
  TBranch        *b_track_truth_decayr;   //!
  TBranch        *b_track_truth_matchingProb;   //!
  TBranch        *b_track_truth_parent_phi;   //!
  TBranch        *b_track_truth_parent_eta;   //!
  TBranch        *b_track_truth_parent_pt;   //!
  TBranch        *b_track_truth_parent_e;   //!
  TBranch        *b_track_truth_parent_charge;   //!
  TBranch        *b_track_truth_parent_pdgid;   //!
  TBranch        *b_track_truth_parent_barcode;   //!
  TBranch        *b_track_truth_parent_nChildren;   //!
  TBranch        *b_track_truth_parent_decayr;   //!

  struct hist {
    bool   is2d;
    std::vector<std::vector<std::vector<TH1D*>>>  hist1d;
    std::vector<std::vector<std::vector<TH2D*>>>  hist2d;
    std::string name;
    std::string label;
    int    binx;
    double minx;
    double maxx;
    int    biny;
    double miny;
    double maxy;
    bool   isTreatBin;
    bool   islogx;
    bool   islogy;
    std::vector<double> binx_vec;
    std::vector<double> biny_vec;
  };

  struct track {
    int     isTracklet;
    float   phi;
    float   eta;
    float   pt;
    float   e;
    int     charge;
    float   KVUPhi;
    float   KVUEta;
    float   KVUPt;
    int     KVUCharge;
    float   KVUChi2Prob;
    int     pixelBarrel_0;
    int     pixelBarrel_1;
    int     pixelBarrel_2;
    int     pixelBarrel_3;
    int     sctBarrel_0;
    int     sctBarrel_1;
    int     sctBarrel_2;
    int     sctBarrel_3;
    int     sctEndcap_0;
    int     sctEndcap_1;
    int     sctEndcap_2;
    int     sctEndcap_3;
    int     sctEndcap_4;
    int     sctEndcap_5;
    int     sctEndcap_6;
    int     sctEndcap_7;
    int     sctEndcap_8;
    int     nPixelEndcap;
    int     nPixHits;
    int     nPixHoles;
    int     nPixSharedHits;
    int     nPixOutliers;
    int     nPixDeadSensors;
    int     nContribPixelLayers;
    int     nGangedFlaggedFakes;
    int     nPixelSpoiltHits;
    int     nSCTHits;
    int     nSCTHoles;
    int     nSCTSharedHits;
    int     nSCTOutliers;
    int     nSCTDeadSensors;
    int     nTRTHits;
    int     nTRTHoles;
    int     nTRTSharedHits;
    int     nTRTDeadSensors;
    int     nTRTOutliers;
    float   dEdx;
    float   ptCone20OverPt;
    float   ptCone30OverPt;
    float   ptCone40OverPt;
    float   ptCone20OverKVUPt;
    float   ptCone30OverKVUPt;
    float   ptCone40OverKVUPt;
    float   eTTopoClus20;
    float   eTTopoClus30;
    float   eTTopoClus40;
    float   eTTopoCone20;
    float   eTTopoCone30;
    float   eTTopoCone40;
    float   dRJet20;
    float   dRJet50;
    float   dRMSTrack;
    float   dRMuon;
    float   dRElectron;
    float   d0;
    float   d0Sig;
    float   z0;
    float   z0PV;
    float   z0PVSinTheta;
    float   chi2Prob;
    std::vector<float> parameterCovMatrix;
    float   electron_key;
    float   muon_key;
    float   truth_phi;
    float   truth_eta;
    float   truth_pt;
    float   truth_e;
    int     truth_charge;
    int     truth_pdgid;
    int     truth_barcode;
    int     truth_nChildren;
    float   truth_properTime;
    float   truth_decayr;
    float   truth_matchingProb;
    float   truth_parent_phi;
    float   truth_parent_eta;
    float   truth_parent_pt;
    float   truth_parent_e;
    int     truth_parent_charge;
    int     truth_parent_pdgid;
    int     truth_parent_barcode;
    int     truth_parent_nChildren;
    float   truth_parent_decayr;
    bool    truth_flag;
    float   truth_deltar;
  };

  struct truth {
    float   phi;
    float   eta;
    float   pt;
    float   e;
    float   vis_phi;
    float   vis_eta;
    float   vis_pt;
    float   vis_e;
    int     charge;
    int     pdgid;
    int     barcode;
    int     nChildren;
    int     nTauProngs;
    int     nPi0;
    int     isTauHad;
    int     parent_barcode;
    float   properTime;
    float   decayr;
  };

  struct lepton {
    float   phi;
    float   eta;
    float   pt;
    float   e;
    int     charge;
    float   ptVarCone20OverPt;
    float   ptVarCone30OverPt;
    float   ptVarCone40OverPt;
    float   eTTopoCone20;
    float   eTTopoCone30;
    float   eTTopoCone40;
    int     quality;
    int     isSignal;
    int     isTrigMatch;
    int     key;
  };

  struct lepton_track {
    float   lepton_phi;
    float   lepton_eta;
    float   lepton_pt;
    float   lepton_e;
    int     lepton_quality;
    int     lepton_isTrigMatch;
    float   mstrack_phi;
    float   mstrack_eta;
    float   mstrack_pt;
    float   mstrack_e;
    float   mstrack_charge;
    float   stdtrack_phi;
    float   stdtrack_eta;
    float   stdtrack_pt;
    float   stdtrack_e;
    float   stdtrack_charge;
    float   stdtrack_chi2Prob;
    float   stdtrack_d0;
    float   stdtrack_deltaR;
    float   stdtrack_deltaR_MS;
    float   stdtrack_SCTHits;
    float   stdtrack_ptcone40overpt;
    std::string stdtrack_layer;
    float   tracklet_pt;
    float   tracklet_phi;
    float   tracklet_eta;
    float   tracklet_vcpt;
    float   tracklet_vcphi;
    float   tracklet_vceta;
    float   tracklet_e;
    float   tracklet_charge;
    float   tracklet_chi2Prob;
    float   tracklet_deltaR_MS;
    float   tracklet_vccharge;
    float   tracklet_d0;
    float   tracklet_deltaR;
    float   tracklet_ptcone40overpt;
    std::string tracklet_layer;
  };

  struct tau {
    float   phi;
    float   eta;
    float   pt;
    float   e;
    int     charge;
    int     nprongs;
    float   BDTJetScore;
    int     isMedium;
    std::vector<float> track_phi;
    std::vector<float> track_eta;
    std::vector<float> track_pt;
    std::vector<float> track_e;
  };

  struct jet {
    float   phi;
    float   eta;
    float   pt;
    float   e;
    int     isBJet;
  };

  struct msTrack {
    float   phi;
    float   eta;
    float   pt;
    float   e;
    float   charge;
  };

  MyAnalysis(TTree *tree=0);
  virtual ~MyAnalysis();
  virtual Int_t    Cut(Long64_t entry);
  virtual Int_t    GetEntry(Long64_t entry);
  virtual Long64_t LoadTree(Long64_t entry);
  virtual void     Init(TTree *tree);
  virtual void     Loop(std::string fin, std::string fout, double xsec, double sum_of_weight, bool is_special_sample);
  virtual Bool_t   Notify();
  virtual void     Show(Long64_t entry = -1);

  // event_manager.C
  virtual void     pileup_condition_finder();
  virtual void     zll_met();
  virtual void     get_hadron_cr_weight_hist();
  virtual void     fill_event_hist();
  virtual double   get_jet_met_deltaphi(struct jet*);
  virtual double   get_reco_mt(struct tau *tau);

  // proxy_setter.C
  virtual void     proxy_setter();
  virtual void     struct_clear();

  // hist_manager.C
  virtual void     hist_getter();
  virtual void     hist_def();
  virtual void     hist_region_setter();
  virtual void     skim_tree_setter(std::string fout);
  virtual void     write_skim_tree();
  virtual void     write_hist(std::string fout);
  virtual void     treat_and_write(TH1* hist);
  virtual void     treat_hist(TH1* hist, bool islogx, bool isTreatBin);
  virtual void     treat_hist_2d(TH1* hist, bool islogx);
  virtual void     treat_bin(TH1* hist);

  // kinematic_selection.C
  virtual std::string   kinematic_selection();
  virtual void     tag_probe_manager(struct track* track);

  // track_manager.C
  virtual void     fill_histogram();
  virtual void     print_track_info(struct track*);
  virtual void     print_electron_info(struct lepton*);
  virtual void     print_track_info(struct lepton_track*);
  virtual void     fill_skim_tree(struct track*);
  virtual double   get_hadron_cr_ettopoclus20_weight(struct track*);
  virtual double   get_met_deltaphi(struct track*);
  virtual double   get_met_deltaphi(struct lepton*);
  virtual double   get_cstmet_deltaphi(struct track*);
  virtual double   get_tst_deltaphi(struct track*);
  virtual double   get_cst_deltaphi(struct track*);
  virtual double   get_mT(struct track*);
  virtual double   get_mT(struct lepton*);
  virtual double   get_mT_subtract(struct track*);
  virtual double   get_subtractmet(struct track*);
  virtual double   get_visibletau_mT(struct track*);
  virtual double   get_visibletau_mT_subtract(struct track*);
  virtual double   get_deltar_truth_parent(struct track* track);
  virtual void     electron_cr(struct track* track);

  // track_selection.C
  virtual void     track_layer_finder(struct track*);
  virtual bool     preselection(struct track*, std::string purpose);
  virtual bool     blind(struct track*);

  // truth_manager.C
  virtual void     nearest_truth_finder(struct track*);
  virtual void     nearest_track_finder(struct truth*);
  virtual unsigned get_number_of_pi0(struct track*);
  virtual double   get_parent_tau_visible_pt(struct track* track);
  virtual double   get_parent_tau_visible_phi(struct track* track);
  virtual void     fill_truth_hist();
  virtual double   get_truth_mt(struct truth *truth);
  virtual unsigned get_tau_prongs(int barcode);
  virtual unsigned get_npi0(int barcode);
  virtual void     print_truth_info(struct truth *truth);

  // smearing.C
  virtual void     smearing_manager(struct track* track);
  virtual void     pt_smearing_muon(double pt, int charge, double tf_weight, double cr_weight, std::vector<double>* bin, TH1* hist, std::string function_mode);
  virtual void     pt_smearing_muon(double pt, double eta, int charge, double tf_weight, double cr_weight, std::vector<double>* bin, TH2* hist, std::string function_mode);
  virtual void     convert_qoverpt_to_pt();

  // retracking.C
  virtual void     retracking_manager(struct track* track);
  virtual void     fill_retracking_hist();

  // tf_manager.C
  virtual void     tf_manager();
  virtual void     tag_probe_for_caloveto(struct track* track);
  virtual void     apply_caloveto();
  virtual void     get_correction_factor(struct track* track);
  virtual void     truthpt_weight_open();

  // fit_function.C
  virtual double   crystalBall(double *x, double *par);
  virtual double   crystalBall_linear(double *x, double *par);
  virtual double   crystalBall_extend(double *x, double *par);

  // tag_and_probe_manager.cxx
  virtual bool     tag_and_probe_manager(struct track* track);
  virtual TLorentzVector* get_probe_cluster(struct track *track);

  /* template <typename T, typename U> */
  /* double get_delta_phi(T *phys_obj0, U *phys_obj1) { */
  /*   double delta_phi = TStd::Vector2::Phi_mpi_pi(phys_obj0->phi - phys_obj1->phi); */
  /*   return delta_phi; */
  /* } */

  template <typename T, typename U>
  double get_collinear_mass(T tau0, U tau1) {
    double collinear_mass(0.);
    double tau0_pt_miss = missingET_TST_pt * TMath::Sin(TVector2::Phi_mpi_pi(tau1->phi - missingET_TST_phi)) / TMath::Sin(TVector2::Phi_mpi_pi(tau0->phi - tau1->phi));
    double tau1_pt_miss = missingET_TST_pt * TMath::Sin(TVector2::Phi_mpi_pi(tau0->phi - missingET_TST_phi)) / TMath::Sin(TVector2::Phi_mpi_pi(tau0->phi - tau1->phi));
    double tau0_p_miss = tau0_pt_miss * cosh(tau0->eta);
    double tau1_p_miss = tau1_pt_miss * cosh(tau1->eta);
    double tau0_p_vis = tau0->pt * cosh(tau0->eta);
    double tau1_p_vis = tau1->pt * cosh(tau1->eta);
    double x0 = tau0_p_vis / (tau0_p_vis + tau0_p_miss);
    double x1 = tau1_p_vis / (tau1_p_vis + tau1_p_miss);
    TLorentzVector tau0_vis, tau1_vis, m_vis;
    tau0_vis.SetPtEtaPhiE(tau0->pt, tau0->eta, tau0->phi, tau0->e);
    tau1_vis.SetPtEtaPhiE(tau1->pt, tau1->eta, tau1->phi, tau1->e);
    collinear_mass = (tau0_vis = tau1_vis).M() / TMath::Sqrt(x0 * x1);
    return collinear_mass; 
  }

  template <typename T>
  double get_collinear_mass(T tau0, struct track *tau1) {
    if(fabs(TVector2::Phi_mpi_pi(tau0->phi - missingET_TST_phi)) + fabs(TVector2::Phi_mpi_pi(tau1->phi - missingET_TST_phi)) > fabs(TVector2::Phi_mpi_pi(tau0->phi - tau1->phi))) return -1.;
    double collinear_mass(0.);
    double tau0_pt_miss = missingET_TST_pt * fabs(TMath::Sin(TVector2::Phi_mpi_pi(tau1->phi - missingET_TST_phi))) / fabs(TMath::Sin(TVector2::Phi_mpi_pi(tau1->phi - tau0->phi)));
    double tau1_pt_miss = missingET_TST_pt * fabs(TMath::Sin(TVector2::Phi_mpi_pi(tau0->phi - missingET_TST_phi))) / fabs(TMath::Sin(TVector2::Phi_mpi_pi(tau0->phi - tau1->phi)));
    double tau0_p_miss = tau0_pt_miss * cosh(tau0->eta);
    double tau1_p_miss = tau1_pt_miss * cosh(tau1->eta);
    double tau0_p_vis = tau0->pt * cosh(tau0->eta);
    double tau1_p_vis = tau1->eTTopoClus20 * cosh(tau1->eta);
    double x0 = tau0_p_vis / (tau0_p_vis + tau0_p_miss);
    double x1 = tau1_p_vis / (tau1_p_vis + tau1_p_miss);
    TLorentzVector tau0_vis, tau1_vis, m_vis;
    tau0_vis.SetPtEtaPhiE(tau0->pt, tau0->eta, tau0->phi, tau0->e);
    tau1_vis.SetPtEtaPhiE(tau1->eTTopoClus20, tau1->eta, tau1->phi, tau1->eTTopoClus20 * cosh(tau1->eta));
    collinear_mass = (tau0_vis + tau1_vis).M() / TMath::Sqrt(x0 * x1);
    return collinear_mass; 
  }

  /* template <> */
  /* double get_collinear_mass(struct truth *tau0, struct truth *tau1) { */
  /*   double collinear_mass(0.); */
  /*   // std::cout<<"tau0 : "<<tau0->isTauHad<<", tau1 : "<<tau1->isTauHad<<std::endl; */
  /*   // std::cout<<"phi0 = "<<tau0->vis_phi<<", phi1 = "<<tau1->vis_phi<<", MET phi = "<<missingET_TST_phi<<", MET pt = "<<missingET_TST_pt<<std::endl; */
  /*   // std::cout<<fabs(TVector2::Phi_mpi_pi(tau1->vis_phi - missingET_TST_phi))<<", "<<fabs(TVector2::Phi_mpi_pi(tau0->vis_phi - missingET_TST_phi))<<", "<<fabs(TVector2::Phi_mpi_pi(tau0->vis_phi - tau1->vis_phi))<<std::endl; */
  /*   if(fabs(TVector2::Phi_mpi_pi(tau1->vis_phi - missingET_TST_phi)) + fabs(TVector2::Phi_mpi_pi(tau0->vis_phi - missingET_TST_phi)) > fabs(TVector2::Phi_mpi_pi(tau0->vis_phi - tau1->vis_phi))) return -1.; */
  /*   double tau0_pt_miss = missingET_TST_pt * fabs(TMath::Sin(TVector2::Phi_mpi_pi(tau1->vis_phi - missingET_TST_phi))) / fabs(TMath::Sin(TVector2::Phi_mpi_pi(tau1->vis_phi - tau0->vis_phi))); */
  /*   double tau1_pt_miss = missingET_TST_pt * fabs(TMath::Sin(TVector2::Phi_mpi_pi(tau0->vis_phi - missingET_TST_phi))) / fabs(TMath::Sin(TVector2::Phi_mpi_pi(tau0->vis_phi - tau1->vis_phi))); */
  /*   double tau0_p_miss = tau0_pt_miss * cosh(tau0->vis_eta); */
  /*   double tau1_p_miss = tau1_pt_miss * cosh(tau1->vis_eta); */
  /*   double tau0_p_vis = tau0->vis_pt * cosh(tau0->vis_eta); */
  /*   double tau1_p_vis = tau1->vis_pt * cosh(tau1->vis_eta); */
  /*   double x0 = tau0_p_vis / (tau0_p_vis + tau0_p_miss); */
  /*   double x1 = tau1_p_vis / (tau1_p_vis + tau1_p_miss); */
  /*   TLorentzVector tau0_vis, tau1_vis, m_vis; */
  /*   tau0_vis.SetPtEtaPhiE(tau0->vis_pt, tau0->vis_eta, tau0->vis_phi, tau0->vis_e); */
  /*   tau1_vis.SetPtEtaPhiE(tau1->vis_pt, tau1->vis_eta, tau1->vis_phi, tau1->vis_e); */
  /*   collinear_mass = (tau0_vis + tau1_vis).M() / TMath::Sqrt(x0 * x1); */
  /*   // std::cout<<"collinear_mass = "<<collinear_mass<<std::endl; */
  /*   return collinear_mass; */
  /* } */

  double m_EventWeight;
  double m_integrated_luminosity;
  std::string m_selected_KinematicalRegion;
  std::string m_selected_KindOfTrack;
  bool m_is_special_sample;
  bool m_is_vc_default;
  bool m_is_cst_default;
  const Float_t zmass;
  const Float_t emass;
  double m_min_dphi_jet_met;

  std::vector<struct track*>* tracks;
  std::vector<struct truth*>* truths;
  std::vector<struct lepton*>* electrons;
  std::vector<struct lepton*>* muons;
  std::vector<struct tau*>* taus;
  std::vector<struct jet*>* jets;
  std::vector<struct jet*>* truthjets;
  std::vector<struct jet*>* caloClusters;
  std::vector<struct msTrack*>* msTracks;
  std::vector<struct lepton_track*>* muon_tracks;
  std::vector<struct lepton_track*>* electron_tracks;

  Float_t         missingET_TST_lepton_pt;
  Float_t         missingET_TST_lepton_phi;

  std::vector<std::string>* name_KinematicalRegion;
  std::vector<std::string>* name_TrackRegion;
  std::vector<std::string>* name_KindOfTrack;
  unsigned nKinematicalRegion;
  unsigned nTrackRegion;
  unsigned nKindOfTrack;


  std::vector<struct hist*>* hists;
  std::vector<struct hist> hists_vec;

  std::ofstream m_file_tracklet_profile_test;
  TFile *m_file_weight_truthpt;

  // weight
  TH1D* m_h_weight_ettopoclus20_zll_signal_region_fourlayer;
  TH1D* m_h_weight_ettopoclus20_lowmet_signal_region_fourlayer;
  TH1D* m_h_weight_ettopoclus20_highmet_signal_region_fourlayer;
  TH2D* m_h_weight_pt_eta_zll_signal_region_fourlayer;
  TH2D* m_h_weight_pt_eta_lowmet_signal_region_fourlayer;
  TH2D* m_h_weight_pt_eta_highmet_signal_region_fourlayer;

  TH1D* m_h_weight_truthpt_flat_pion;
};
#endif

