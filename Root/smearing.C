#include "MyAnalysis.h"

void MyAnalysis::smearing_manager(struct track* track) {

  double tf_weight(0.);
  double cr_weight(0.);

  std::vector<double> *QoverPtBin = new std::vector<double>;
  *QoverPtBin = {1./12.5, 1./2.5, 1./1.11803, 1./0.5, 1./0.292402, 1./0.170998, 1./0.1, 1./0.0764724, 1./0.06, 1./0.0447214, 1./0.0341995, 1./0.0261532, 1./0.02};


  if(preselection(track, "smearing") == false) return;

  if(track->ptCone40OverPt < 0.04 &&
     fabs(track->d0Sig) < 1.5 && fabs(track->z0PVSinTheta) < 0.5)
    m_h_track_met_hadron->Fill(missingET_TST_pt, m_EventWeight);
  if(m_selected_KinematicalRegion == "highmet") {
    if(fabs(track->d0Sig) < 1.5 && fabs(track->z0PVSinTheta) < 0.5)
      m_h_track_ptcone40overpt_hadron_highmet->Fill(track->ptCone40OverPt, m_EventWeight);
    if(track->ptCone40OverPt < 0.04) {
      m_h_track_d0Sig_hadron_highmet->Fill(track->d0Sig, m_EventWeight);
    }
    if(track->ptCone40OverPt < 0.04 &&
       fabs(track->d0Sig) < 1.5 && fabs(track->z0PVSinTheta) < 0.5) {
      tf_weight = m_h_ele_caloiso_432to0->GetBinContent(m_h_ele_caloiso_432to0->FindBin(track->pt, track->eta));
      cr_weight = m_h_had_corr_caloiso_432to0->GetBinContent(m_h_had_corr_caloiso_432to0->FindBin(track->pt));
      m_h_track_pt_eta_hadron_highmet->Fill(track->pt, track->eta, m_EventWeight);
      m_h_track_pt_eta_caloveto_hadron_highmet->Fill(track->pt, track->eta, m_EventWeight * tf_weight);
      m_h_track_ettopoclus20_hadron_highmet->Fill(track->eTTopoClus20, m_EventWeight);

      pt_smearing_muon(track->pt, track->eta, track->charge, 1.       , 1.       , QoverPtBin, m_h_track_smeared_qoverpt_eta_hadron_highmet                     , "extend");
      pt_smearing_muon(track->pt, track->eta, track->charge, tf_weight, 1.       , QoverPtBin, m_h_track_smeared_qoverpt_eta_caloveto_hadron_highmet            , "extend");
      pt_smearing_muon(track->pt, track->eta, track->charge, tf_weight, cr_weight, QoverPtBin, m_h_track_smeared_qoverpt_eta_caloveto_corr_hadron_highmet       , "extend");
      pt_smearing_muon(track->pt, track->eta, track->charge, tf_weight, cr_weight, QoverPtBin, m_h_track_smeared_linear_qoverpt_eta_caloveto_corr_hadron_highmet, "linear");

      // print_track_info(track);
    }
  }
  if(m_selected_KinematicalRegion == "lowmet") {
    if(fabs(track->d0Sig) < 1.5 && fabs(track->z0PVSinTheta) < 0.5)
      m_h_track_ptcone40overpt_hadron_lowmet->Fill(track->ptCone40OverPt, m_EventWeight);
    if(track->ptCone40OverPt < 0.04) {
      m_h_track_d0Sig_hadron_lowmet->Fill(track->d0Sig, m_EventWeight);
    }
    if(track->ptCone40OverPt < 0.04 &&
       fabs(track->d0Sig) < 1.5 && fabs(track->z0PVSinTheta) < 0.5) {
      tf_weight = m_h_ele_caloiso_432to0->GetBinContent(m_h_ele_caloiso_432to0->FindBin(track->pt, track->eta));
      cr_weight = m_h_had_corr_caloiso_432to0->GetBinContent(m_h_had_corr_caloiso_432to0->FindBin(track->pt));
      m_h_track_pt_eta_hadron_lowmet->Fill(track->pt, track->eta, m_EventWeight);
      m_h_track_pt_eta_caloveto_hadron_lowmet->Fill(track->pt, track->eta, m_EventWeight * tf_weight);
      m_h_track_ettopoclus20_hadron_lowmet->Fill(track->eTTopoClus20, m_EventWeight);

      pt_smearing_muon(track->pt, track->eta, track->charge, 1.       , 1.       , QoverPtBin, m_h_track_smeared_qoverpt_eta_hadron_lowmet                     , "extend");
      pt_smearing_muon(track->pt, track->eta, track->charge, tf_weight, 1.       , QoverPtBin, m_h_track_smeared_qoverpt_eta_caloveto_hadron_lowmet            , "extend");
      pt_smearing_muon(track->pt, track->eta, track->charge, tf_weight, cr_weight, QoverPtBin, m_h_track_smeared_qoverpt_eta_caloveto_corr_hadron_lowmet       , "extend");
      pt_smearing_muon(track->pt, track->eta, track->charge, tf_weight, cr_weight, QoverPtBin, m_h_track_smeared_linear_qoverpt_eta_caloveto_corr_hadron_lowmet, "linear");

    }
  }
}

void MyAnalysis::pt_smearing_muon(double pt, int charge, double tf_weight, double cr_weight, std::vector<double>* bin, TH1* hist, std::string function_mode) {
  TF1* f_smearing_muon;
  if(function_mode == "extend") {
    f_smearing_muon = new TF1("smearing_muon", [&](double*x, double *p){ return crystalBall_extend(x,p); }, -1000., 1000., 6);
    if(pt > 10. && pt <= 15.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.87, 1.74, 6.53, 0.423);
    if(pt > 15. && pt <= 20.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.46, 1.74, 6.44, 0.394);
    if(pt > 20. && pt <= 25.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.24, 1.74, 6.40, 0.378);
    if(pt > 25. && pt <= 35.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.12, 1.74, 6.37, 0.369);
    if(pt > 35. && pt <= 45.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.88, 1.64, 6.70, 0.352);
    if(pt > 45. && pt <= 60.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.76, 1.59, 6.85, 0.344);
    if(pt > 60. && pt <= 100.) f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.68, 1.54, 7.09, 0.337);
    if(pt > 100.)              f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.7 , 1.47, 7.41, 0.339);
  } else if(function_mode == "sigma1up") {
    f_smearing_muon = new TF1("smearing_muon", [&](double*x, double *p){ return crystalBall_extend(x,p); }, -1000., 1000., 6);
    if(pt > 10. && pt <= 15.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.87+0.02, 1.74, 6.53, 0.423);
    if(pt > 15. && pt <= 20.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.46+0.02, 1.74, 6.44, 0.394);
    if(pt > 20. && pt <= 25.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.24+0.02, 1.74, 6.40, 0.378);
    if(pt > 25. && pt <= 35.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.12+0.02, 1.74, 6.37, 0.369);
    if(pt > 35. && pt <= 45.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.88+0.01, 1.64, 6.70, 0.352);
    if(pt > 45. && pt <= 60.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.76+0.02, 1.59, 6.85, 0.344);
    if(pt > 60. && pt <= 100.) f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.68+0.03, 1.54, 7.09, 0.337);
    if(pt > 100.)              f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.7 +0.1 , 1.47, 7.41, 0.339);
  } else if(function_mode == "sigma1down") {
    f_smearing_muon = new TF1("smearing_muon", [&](double*x, double *p){ return crystalBall_extend(x,p); }, -1000., 1000., 6);
    if(pt > 10. && pt <= 15.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.87-0.02, 1.74, 6.53, 0.423);
    if(pt > 15. && pt <= 20.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.46-0.02, 1.74, 6.44, 0.394);
    if(pt > 20. && pt <= 25.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.24-0.02, 1.74, 6.40, 0.378);
    if(pt > 25. && pt <= 35.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.12-0.02, 1.74, 6.37, 0.369);
    if(pt > 35. && pt <= 45.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.88-0.01, 1.64, 6.70, 0.352);
    if(pt > 45. && pt <= 60.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.76-0.02, 1.59, 6.85, 0.344);
    if(pt > 60. && pt <= 100.) f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.68-0.03, 1.54, 7.09, 0.337);
    if(pt > 100.)              f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.7 -0.1 , 1.47, 7.41, 0.339);
  } else if(function_mode == "alpha1up") {
    f_smearing_muon = new TF1("smearing_muon", [&](double*x, double *p){ return crystalBall_extend(x,p); }, -1000., 1000., 6);
    if(pt > 10. && pt <= 15.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.87, 1.74+0.02, 6.53, 0.423);
    if(pt > 15. && pt <= 20.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.46, 1.74+0.02, 6.44, 0.394);
    if(pt > 20. && pt <= 25.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.24, 1.74+0.02, 6.40, 0.378);
    if(pt > 25. && pt <= 35.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.12, 1.74+0.02, 6.37, 0.369);
    if(pt > 35. && pt <= 45.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.88, 1.64+0.01, 6.70, 0.352);
    if(pt > 45. && pt <= 60.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.76, 1.59+0.01, 6.85, 0.344);
    if(pt > 60. && pt <= 100.) f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.68, 1.54+0.02, 7.09, 0.337);
    if(pt > 100.)              f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.7 , 1.47+0.06, 7.41, 0.339);
  } else if(function_mode == "alpha1down") {
    f_smearing_muon = new TF1("smearing_muon", [&](double*x, double *p){ return crystalBall_extend(x,p); }, -1000., 1000., 6);
    if(pt > 10. && pt <= 15.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.87, 1.74-0.02, 6.53, 0.423);
    if(pt > 15. && pt <= 20.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.46, 1.74-0.02, 6.44, 0.394);
    if(pt > 20. && pt <= 25.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.24, 1.74-0.02, 6.40, 0.378);
    if(pt > 25. && pt <= 35.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.12, 1.74-0.02, 6.37, 0.369);
    if(pt > 35. && pt <= 45.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.88, 1.64-0.01, 6.70, 0.352);
    if(pt > 45. && pt <= 60.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.76, 1.59-0.01, 6.85, 0.344);
    if(pt > 60. && pt <= 100.) f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.68, 1.54-0.02, 7.09, 0.337);
    if(pt > 100.)              f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.7 , 1.47-0.06, 7.41, 0.339);
  } else if(function_mode == "beta1up") {
    f_smearing_muon = new TF1("smearing_muon", [&](double*x, double *p){ return crystalBall_extend(x,p); }, -1000., 1000., 6);
    if(pt > 10. && pt <= 15.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.87, 1.74, 6.53+0.93, 0.423);
    if(pt > 15. && pt <= 20.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.46, 1.74, 6.44+0.88, 0.394);
    if(pt > 20. && pt <= 25.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.24, 1.74, 6.40+0.86, 0.378);
    if(pt > 25. && pt <= 35.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.12, 1.74, 6.37+0.84, 0.369);
    if(pt > 35. && pt <= 45.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.88, 1.64, 6.70+0.90, 0.352);
    if(pt > 45. && pt <= 60.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.76, 1.59, 6.85+0.93, 0.344);
    if(pt > 60. && pt <= 100.) f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.68, 1.54, 7.09+0.98, 0.337);
    if(pt > 100.)              f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.7 , 1.47, 7.41+1.05, 0.339);
  } else if(function_mode == "beta1down") {
    f_smearing_muon = new TF1("smearing_muon", [&](double*x, double *p){ return crystalBall_extend(x,p); }, -1000., 1000., 6);
    if(pt > 10. && pt <= 15.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.87, 1.74, 6.53-0.93, 0.423);
    if(pt > 15. && pt <= 20.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.46, 1.74, 6.44-0.88, 0.394);
    if(pt > 20. && pt <= 25.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.24, 1.74, 6.40-0.86, 0.378);
    if(pt > 25. && pt <= 35.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.12, 1.74, 6.37-0.84, 0.369);
    if(pt > 35. && pt <= 45.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.88, 1.64, 6.70-0.90, 0.352);
    if(pt > 45. && pt <= 60.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.76, 1.59, 6.85-0.93, 0.344);
    if(pt > 60. && pt <= 100.) f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.68, 1.54, 7.09-0.98, 0.337);
    if(pt > 100.)              f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.7 , 1.47, 7.41-1.05, 0.339);
  } else if(function_mode == "gamma1up") {
    f_smearing_muon = new TF1("smearing_muon", [&](double*x, double *p){ return crystalBall_extend(x,p); }, -1000., 1000., 6);
    if(pt > 10. && pt <= 15.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.87, 1.74, 6.53, 0.423+0.061);
    if(pt > 15. && pt <= 20.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.46, 1.74, 6.44, 0.394+0.057);
    if(pt > 20. && pt <= 25.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.24, 1.74, 6.40, 0.378+0.054);
    if(pt > 25. && pt <= 35.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.12, 1.74, 6.37, 0.369+0.053);
    if(pt > 35. && pt <= 45.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.88, 1.64, 6.70, 0.352+0.051);
    if(pt > 45. && pt <= 60.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.76, 1.59, 6.85, 0.344+0.050);
    if(pt > 60. && pt <= 100.) f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.68, 1.54, 7.09, 0.337+0.049);
    if(pt > 100.)              f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.7 , 1.47, 7.41, 0.339+0.049);
  } else if(function_mode == "gamma1down") {
    f_smearing_muon = new TF1("smearing_muon", [&](double*x, double *p){ return crystalBall_extend(x,p); }, -1000., 1000., 6);
    if(pt > 10. && pt <= 15.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.87, 1.74, 6.53, 0.423-0.061);
    if(pt > 15. && pt <= 20.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.46, 1.74, 6.44, 0.394-0.057);
    if(pt > 20. && pt <= 25.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.24, 1.74, 6.40, 0.378-0.054);
    if(pt > 25. && pt <= 35.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.12, 1.74, 6.37, 0.369-0.053);
    if(pt > 35. && pt <= 45.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.88, 1.64, 6.70, 0.352-0.051);
    if(pt > 45. && pt <= 60.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.76, 1.59, 6.85, 0.344-0.050);
    if(pt > 60. && pt <= 100.) f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.68, 1.54, 7.09, 0.337-0.049);
    if(pt > 100.)              f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.7 , 1.47, 7.41, 0.339-0.049);
  } else if(function_mode == "mean") {
    f_smearing_muon = new TF1("smearing_muon", [&](double*x, double *p){ return crystalBall_extend(x,p); }, -1000., 1000., 6);
    if(pt > 10. && pt <= 15.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt+0.233, 5.87, 1.74, 6.53, 0.423);
    if(pt > 15. && pt <= 20.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt+0.233, 5.46, 1.74, 6.44, 0.394);
    if(pt > 20. && pt <= 25.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt+0.233, 5.24, 1.74, 6.40, 0.378);
    if(pt > 25. && pt <= 35.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt+0.233, 5.12, 1.74, 6.37, 0.369);
    if(pt > 35. && pt <= 45.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt+0.219, 4.88, 1.64, 6.70, 0.352);
    if(pt > 45. && pt <= 60.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt+0.206, 4.76, 1.59, 6.85, 0.344);
    if(pt > 60. && pt <= 100.) f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt+0.116, 4.68, 1.54, 7.09, 0.337);
    if(pt > 100.)              f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt+0.223, 4.7 , 1.47, 7.41, 0.339);
  } else if(function_mode == "linear") {
    f_smearing_muon = new TF1("smearing_muon_linear", [&](double*x, double *p){ return crystalBall_linear(x,p); }, -1000., 1000., 4);
    if(pt > 10. && pt <= 15.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.87, 1.74);
    if(pt > 15. && pt <= 20.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.46, 1.74);
    if(pt > 20. && pt <= 25.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.24, 1.74);
    if(pt > 25. && pt <= 35.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 5.12, 1.74);
    if(pt > 35. && pt <= 45.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.88, 1.64);
    if(pt > 45. && pt <= 60.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.76, 1.59);
    if(pt > 60. && pt <= 100.) f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.68, 1.54);
    if(pt > 100.)              f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 4.7 , 1.47);
  } else if(function_mode == "woVC") {
    f_smearing_muon = new TF1("smearing_muon_linear", [&](double*x, double *p){ return crystalBall_linear(x,p); }, -1000., 1000., 4);
    if(pt > 10. && pt <= 15.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 14.6, 1.89);
    if(pt > 15. && pt <= 20.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 14.0, 1.89);
    if(pt > 20. && pt <= 25.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 13.7, 1.89);
    if(pt > 25. && pt <= 35.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 13.5, 1.89);
    if(pt > 35. && pt <= 45.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 13.2, 1.85);
    if(pt > 45. && pt <= 60.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 13.0, 1.81);
    if(pt > 60. && pt <= 100.) f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 13.0, 1.92);
    if(pt > 100.)              f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 12.9, 1.89);
  } else if(function_mode == "1stwave") {
    f_smearing_muon = new TF1("smearing_muon_linear", [&](double*x, double *p){ return crystalBall_linear(x,p); }, -1000., 1000., 4);
    if(pt > 10. && pt <= 15.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 16.96, 1.72);
    if(pt > 15. && pt <= 20.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 15.54, 1.72);
    if(pt > 20. && pt <= 25.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 14.91, 1.72);
    if(pt > 25. && pt <= 35.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 14.84, 1.72);
    if(pt > 35. && pt <= 45.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 14.21, 1.66);
    if(pt > 45. && pt <= 60.)  f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 13.64, 1.62);
    if(pt > 60. && pt <= 100.) f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 13.44, 1.68);
    if(pt > 100.)              f_smearing_muon->SetParameters(1., (double)charge*1.e3/pt, 13.21, 1.64);
  } else {
    std::cout<<"function mode = "<<function_mode<<" didn't match any defined name"<<std::endl;
    return;
  }

  double constant(0.);
  if(function_mode == "mean") constant = tf_weight * cr_weight / f_smearing_muon->Integral(-1000.+charge*1.e3/pt+0.233, 1000.+charge*1.e3/pt+0.233);
  else constant = tf_weight * cr_weight / f_smearing_muon->Integral(-1000.+charge*1.e3/pt, 1000.+charge*1.e3/pt);
  f_smearing_muon->SetParameter(0, constant);
  double bin_bottom(0.);
  double bin_top(0.);
  // std::cout<<"function mode = "<<function_mode<<" >>>>> ";
  for(unsigned ii_bin(0); ii_bin < bin->size()-1; ++ii_bin) {
    bin_bottom = bin->at(ii_bin);
    bin_top = bin->at(ii_bin+1);
    hist->Fill((bin_bottom + bin_top) / 2., f_smearing_muon->Integral(bin_bottom, bin_top) + f_smearing_muon->Integral(-bin_top, -bin_bottom));
  }
  // std::cout<<std::endl;
  delete f_smearing_muon;
  f_smearing_muon = nullptr;
}

void MyAnalysis::pt_smearing_muon(double pt, double eta, int charge, double tf_weight, double cr_weight, std::vector<double>* bin, TH2* hist, std::string function_mode) {
  TF1* f_smearing_muon;
  if(function_mode == "extend") {
    f_smearing_muon = new TF1("smearing_muon", [&](double*x, double *p){ return crystalBall_extend(x,p); }, -100., 100., 6);
    if(pt > 10. && pt <= 15.)  f_smearing_muon->SetParameters(1., 1.e3/pt, 5.87, 1.74, 6.53, 0.423);
    if(pt > 15. && pt <= 20.)  f_smearing_muon->SetParameters(1., 1.e3/pt, 5.46, 1.74, 6.44, 0.394);
    if(pt > 20. && pt <= 25.)  f_smearing_muon->SetParameters(1., 1.e3/pt, 5.24, 1.74, 6.40, 0.378);
    if(pt > 25. && pt <= 35.)  f_smearing_muon->SetParameters(1., 1.e3/pt, 5.12, 1.74, 6.37, 0.369);
    if(pt > 35. && pt <= 45.)  f_smearing_muon->SetParameters(1., 1.e3/pt, 4.88, 1.64, 6.70, 0.352);
    if(pt > 45. && pt <= 60.)  f_smearing_muon->SetParameters(1., 1.e3/pt, 4.76, 1.59, 6.85, 0.344);
    if(pt > 60. && pt <= 100.) f_smearing_muon->SetParameters(1., 1.e3/pt, 4.68, 1.54, 7.09, 0.337);
    if(pt > 100.)              f_smearing_muon->SetParameters(1., 1.e3/pt, 4.7 , 1.47, 7.41, 0.339);
  } else if(function_mode == "linear") {
    f_smearing_muon = new TF1("smearing_muon", [&](double*x, double *p){ return crystalBall_linear(x,p); }, -100., 100., 4);
    if(pt > 10. && pt <= 15.)  f_smearing_muon->SetParameters(1., 1.e3/pt, 5.87, 1.74);
    if(pt > 15. && pt <= 20.)  f_smearing_muon->SetParameters(1., 1.e3/pt, 5.46, 1.74);
    if(pt > 20. && pt <= 25.)  f_smearing_muon->SetParameters(1., 1.e3/pt, 5.24, 1.74);
    if(pt > 25. && pt <= 35.)  f_smearing_muon->SetParameters(1., 1.e3/pt, 5.12, 1.74);
    if(pt > 35. && pt <= 45.)  f_smearing_muon->SetParameters(1., 1.e3/pt, 4.88, 1.64);
    if(pt > 45. && pt <= 60.)  f_smearing_muon->SetParameters(1., 1.e3/pt, 4.76, 1.59);
    if(pt > 60. && pt <= 100.) f_smearing_muon->SetParameters(1., 1.e3/pt, 4.68, 1.54);
    if(pt > 100.)              f_smearing_muon->SetParameters(1., 1.e3/pt, 4.7 , 1.47);
  } else if(function_mode == "woVC") {
    f_smearing_muon = new TF1("smearing_muon", [&](double*x, double *p){ return crystalBall_linear(x,p); }, -100., 100., 4);
    if(pt > 10. && pt <= 15.)  f_smearing_muon->SetParameters(1., 1.e3/pt, 14.6, 1.89);
    if(pt > 15. && pt <= 20.)  f_smearing_muon->SetParameters(1., 1.e3/pt, 14.0, 1.89);
    if(pt > 20. && pt <= 25.)  f_smearing_muon->SetParameters(1., 1.e3/pt, 13.7, 1.89);
    if(pt > 25. && pt <= 35.)  f_smearing_muon->SetParameters(1., 1.e3/pt, 13.5, 1.89);
    if(pt > 35. && pt <= 45.)  f_smearing_muon->SetParameters(1., 1.e3/pt, 13.2, 1.85);
    if(pt > 45. && pt <= 60.)  f_smearing_muon->SetParameters(1., 1.e3/pt, 13.0, 1.81);
    if(pt > 60. && pt <= 100.) f_smearing_muon->SetParameters(1., 1.e3/pt, 13.0, 1.92);
    if(pt > 100.)              f_smearing_muon->SetParameters(1., 1.e3/pt, 12.9, 1.89);
  } else if(function_mode == "1stwave") {
    f_smearing_muon = new TF1("smearing_muon_linear", [&](double*x, double *p){ return crystalBall_linear(x,p); }, -100., 100., 4);
    if(pt > 10. && pt <= 15.)  f_smearing_muon->SetParameters(1., 1.e3/pt, 16.96, 1.72);
    if(pt > 15. && pt <= 20.)  f_smearing_muon->SetParameters(1., 1.e3/pt, 15.54, 1.72);
    if(pt > 20. && pt <= 25.)  f_smearing_muon->SetParameters(1., 1.e3/pt, 14.91, 1.72);
    if(pt > 25. && pt <= 35.)  f_smearing_muon->SetParameters(1., 1.e3/pt, 14.84, 1.72);
    if(pt > 35. && pt <= 45.)  f_smearing_muon->SetParameters(1., 1.e3/pt, 14.21, 1.66);
    if(pt > 45. && pt <= 60.)  f_smearing_muon->SetParameters(1., 1.e3/pt, 13.64, 1.62);
    if(pt > 60. && pt <= 100.) f_smearing_muon->SetParameters(1., 1.e3/pt, 13.44, 1.68);
    if(pt > 100.)              f_smearing_muon->SetParameters(1., 1.e3/pt, 13.21, 1.64);
  } else std::cout<<"function mode = "<<function_mode<<" didn't match any defined name"<<std::endl;

  double constant = tf_weight * cr_weight / f_smearing_muon->Integral(-1000., 1000.);
  f_smearing_muon->SetParameter(0, constant);
  double bin_bottom(0.);
  double bin_top(0.);
  for(unsigned ii_bin(0); ii_bin < bin->size()-1; ++ii_bin) {
    bin_bottom = bin->at(ii_bin);
    bin_top = bin->at(ii_bin+1);
    hist->Fill((bin_bottom + bin_top) / 2., eta, f_smearing_muon->Integral(bin_bottom, bin_top));
  }
  delete f_smearing_muon;
  f_smearing_muon = nullptr;
}

void MyAnalysis::convert_qoverpt_to_pt() {
  for(unsigned ii_binx = 0; ii_binx < m_h_track_smeared_qoverpt_eta_hadron_highmet->GetNbinsX(); ++ii_binx) {

    for(unsigned ii_zll = 0; ii_zll < 2; ++ii_zll) {
      for(unsigned ii_layer = 0; ii_layer < 2; ++ii_layer) {
	m_h_retracking_smeared_stdtrack_pt[ii_zll][ii_layer]->SetBinContent(ii_binx+1, m_h_retracking_smeared_stdtrack_qoverpt[ii_zll][ii_layer]->GetBinContent(m_h_retracking_smeared_stdtrack_qoverpt[ii_zll][ii_layer]->GetNbinsX() - ii_binx));
	m_h_retracking_smeared_stdtrack_pt[ii_zll][ii_layer]->SetBinError(ii_binx+1, m_h_retracking_smeared_stdtrack_qoverpt[ii_zll][ii_layer]->GetBinError(m_h_retracking_smeared_stdtrack_qoverpt[ii_zll][ii_layer]->GetNbinsX() - ii_binx));
	m_h_retracking_smeared_stdtrack_pt_1stwave[ii_zll][ii_layer]->SetBinContent(ii_binx+1, m_h_retracking_smeared_stdtrack_qoverpt_1stwave[ii_zll][ii_layer]->GetBinContent(m_h_retracking_smeared_stdtrack_qoverpt_1stwave[ii_zll][ii_layer]->GetNbinsX() - ii_binx));
	m_h_retracking_smeared_stdtrack_pt_1stwave[ii_zll][ii_layer]->SetBinError(ii_binx+1, m_h_retracking_smeared_stdtrack_qoverpt_1stwave[ii_zll][ii_layer]->GetBinError(m_h_retracking_smeared_stdtrack_qoverpt_1stwave[ii_zll][ii_layer]->GetNbinsX() - ii_binx));
	m_h_retracking_smeared_stdtrack_vcpt_linear[ii_zll][ii_layer]->SetBinContent(ii_binx+1, m_h_retracking_smeared_stdtrack_qovervcpt_linear[ii_zll][ii_layer]->GetBinContent(m_h_retracking_smeared_stdtrack_qovervcpt_linear[ii_zll][ii_layer]->GetNbinsX() - ii_binx));
	m_h_retracking_smeared_stdtrack_vcpt_linear[ii_zll][ii_layer]->SetBinError(ii_binx+1, m_h_retracking_smeared_stdtrack_qovervcpt_linear[ii_zll][ii_layer]->GetBinError(m_h_retracking_smeared_stdtrack_qovervcpt_linear[ii_zll][ii_layer]->GetNbinsX() - ii_binx));
	m_h_retracking_smeared_stdtrack_vcpt[ii_zll][ii_layer]->SetBinContent(ii_binx+1, m_h_retracking_smeared_stdtrack_qovervcpt[ii_zll][ii_layer]->GetBinContent(m_h_retracking_smeared_stdtrack_qoverpt[ii_zll][ii_layer]->GetNbinsX() - ii_binx));
	m_h_retracking_smeared_stdtrack_vcpt[ii_zll][ii_layer]->SetBinError(ii_binx+1, m_h_retracking_smeared_stdtrack_qovervcpt[ii_zll][ii_layer]->GetBinError(m_h_retracking_smeared_stdtrack_qoverpt[ii_zll][ii_layer]->GetNbinsX() - ii_binx));
	m_h_retracking_smeared_stdtrack_vcpt_sigma1up[ii_zll][ii_layer]->SetBinContent(ii_binx+1, m_h_retracking_smeared_stdtrack_qovervcpt_sigma1up[ii_zll][ii_layer]->GetBinContent(m_h_retracking_smeared_stdtrack_qovervcpt_sigma1up[ii_zll][ii_layer]->GetNbinsX() - ii_binx));
	m_h_retracking_smeared_stdtrack_vcpt_sigma1up[ii_zll][ii_layer]->SetBinError(ii_binx+1, m_h_retracking_smeared_stdtrack_qovervcpt_sigma1up[ii_zll][ii_layer]->GetBinError(m_h_retracking_smeared_stdtrack_qovervcpt_sigma1up[ii_zll][ii_layer]->GetNbinsX() - ii_binx));
	m_h_retracking_smeared_stdtrack_vcpt_sigma1down[ii_zll][ii_layer]->SetBinContent(ii_binx+1, m_h_retracking_smeared_stdtrack_qovervcpt_sigma1down[ii_zll][ii_layer]->GetBinContent(m_h_retracking_smeared_stdtrack_qovervcpt_sigma1down[ii_zll][ii_layer]->GetNbinsX() - ii_binx));
	m_h_retracking_smeared_stdtrack_vcpt_sigma1down[ii_zll][ii_layer]->SetBinError(ii_binx+1, m_h_retracking_smeared_stdtrack_qovervcpt_sigma1down[ii_zll][ii_layer]->GetBinError(m_h_retracking_smeared_stdtrack_qovervcpt_sigma1down[ii_zll][ii_layer]->GetNbinsX() - ii_binx));
	m_h_retracking_smeared_stdtrack_vcpt_alpha1up[ii_zll][ii_layer]->SetBinContent(ii_binx+1, m_h_retracking_smeared_stdtrack_qovervcpt_alpha1up[ii_zll][ii_layer]->GetBinContent(m_h_retracking_smeared_stdtrack_qovervcpt_alpha1up[ii_zll][ii_layer]->GetNbinsX() - ii_binx));
	m_h_retracking_smeared_stdtrack_vcpt_alpha1up[ii_zll][ii_layer]->SetBinError(ii_binx+1, m_h_retracking_smeared_stdtrack_qovervcpt_alpha1up[ii_zll][ii_layer]->GetBinError(m_h_retracking_smeared_stdtrack_qovervcpt_alpha1up[ii_zll][ii_layer]->GetNbinsX() - ii_binx));
	m_h_retracking_smeared_stdtrack_vcpt_alpha1down[ii_zll][ii_layer]->SetBinContent(ii_binx+1, m_h_retracking_smeared_stdtrack_qovervcpt_alpha1down[ii_zll][ii_layer]->GetBinContent(m_h_retracking_smeared_stdtrack_qovervcpt_alpha1down[ii_zll][ii_layer]->GetNbinsX() - ii_binx));
	m_h_retracking_smeared_stdtrack_vcpt_alpha1down[ii_zll][ii_layer]->SetBinError(ii_binx+1, m_h_retracking_smeared_stdtrack_qovervcpt_alpha1down[ii_zll][ii_layer]->GetBinError(m_h_retracking_smeared_stdtrack_qovervcpt_alpha1down[ii_zll][ii_layer]->GetNbinsX() - ii_binx));
	m_h_retracking_smeared_stdtrack_vcpt_beta1up[ii_zll][ii_layer]->SetBinContent(ii_binx+1, m_h_retracking_smeared_stdtrack_qovervcpt_beta1up[ii_zll][ii_layer]->GetBinContent(m_h_retracking_smeared_stdtrack_qovervcpt_beta1up[ii_zll][ii_layer]->GetNbinsX() - ii_binx));
	m_h_retracking_smeared_stdtrack_vcpt_beta1up[ii_zll][ii_layer]->SetBinError(ii_binx+1, m_h_retracking_smeared_stdtrack_qovervcpt_beta1up[ii_zll][ii_layer]->GetBinError(m_h_retracking_smeared_stdtrack_qovervcpt_beta1up[ii_zll][ii_layer]->GetNbinsX() - ii_binx));
	m_h_retracking_smeared_stdtrack_vcpt_beta1down[ii_zll][ii_layer]->SetBinContent(ii_binx+1, m_h_retracking_smeared_stdtrack_qovervcpt_beta1down[ii_zll][ii_layer]->GetBinContent(m_h_retracking_smeared_stdtrack_qovervcpt_beta1down[ii_zll][ii_layer]->GetNbinsX() - ii_binx));
	m_h_retracking_smeared_stdtrack_vcpt_beta1down[ii_zll][ii_layer]->SetBinError(ii_binx+1, m_h_retracking_smeared_stdtrack_qovervcpt_beta1down[ii_zll][ii_layer]->GetBinError(m_h_retracking_smeared_stdtrack_qovervcpt_beta1down[ii_zll][ii_layer]->GetNbinsX() - ii_binx));
	m_h_retracking_smeared_stdtrack_vcpt_gamma1up[ii_zll][ii_layer]->SetBinContent(ii_binx+1, m_h_retracking_smeared_stdtrack_qovervcpt_gamma1up[ii_zll][ii_layer]->GetBinContent(m_h_retracking_smeared_stdtrack_qovervcpt_gamma1up[ii_zll][ii_layer]->GetNbinsX() - ii_binx));
	m_h_retracking_smeared_stdtrack_vcpt_gamma1up[ii_zll][ii_layer]->SetBinError(ii_binx+1, m_h_retracking_smeared_stdtrack_qovervcpt_gamma1up[ii_zll][ii_layer]->GetBinError(m_h_retracking_smeared_stdtrack_qovervcpt_gamma1up[ii_zll][ii_layer]->GetNbinsX() - ii_binx));
	m_h_retracking_smeared_stdtrack_vcpt_gamma1down[ii_zll][ii_layer]->SetBinContent(ii_binx+1, m_h_retracking_smeared_stdtrack_qovervcpt_gamma1down[ii_zll][ii_layer]->GetBinContent(m_h_retracking_smeared_stdtrack_qovervcpt_gamma1down[ii_zll][ii_layer]->GetNbinsX() - ii_binx));
	m_h_retracking_smeared_stdtrack_vcpt_gamma1down[ii_zll][ii_layer]->SetBinError(ii_binx+1, m_h_retracking_smeared_stdtrack_qovervcpt_gamma1down[ii_zll][ii_layer]->GetBinError(m_h_retracking_smeared_stdtrack_qovervcpt_gamma1down[ii_zll][ii_layer]->GetNbinsX() - ii_binx));
      }
    }

    for(unsigned ii_biny = 0; ii_biny < m_h_track_smeared_qoverpt_eta_hadron_highmet->GetNbinsY(); ++ii_biny) {
      m_h_track_smeared_pt_eta_hadron_highmet->SetBinContent(ii_binx+1, ii_biny+1, m_h_track_smeared_qoverpt_eta_hadron_highmet->GetBinContent(m_h_track_smeared_qoverpt_eta_hadron_highmet->GetNbinsX() - ii_binx, ii_biny+1));
      m_h_track_smeared_pt_eta_hadron_highmet->SetBinError(ii_binx+1, ii_biny+1, m_h_track_smeared_qoverpt_eta_hadron_highmet->GetBinError(m_h_track_smeared_qoverpt_eta_hadron_highmet->GetNbinsX() - ii_binx, ii_biny+1));
      m_h_track_smeared_pt_eta_hadron_lowmet->SetBinContent(ii_binx+1, ii_biny+1, m_h_track_smeared_qoverpt_eta_hadron_lowmet->GetBinContent(m_h_track_smeared_qoverpt_eta_hadron_lowmet->GetNbinsX() - ii_binx, ii_biny+1));
      m_h_track_smeared_pt_eta_hadron_lowmet->SetBinError(ii_binx+1, ii_biny+1, m_h_track_smeared_qoverpt_eta_hadron_lowmet->GetBinError(m_h_track_smeared_qoverpt_eta_hadron_lowmet->GetNbinsX() - ii_binx, ii_biny+1));

      m_h_track_smeared_pt_eta_caloveto_hadron_highmet->SetBinContent(ii_binx+1, ii_biny+1, m_h_track_smeared_qoverpt_eta_caloveto_hadron_highmet->GetBinContent(m_h_track_smeared_qoverpt_eta_hadron_highmet->GetNbinsX() - ii_binx, ii_biny+1));
      m_h_track_smeared_pt_eta_caloveto_hadron_highmet->SetBinError(ii_binx+1, ii_biny+1, m_h_track_smeared_qoverpt_eta_caloveto_hadron_highmet->GetBinError(m_h_track_smeared_qoverpt_eta_hadron_highmet->GetNbinsX() - ii_binx, ii_biny+1));
      m_h_track_smeared_pt_eta_caloveto_hadron_lowmet->SetBinContent(ii_binx+1, ii_biny+1, m_h_track_smeared_qoverpt_eta_caloveto_hadron_lowmet->GetBinContent(m_h_track_smeared_qoverpt_eta_hadron_lowmet->GetNbinsX() - ii_binx, ii_biny+1));
      m_h_track_smeared_pt_eta_caloveto_hadron_lowmet->SetBinError(ii_binx+1, ii_biny+1, m_h_track_smeared_qoverpt_eta_caloveto_hadron_lowmet->GetBinError(m_h_track_smeared_qoverpt_eta_hadron_lowmet->GetNbinsX() - ii_binx, ii_biny+1));

      m_h_track_smeared_pt_eta_caloveto_corr_hadron_highmet->SetBinContent(ii_binx+1, ii_biny+1, m_h_track_smeared_qoverpt_eta_caloveto_corr_hadron_highmet->GetBinContent(m_h_track_smeared_qoverpt_eta_hadron_highmet->GetNbinsX() - ii_binx, ii_biny+1));
      m_h_track_smeared_pt_eta_caloveto_corr_hadron_highmet->SetBinError(ii_binx+1, ii_biny+1, m_h_track_smeared_qoverpt_eta_caloveto_corr_hadron_highmet->GetBinError(m_h_track_smeared_qoverpt_eta_hadron_highmet->GetNbinsX() - ii_binx, ii_biny+1));
      m_h_track_smeared_pt_eta_caloveto_corr_hadron_lowmet->SetBinContent(ii_binx+1, ii_biny+1, m_h_track_smeared_qoverpt_eta_caloveto_corr_hadron_lowmet->GetBinContent(m_h_track_smeared_qoverpt_eta_hadron_lowmet->GetNbinsX() - ii_binx, ii_biny+1));
      m_h_track_smeared_pt_eta_caloveto_corr_hadron_lowmet->SetBinError(ii_binx+1, ii_biny+1, m_h_track_smeared_qoverpt_eta_caloveto_corr_hadron_lowmet->GetBinError(m_h_track_smeared_qoverpt_eta_hadron_lowmet->GetNbinsX() - ii_binx, ii_biny+1));

      m_h_track_smeared_linear_pt_eta_caloveto_corr_hadron_highmet->SetBinContent(ii_binx+1, ii_biny+1, m_h_track_smeared_linear_qoverpt_eta_caloveto_corr_hadron_highmet->GetBinContent(m_h_track_smeared_qoverpt_eta_hadron_highmet->GetNbinsX() - ii_binx, ii_biny+1));
      m_h_track_smeared_linear_pt_eta_caloveto_corr_hadron_highmet->SetBinError(ii_binx+1, ii_biny+1, m_h_track_smeared_linear_qoverpt_eta_caloveto_corr_hadron_highmet->GetBinError(m_h_track_smeared_qoverpt_eta_hadron_highmet->GetNbinsX() - ii_binx, ii_biny+1));
      m_h_track_smeared_linear_pt_eta_caloveto_corr_hadron_lowmet->SetBinContent(ii_binx+1, ii_biny+1, m_h_track_smeared_linear_qoverpt_eta_caloveto_corr_hadron_lowmet->GetBinContent(m_h_track_smeared_qoverpt_eta_hadron_lowmet->GetNbinsX() - ii_binx, ii_biny+1));
      m_h_track_smeared_linear_pt_eta_caloveto_corr_hadron_lowmet->SetBinError(ii_binx+1, ii_biny+1, m_h_track_smeared_linear_qoverpt_eta_caloveto_corr_hadron_lowmet->GetBinError(m_h_track_smeared_qoverpt_eta_hadron_lowmet->GetNbinsX() - ii_binx, ii_biny+1));

    }
  }
}

